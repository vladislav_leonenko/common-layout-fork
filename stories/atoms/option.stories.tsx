import { Option } from '../../src/components/atoms';
import * as React from 'react';
import { Center } from '../decorators/center';

export default {
  title: 'Atoms/Option',
  component: Option,
  parameters: {
    background: [{ name: 'Blue', value: '#001D6D', default: true }]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>]
};


export const Default = () => <Option value={'in_progress'}>В работе</Option>;
export const Disabled = () => <Option value={'in_progress'} disabled>В работе</Option>;
