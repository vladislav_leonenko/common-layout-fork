import * as React from "react";
import {DateWithTime} from "../../src/components/atoms/dateWithTime/dateWithTime";
import {Center} from "../decorators/center";


export default {
  title: 'Atoms/DateWithTime',
  component: DateWithTime,
  parameters: {
    background: [{name: 'Blue', value: '#001D6D', default: true}]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>],
};

export const Default = () => <DateWithTime/>
