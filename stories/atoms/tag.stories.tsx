import { Badge, Tag } from '../../src/components/atoms';
import * as React from 'react';
import { Center } from '../decorators/center';

export default {
  title: 'Atoms/Tag',
  component: Tag,
  parameters: {
    background: [{ name: 'Blue', value: '#001D6D', default: true }]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>]
};


export const Default = () => <Tag><span>Тег</span></Tag>;
export const WithBadge = () => <Tag><Badge size={'small'} color={'green'}/><span>Тег</span></Tag>;
