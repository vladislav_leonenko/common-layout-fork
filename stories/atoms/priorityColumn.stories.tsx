import { PriorityColumn } from '../../src/components/atoms';
import * as React from 'react';
import { Center } from '../decorators/center';

export default {
  title: 'Atoms/PriorityColumn',
  component: PriorityColumn,
  parameters: {
    background: [{ name: 'Blue', value: '#001D6D', default: true }]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>]
};


export const Default = () => <PriorityColumn color={'yellow'}/>;
