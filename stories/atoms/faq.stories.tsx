import * as React from "react";
import {Faq} from "../../src/components/atoms/faq/faq";
import {Center} from "../decorators/center";

export const Default = () => <Faq/>

export default {
  title: 'Atoms/Faq',
  component: Faq,
  parameters: {
    background: [{name: 'Blue', value: '#001D6D', default: true}]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>],
};
