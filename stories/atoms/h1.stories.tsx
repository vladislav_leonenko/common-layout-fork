import * as React from "react";
import {Center} from "../decorators/center";
import {H1} from "../../src/components/atoms/h1/h1";

export default {
  title: 'Atoms/Heading (H1)',
  parameters: {
    background: [{name: 'Blue', value: '#001D6D', default: true}]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>],
};

export const Default = () => <H1>Heading</H1>
