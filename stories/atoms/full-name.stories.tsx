import * as React from "react";
import {FullName} from "../../src/components/atoms/fullName/fullName";
import {Center} from "../decorators/center";

export default {
  title: 'Atoms/FullName',
  parameters: {
    background: [{name: 'Blue', value: '#001D6D', default: true}]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>],
};

export const Default = () => <FullName fullName={`Константин Константинопольский`} post={`Руководитель ДО Пресненский`}/>
