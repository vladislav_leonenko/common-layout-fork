import * as React from "react";
import {Center} from "../decorators/center";
import {Switch} from "../../src/components/atoms/switch/switch";

export default {
  title: 'Atoms/Switch',
  parameters: {
    background: [{name: 'Blue', value: '#001D6D', default: true}]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>],
};

const mockProps = {
  checked: false,
  name: 'success',
  onChange: (name: string, value: boolean) => console.log({name, value})
}

export const Default = () => <Switch {...mockProps} />
export const WithLabel = () => <Switch {...mockProps}>Только активные</Switch>
