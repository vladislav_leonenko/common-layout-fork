import * as React from "react";
import {Logotype} from "../../src/components/atoms/logotype/logotype";
import {Center} from "../decorators/center";

export default {
  title: 'Atoms/Logotype',
  parameters: {
    background: [{name: 'Blue', value: '#001D6D', default: true}]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>],
};

export const Default = () => <Logotype/>
