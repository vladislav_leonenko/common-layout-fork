import { Checkbox } from '../../src/components/atoms';
import * as React from 'react';
import { Center } from '../decorators/center';

export default {
  title: 'Atoms/Checkbox',
  component: Checkbox,
  parameters: {
    background: [{ name: 'Blue', value: '#001D6D', default: true }]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>]
};

export const Default = () => <Checkbox/>;
export const WithText = () => <Checkbox>Пункт для выбора</Checkbox>;
export const Checked = () => <Checkbox checked>Пункт для выбора</Checkbox>;
export const Disabled = () => <Checkbox disabled>Пункт для выбора</Checkbox>;
