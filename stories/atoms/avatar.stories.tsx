import * as React from "react";
import {Avatar} from "../../src/components/atoms/avatar/avatar";
import {Center} from "../decorators/center";

export default {
  title: 'Atoms/Avatar',
  parameters: {
    background: [{name: 'Blue', value: '#001D6D', default: true}]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>],
};

export const Default = () => <Avatar/>
export const WithImage = () => <Avatar
  image={`https://rockcult.ru/wp-content/uploads/2018/06/david-guetta-bengaluru-concert-759.jpg`}/>
