import { Link } from '../../src/components/atoms';
import * as React from 'react';
import { Center } from '../decorators/center';

export default {
  title: 'Atoms/Link',
  component: Link,
  parameters: {
    background: [{ name: 'Blue', value: '#001D6D', default: true }]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>]
};

export const Primary = () => <Link content={'Primary link'} />;
export const Secondary = () => <Link type={'secondary'} content={'Secondary link'} />;
