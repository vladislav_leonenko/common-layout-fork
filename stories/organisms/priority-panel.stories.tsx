import * as React from 'react';
import { Center } from '../decorators/center';
import { PriorityPanelItem } from '../../src/components/atoms';
import { LeadCard, PriorityPanel } from '../../src/components/molecules';
import { priorityPanelMock } from '../../src/mocks/priorityPanel';

export default {
  title: 'Organisms/PriorityPanel',
  parameters: {
    background: [{ name: 'Blue', value: '#001D6D', default: true }]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center isFulWidth>{storyFn()}</Center>]
};

export const Default = () => (
  <PriorityPanel>
    {priorityPanelMock.map((item) => (
      <PriorityPanelItem key={Math.random()} {...item}>
        <div
          style={{
            position: 'absolute',
            top: 'calc(100% + 2px)',
            width: '100%'
          }}
        >
          <LeadCard
            id={Math.random()}
            title={`Заявка на `}
            created="12:30"
            companyName="LLC Facebook"
            fullName="Mark Zukerberg"
            leadChannel="CRM"
            timer={'10:59'}
            links={['#']}
            branch={'Branch'}
            performer="Ivanov"
          />
        </div>
      </PriorityPanelItem>
    ))}
  </PriorityPanel>
);
