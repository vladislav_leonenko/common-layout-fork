import * as React from 'react';
import { Center } from '../decorators/center';
import { Table } from '../../src/components/organisms/table/table';
import { ColumnsTable, TasksTableItem } from '../../src/types';
import { myTasksTableMock } from '../../src/mocks/tasks';
import style from '../../src/components/organisms/table/table.sass';
import block from 'bem-css-modules';

const b = block(style);

export default {
  title: 'Organisms/Table',
  parameters: {
    background: [{ name: 'Blue', value: '#001D6D', default: true }]
  }
};

const columns: ColumnsTable[] = [{
  title: 'Наименование',
  index: 'title',
  key: 'title',
  render: (text: string, row: TasksTableItem) => {
    return <span className={b('priority', {
      [row.priority]: true
    })}>{text}</span>;
  }
}, {
  title: 'Исполнитель',
  index: 'executor',
  key: 'executor',
  width: '200px'
}, {
  title: 'Тип',
  index: 'type',
  key: 'type',
  width: '180px'
}, {
  title: 'Статус',
  index: 'status',
  key: 'status',
  width: '120px'
}, {
  title: 'Срок',
  index: 'completion_date',
  key: 'completion_date',
  width: '100px'
}, {
  title: '',
  index: 'dots',
  key: '',
  width: '50px',
  render: (text: string, row: any) => {
    return <span className={b('dots')}/>;
  }
}];

export const Default = () => <Table columns={columns} source={myTasksTableMock} onClick={(row: any) => console.log(row)}/>;
