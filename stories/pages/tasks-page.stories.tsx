import * as React from 'react';
import { TasksPage } from '../../src/components/pages/tasks/tasks';
import { Layout } from '../../src/components/templates';
import style from '../../src/components/organisms/table/table.sass';
import block from 'bem-css-modules';
import { ColumnsTable, TasksTableItem } from '../../src/types';

const b = block(style);

export default {
  title: 'Pages/Tasks',
  parameters: {
    background: [{ name: 'Blue', value: '#001D6D', default: true }]
  }
};

const columns: ColumnsTable[] = [
  {
    title: 'Наименование',
    index: 'title',
    key: 'title',
    render: (text: string, row: TasksTableItem) => {
      return (
        <span
          className={b('priority', {
            [row.priority]: true
          })}
        >
          {text}
        </span>
      );
    }
  },
  {
    title: 'Исполнитель',
    index: 'executor',
    key: 'executor',
    render: (text: string, row: TasksTableItem) => {
      return <span>!{text}</span>;
    }
  },
  {
    title: 'Тип',
    index: 'type',
    key: 'type'
  },
  {
    title: 'Статус',
    index: 'status',
    key: 'status'
  },
  {
    title: 'Срок',
    index: 'completion_date',
    key: 'completion_date'
  },
  {
    title: '',
    index: 'dots',
    key: '',
    render: (text: string, row: any) => {
      return <span className={b('dots')} />;
    }
  }
];

export const Default = () => (
  <Layout
    menuItems={[]}
    image={`https://rockcult.ru/wp-content/uploads/2018/06/david-guetta-bengaluru-concert-759.jpg`}
    fullName={`Константин Константинопольский`}
    post={`Руководитель ДО Пресненский`}
  >
    <TasksPage columns={columns} />
  </Layout>
);
