import * as React from 'react';
import { Layout } from '../../src/components/templates';
import { PriorityPanelItem } from '../../src/components/atoms';
import { PriorityPanel, WidgetCalendar, WidgetTasks } from '../../src/components/molecules';
import { priorityPanelMock } from '../../src/mocks/priorityPanel';
import { menuMock, myTasksMock } from '../../src/mocks';

export default {
  title: 'Pages/Main',
  parameters: {
    background: [{ name: 'Blue', value: '#001D6D', default: true }]
  }
};

const handleOnClickTask = (id_task) => {
  console.log('handleOnClickTask', id_task);
};

export const Default = () => (
  <Layout
    menuItems={menuMock}
    image={`https://rockcult.ru/wp-content/uploads/2018/06/david-guetta-bengaluru-concert-759.jpg`}
    fullName={`Константин Константинопольский`}
    post={`Руководитель ДО Пресненский`}
  >
    <PriorityPanel>
      {priorityPanelMock.map((item) => (
        <PriorityPanelItem key={Math.random()} {...item} />
      ))}
    </PriorityPanel>

    <div style={{ padding: '0 24px' }}>
      <div className="container-fluid no-padding">
        <div className="row">
          <div className="col-lg-6 col-sm-12">
            <WidgetTasks {...myTasksMock} onClickTask={handleOnClickTask}/>
          </div>

          <div className="col-lg-6 col-sm-12">
            <WidgetCalendar list={[]}/>
          </div>
        </div>
      </div>
    </div>
  </Layout>
);
