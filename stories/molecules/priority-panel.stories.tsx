import * as React from 'react';
import { Center } from '../decorators/center';
import { PriorityPanelItem } from '../../src/components/atoms';
import { PriorityPanel } from '../../src/components/molecules';
import { priorityPanelMock } from '../../src/mocks/priorityPanel';

export default {
  title: 'Molecules/PriorityPanel',
  parameters: {
    background: [{ name: 'Blue', value: '#001D6D', default: true }]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center isFulWidth>{storyFn()}</Center>]
};

export const Default = () => (
  <PriorityPanel>
    {priorityPanelMock.map((item) => (
      <PriorityPanelItem key={Math.random()} {...item} />
    ))}
  </PriorityPanel>
);
