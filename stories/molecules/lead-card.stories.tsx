import * as React from 'react';
import { Center } from '../decorators/center';
import { LeadCard } from '../../src/components/molecules/';
import { leadCardsMock } from '../../src/mocks/';

export default {
  title: 'Molecules/LeadCard',
  parameters: {
    background: [{ name: 'Blue', value: '#001D6D', default: true }]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center isFulWidth>{storyFn()}</Center>]
};

export const Default = () => (
  <>
    <LeadCard {...leadCardsMock} />
  </>
);
