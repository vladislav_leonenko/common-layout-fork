import * as React from 'react';
import {Menu} from "../../src/components/molecules/menu/menu";
import {menuMock} from "../../src/mocks/menu";
import {Center} from "../decorators/center";

export default {
  title: 'Molecules/Menu',
  parameters: {
    background: [{name: 'Blue', value: '#001D6D', default: true}]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>],
};

// @ts-ignore
export const Default = () => <Menu list={menuMock}/>
