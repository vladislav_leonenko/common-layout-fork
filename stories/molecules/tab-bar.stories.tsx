import * as React from "react";
import {Center} from "../decorators/center";
import {TabBar} from "../../src/components/molecules/tabBar/tabBar";
import { TabBarItem } from '../../src/components/atoms/tabBarItem/tabBarItem';

export default {
  title: 'Molecules/TabBar',
  parameters: {
    background: [{name: 'Blue', value: '#001D6D', default: true}]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>],
};

export const Default = () => <TabBar>
  <TabBarItem label={'Мои задачи'}>1</TabBarItem>
  <TabBarItem label={'Порученные мной'}>2</TabBarItem>
</TabBar>
