import * as React from "react";
import {FullNameWithAvatar} from "../../src/components/molecules/fullNameWithAvatar/fullNameWithAvatar";
import {Center} from "../decorators/center";

export default {
  title: 'Molecules/FullNameWithAvatar',
  parameters: {
    background: [{name: 'Blue', value: '#001D6D', default: true}]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>],
};

export const Default = () => <FullNameWithAvatar image={`https://2.bp.blogspot.com/-R_OBDK5hQvo/VcfWldbQz7I/AAAAAAAACR4/9R0S_dpVsmI/s1600/hilo-640x472%2B%25281%2529.jpg`}
                                                 fullName={`Иванов Иван Иванович`}
                                                 post={`Руководитель ДО Пресненский`}/>
