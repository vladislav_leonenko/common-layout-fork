import * as React from "react";
import {Header} from "../../src/components/molecules/header/header";

export default {
  title: 'Molecules/Header',
  parameters: {
    background: [{name: 'Blue', value: '#001D6D', default: true}]
  },
};

export const Default = () => <Header
  image={`https://rockcult.ru/wp-content/uploads/2018/06/david-guetta-bengaluru-concert-759.jpg`}
  fullName={`Константин Константинопольский`}
  post={`Руководитель ДО`}/>
