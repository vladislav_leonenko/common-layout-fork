import * as React from 'react';
import { NotificationPanel } from '../../src/components/molecules';
import { Center } from '../decorators/center';
import { linksData, contentNew } from '../../src/mocks';

export default {
  title: 'Molecules/NotificationPanel',
  parameters: {
    background: [{ name: 'Blue', value: '#001D6D', default: true }]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center isFulWidth>{storyFn()}</Center>]
};

export const Success = () => <NotificationPanel links={linksData} content={contentNew} />;

export const Warning = () => <NotificationPanel links={linksData} isSuccess={false} content={contentNew} />;
