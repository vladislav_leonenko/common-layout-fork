import * as React from "react";
import {Sidebar} from "../../src/components/molecules/sidebar/sidebar";
import {menuMock} from "../../src/mocks/menu";
import {Center} from "../decorators/center";

export default {
  title: 'Molecules/Sidebar',
  parameters: {
    background: [{name: 'Blue', value: '#001D6D', default: true}]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>],
};

export const Default = () => <Sidebar list={menuMock}/>
