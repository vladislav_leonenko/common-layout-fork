import * as React from 'react';
import { Center } from '../decorators/center';
import { SelectBox } from '../../src/components/molecules';
import { Checkbox, Option } from '../../src/components/atoms';

export default {
  title: 'Molecules/SelectBox',
  parameters: {
    background: [{ name: 'Blue', value: '#001D6D', default: true }]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>]
};

export const Default = () => (<SelectBox placeholder={'Статус'}
                                         selected={['in_progress']}
                                         multiple>
  <Option value={'new'}>Новая</Option>
  <Option value={'in_progress'}>В работе</Option>
  <Option value={'completed'}>Завершенная</Option>
</SelectBox>);
