import * as React from 'react';
import { Center } from '../decorators/center';
import { DatepickerInput } from '../../src/components/molecules';

export default {
  title: 'Molecules/DatePickerInput',
  component: DatepickerInput,
  parameters: {
    background: [{ name: 'Blue', value: '#001D6D', default: true }]
  },
  decorators: [(storyFn: () => React.ReactNode) => <Center>{storyFn()}</Center>]
};


export const Default = () => <DatepickerInput/>;
