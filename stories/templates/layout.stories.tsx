import * as React from 'react';
import { Layout } from '../../src/components/templates';
import { menuMock } from '../../src/mocks';

export default {
  title: 'Templates/Layout',
  parameters: {
    background: [{ name: 'Blue', value: '#001D6D', default: true }]
  }
};

export const Default = () => <Layout image={`https://rockcult.ru/wp-content/uploads/2018/06/david-guetta-bengaluru-concert-759.jpg`}
                                     fullName={`Константин Константинопольский`}
                                     post={`Руководитель ДО Пресненский`}
                                     menuItems={menuMock}>
</Layout>;
