import * as React from 'react'
import style from './center.sass';
import block from 'bem-css-modules';

const b = block(style);

interface CenterProps {
  children: React.ReactNode,
  isFulWidth?: boolean
}

export const Center: React.FC<CenterProps> = ({children, isFulWidth = false}) => {
  return (<div className={b(null, {isFulWidth})}>
    {children}
  </div>);
}
