import { MyTasksWidgetInterface } from '../types';
import { PRIORITY_COLOR } from '../utils';
import moment from 'moment';

export const myTasksMock: MyTasksWidgetInterface = {
  my: {
    today: 30,
    expired: 2,
    list: [{
      id: 1,
      name: 'Отчет по найму сотрудников в апреле',
      limitDate: (new Date()),
      badge: 'red',
      is_expired: true
    }, {
      id: 2,
      name: 'Отчет о переводе сотрудников на диста…',
      limitDate: (new Date()),
      badge: 'yellow'
    }, {
      id: 3,
      name: 'Встретиться с клиентом ООО Рома…',
      limitDate: (new Date()),
      badge: 'red'
    }, {
      id: 4,
      name: 'Отчет по найму сотрудников в апреле',
      limitDate: (new Date()),
      badge: 'blue',
      last: true
    }]
  },
  received: {
    today: 34,
    expired: 1,
    list: [{
      id: 5,
      name: 'Собрать документы у ИП Петров',
      limitDate: (new Date()),
      badge: 'yellow'
    }, {
      id: 6,
      name: 'Позвонить ООО АБС',
      limitDate: (new Date()),
      badge: 'red'
    }, {
      id: 7,
      name: 'Подготовить отчет по ООО Комета',
      limitDate: (new Date()),
      badge: 'blue'
    }, {
      id: 8,
      name: 'Подготовить отчет по ООО Орбита',
      limitDate: (new Date()),
      badge: 'silver',
      last: true
    }]
  }
};


export const myTasksTableMock = [{
  priority: PRIORITY_COLOR.IMPORTANT_URGENT,
  name: 'Собрать документы у ИП Петров',
  executor: 'Колиниченко Г.А.',
  type: 'Встреча с клиентом',
  status: 'В работе',
  completion_date: '18 мая'
}, {
  priority: PRIORITY_COLOR.IMPORTANT_NOT_URGENT,
  name: 'Собрать документы у ИП Петров',
  executor: 'Колиниченко Г.А.',
  type: 'Встреча с клиентом',
  status: 'В работе',
  completion_date: '18 мая'
}, {
  priority: PRIORITY_COLOR.NOT_IMPORTANT_URGENT,
  name: 'Собрать документы у ИП Петров',
  executor: 'Колиниченко Г.А.',
  type: 'Встреча с клиентом',
  status: 'В работе',
  completion_date: '18 мая'
}, {
  priority: PRIORITY_COLOR.NOT_IMPORTANT_NOT_URGENT,
  name: 'Собрать документы у ИП Петров',
  executor: 'Колиниченко Г.А.',
  type: 'Встреча с клиентом',
  status: 'В работе',
  completion_date: '18 мая'
}, {
  priority: PRIORITY_COLOR.NO_PRIORITY,
  name: 'Собрать документы у ИП Петров',
  executor: 'Колиниченко Г.А.',
  type: 'Встреча с клиентом',
  status: 'В работе',
  completion_date: '18 мая'
}, {
  priority: PRIORITY_COLOR.IMPORTANT_NOT_URGENT,
  name: 'Собрать документы у ИП Петров',
  executor: 'Колиниченко Г.А.',
  type: 'Встреча с клиентом',
  status: 'В работе',
  completion_date: '18 мая'
}, {
  priority: PRIORITY_COLOR.IMPORTANT_NOT_URGENT,
  name: 'Собрать документы у ИП Петров',
  executor: 'Колиниченко Г.А.',
  type: 'Встреча с клиентом',
  status: 'В работе',
  completion_date: '18 мая'
}, {
  priority: PRIORITY_COLOR.IMPORTANT_NOT_URGENT,
  name: 'Собрать документы у ИП Петров',
  executor: 'Колиниченко Г.А.',
  type: 'Встреча с клиентом',
  status: 'В работе',
  completion_date: '18 мая'
}, {
  priority: PRIORITY_COLOR.IMPORTANT_NOT_URGENT,
  name: 'Собрать документы у ИП Петров',
  executor: 'Колиниченко Г.А.',
  type: 'Встреча с клиентом',
  status: 'В работе',
  completion_date: '18 мая'
}, {
  priority: PRIORITY_COLOR.IMPORTANT_NOT_URGENT,
  name: 'Собрать документы у ИП Петров',
  executor: 'Колиниченко Г.А.',
  type: 'Встреча с клиентом',
  status: 'В работе',
  completion_date: '18 мая'
}, {
  priority: PRIORITY_COLOR.IMPORTANT_NOT_URGENT,
  name: 'Собрать документы у ИП Петров',
  executor: 'Колиниченко Г.А.',
  type: 'Встреча с клиентом',
  status: 'В работе',
  completion_date: '18 мая'
}, {
  priority: PRIORITY_COLOR.IMPORTANT_NOT_URGENT,
  name: 'Собрать документы у ИП Петров',
  executor: 'Колиниченко Г.А.',
  type: 'Встреча с клиентом',
  status: 'В работе',
  completion_date: '18 мая'
}, {
  priority: PRIORITY_COLOR.IMPORTANT_NOT_URGENT,
  name: 'Собрать документы у ИП Петров',
  executor: 'Колиниченко Г.А.',
  type: 'Встреча с клиентом',
  status: 'В работе',
  completion_date: '18 мая'
}, {
  priority: PRIORITY_COLOR.IMPORTANT_NOT_URGENT,
  name: 'Собрать документы у ИП Петров',
  executor: 'Колиниченко Г.А.',
  type: 'Встреча с клиентом',
  status: 'В работе',
  completion_date: '18 мая'
}, {
  priority: PRIORITY_COLOR.IMPORTANT_NOT_URGENT,
  name: 'Собрать документы у ИП Петров',
  executor: 'Колиниченко Г.А.',
  type: 'Встреча с клиентом',
  status: 'В работе',
  completion_date: '18 мая'
}, {
  priority: PRIORITY_COLOR.IMPORTANT_NOT_URGENT,
  name: 'Собрать документы у ИП Петров',
  executor: 'Колиниченко Г.А.',
  type: 'Встреча с клиентом',
  status: 'В работе',
  completion_date: '18 мая'
}];

