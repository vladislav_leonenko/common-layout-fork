import moment from "moment";

export interface calendarWidgetMockInterface {
  type: string,
  dateStart: string,
  dateEnd: string,
  title: string,
  description: string,
}

export const calendarWidgetMock: calendarWidgetMockInterface[] = [{
  type: 'red',
  dateStart: moment().set({
    hour: 9,
    minute: 30
  }).toISOString(),
  dateEnd: moment().set({
    hour: 10,
    minute: 0
  }).toISOString(),
  title: 'Стендап',
  description: 'Описание программы стендапа'
},{
  type: 'yellow',
  dateStart: moment().set({
    hour: 10,
    minute: 30
  }).toISOString(),
  dateEnd: moment().set({
    hour: 11,
    minute: 0
  }).toISOString(),
  title: 'Единая дизайн-система',
  description: 'Описание единой дизайн-системы'
},{
  type: 'green',
  dateStart: moment().set({
    hour: 11,
    minute: 0
  }).toISOString(),
  dateEnd: moment().set({
    hour: 13,
    minute: 30
  }).toISOString(),
  title: 'Знакомство с первой партией ИТ-команды',
  description: 'Описание знакомства с первой партией ИТ-команды'
},{
  type: 'blue',
  dateStart: moment().set({
    hour: 11,
    minute: 30
  }).toISOString(),
  dateEnd: moment().set({
    hour: 13,
    minute: 0
  }).toISOString(),
  title: 'Интервью дизайнера Иванова',
  description: 'Описание интервью дизайнера Иванова'
},{
  type: 'blue',
  dateStart: moment().set({
    hour: 11,
    minute: 30
  }).toISOString(),
  dateEnd: moment().set({
    hour: 12,
    minute: 30
  }).toISOString(),
  title: 'Интервью дизайнера Петрова',
  description: 'Описание интервью дизайнера Петрова'
},{
  type: 'blue',
  dateStart: moment().set({
    hour: 11,
    minute: 30
  }).toISOString(),
  dateEnd: moment().set({
    hour: 12,
    minute: 30
  }).toISOString(),
  title: 'Интервью дизайнера Петрова',
  description: 'Описание интервью дизайнера Петрова'
}];
