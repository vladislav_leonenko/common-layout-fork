const randomInteger = (min, max) => {
  let rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
};

export const mockKPI = {
  data: [
    {
      id: 1,
      title: 'KPI',
      value: ''
    }
  ],
  link: ''
};

const plus10Minites = 1000 * 60 * 10;
const plus15Minites = 1000 * 60 * 15;

const unallocatedError = {
  //@ts-ignore
  lead_created: new Date(Date.parse(new Date()) - plus15Minites).toISOString(),
  salepoint_assignment_date: new Date().toISOString(),
  assignment_deadline: new Date(new Date().getTime()).toISOString(),
  work_starting_deadline: new Date(new Date().getTime() - 1000).toISOString(),
  feedback_fixation_deadline: new Date(new Date().getTime() + 10000).toISOString()
};

const unallocatedWarning = {
  //@ts-ignore
  lead_created: new Date(Date.parse(new Date()) - plus10Minites).toISOString(),
  work_starting_deadline: new Date(new Date().getTime() + 5000).toISOString(),
  feedback_fixation_deadline: new Date(new Date().getTime() + plus10Minites + 5000).toISOString()
};

const unallocatedSuccess = {
  lead_created: new Date().toISOString(),
  work_starting_deadline: new Date(new Date().getTime() + plus10Minites).toISOString(),
  feedback_fixation_deadline: new Date(new Date().getTime() + plus15Minites).toISOString()
};

export const leadCardsMockUnallocated = {
  count_actual: randomInteger(5, 10),
  count_overdue: randomInteger(0, 4),
  data: [
    {
      lead_id: Math.random(),
      lead_task_name: 'Заявка на РКО 1',
      lead_company_name: 'ООО Льбина',
      lead_channel: 'REMOTE_CHANNEL',
      lead_created_date: unallocatedError.lead_created,
      contact_person_name: 'Иванов Иван',
      contact_person_phone: '+71234567890',
      salepoint_assignment_date: unallocatedError.salepoint_assignment_date,
      assignment_deadline: unallocatedError.assignment_deadline,
      work_starting_deadline: unallocatedError.work_starting_deadline,
      feedback_fixation_deadline: unallocatedError.feedback_fixation_deadline
    },
    {
      lead_id: Math.random(),
      lead_task_name: 'Заявка на РКО 2',
      lead_company_name: 'ООО Льбина',
      lead_channel: 'INCOMING_TRAFFIC',
      lead_created_date: unallocatedWarning.lead_created,
      contact_person_name: 'Иванов Иван',
      contact_person_phone: '+71234567890',
      //@ts-ignore
      salepoint_assignment_date: unallocatedWarning.salepoint_assignment_date,
      //@ts-ignore
      assignment_deadline: unallocatedWarning.assignment_deadline,
      work_starting_deadline: unallocatedWarning.work_starting_deadline,
      feedback_fixation_deadline: unallocatedWarning.feedback_fixation_deadline
    },
    {
      lead_id: Math.random(),
      lead_task_name: 'Заявка на РКО 3',
      lead_company_name: 'ООО Льбина',
      lead_channel: 'RECOMMENDATION',
      lead_created_date: unallocatedSuccess.lead_created,
      contact_person_name: 'Иванов Иван',
      contact_person_phone: '+71234567890',
      //@ts-ignore
      salepoint_assignment_date: unallocatedSuccess.salepoint_assignment_date,
      //@ts-ignore
      assignment_deadline: unallocatedSuccess.assignment_deadline,
      work_starting_deadline: unallocatedSuccess.work_starting_deadline,
      feedback_fixation_deadline: unallocatedSuccess.feedback_fixation_deadline
    }
  ]
};

export const leadCardsMockAssigned = {
  count_actual: randomInteger(5, 3),
  count_overdue: randomInteger(1, 2),
  data: [
    {
      lead_id: Math.random(),
      lead_task_name: 'Заявка на РКО 4',
      lead_company_name: 'ООО Льбина',
      lead_channel: 'CRM_CAMPAIGN',
      lead_created_date: unallocatedWarning.lead_created,
      contact_person_name: 'Иванов Иван',
      contact_person_phone: '+71234567890',
      //@ts-ignore
      salepoint_assignment_date: unallocatedWarning.salepoint_assignment_date,
      //@ts-ignore
      assignment_deadline: unallocatedWarning.assignment_deadline,
      work_starting_deadline: unallocatedWarning.work_starting_deadline,
      feedback_fixation_deadline: unallocatedWarning.feedback_fixation_deadline
    }
  ]
};

export const leadCardsMock = {
  id: 1,
  title: 'Заявка на Банковская гарантия',
  created: '15:40',
  timer: '4:29',
  companyName: 'ООО Льбина',
  fullName: 'Иванов Иван',
  leadChannel: 'CRM',
  timerType: 'success',
  titleType: 'default',
  links: [{ title: 'Назначить', href: '#', id: 12 }],
  branch: 'Московский',
  performer: 'Иванов В.В'
};
