export interface ItemInterface {
  icon: string,
  name: string,
  link: string,
  is_active?: boolean,
  is_hidden?: boolean
}

export const menuMock: ItemInterface[] = [{
  icon: 'main',
  name: 'Главная',
  link: '/',
}, {
  icon: 'calendar',
  name: 'Календарь',
  link: '/calendar',
}, {
  icon: 'tasks',
  name: 'Задачи',
  link: '/tasks',
}, {
  icon: 'processes',
  name: 'Процессы',
  link: '/processes',
}, {
  icon: 'analytics',
  name: 'Аналитика',
  link: '/analytics'
}, {
  icon: 'marketplace',
  name: 'Витрина',
  link: '/marketplace',
}, {
  icon: 'education',
  name: 'Обучение',
  link: '/education',
}, {
  icon: 'kpi',
  name: 'KPI',
  link: '/kpi',
}];
