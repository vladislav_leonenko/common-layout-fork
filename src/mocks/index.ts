export { menuMock } from './menu';
export { myTasksMock } from './tasks';
export { calendarWidgetMock } from './calendarWidget';
export { priorityPanelMock } from './priorityPanel';
export { linksData, contentNew } from './notificationPanel';
export { mockKPI, leadCardsMockUnallocated, leadCardsMockAssigned, leadCardsMock } from './leads';
