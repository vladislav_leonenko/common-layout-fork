import { LinkProps } from '../../src/components/atoms';

export const linksData: LinkProps[] = [
  { content: 'Взять в работу', href: '#' },
  { content: 'Назначить', href: '#' }
];

export const contentNew = {
  isNew: true,
  type: 'Лид на ТП',
  companyName: 'РКО — Рога и копыта'
};
