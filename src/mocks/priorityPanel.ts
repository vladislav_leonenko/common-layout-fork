export interface PriorityPanelItemInterface {
  data: { id: number; title: string; value: string | number; isWarning?: boolean }[];
  link: string;
}

export const priorityPanelMock: PriorityPanelItemInterface[] = [
  {
    data: [
      {
        id: 1,
        title: 'KPI по Р/С',
        value: '57%'
      }
    ],
    link: ''
  },
  {
    data: [
      {
        id: 1,
        title: 'Нераспределенные лиды',
        value: '11'
      },
      {
        id: 2,
        title: 'Просрочено',
        value: '10',
        isWarning: true
      }
    ],
    link: ''
  },
  {
    data: [
      {
        id: 1,
        title: 'Назначенные',
        value: '2'
      },
      {
        id: 2,
        title: 'Просрочено',
        value: '1',
        isWarning: true
      }
    ],
    link: ''
  }
];
