import * as React from 'react';
import style from './tasks.sass';
import block from 'bem-css-modules';
import { PriorityPanel } from '../../molecules/priorityPanel/priorityPanel';
import { PriorityPanelItem } from '../../atoms';
import { priorityPanelMock } from '../../../mocks/priorityPanel';
import { H1 } from '../../atoms/h1/h1';
import { Switch } from '../../atoms/switch/switch';
import { SelectBox } from '../../molecules/selectbox/selectbox';
import { TabBar } from '../../molecules/tabBar/tabBar';
import { TabBarItem } from '../../atoms/tabBarItem/tabBarItem';
import { Table } from '../../organisms/table/table';
import { myTasksTableMock } from '../../../mocks/tasks';
import { ColumnsTable, TasksTableItem } from '../../../types';

const b = block(style);

type TasksPageProps = {
  columns: any[];
};
type TasksPageState = {};

export class TasksPage extends React.Component<TasksPageProps, TasksPageState> {
  state = {
    only_is_active: false
  };

  handleOnOnlyIsActive = (name: string, checked: boolean) => {
    console.log({ name, checked });
  };

  render() {
    const { only_is_active } = this.state;
    const { columns } = this.props;

    return (
      <div>
        <PriorityPanel>
          {priorityPanelMock.map((item) => (
            <PriorityPanelItem key={Math.random()} {...item} />
          ))}
        </PriorityPanel>
        <div className={b(null)}>
          <div className="container-fluid no-padding">
            <div className="row middle-lg">
              <div className="col-lg-6">
                <H1>Задачи</H1>
              </div>
              <div className="col-lg-6 end-lg d-flex middle-lg">
                <div className={b('switcher')}>
                  <Switch name={'is_active'} onChange={this.handleOnOnlyIsActive} checked={only_is_active}>
                    Только активные
                  </Switch>
                </div>
                <div className={b('selectbox')}>
                  <SelectBox label={`Приоритет`} />
                </div>
                <div className={b('selectbox')}>
                  <SelectBox label={`Статус`} />
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-12">
                <TabBar>
                  <TabBarItem label={'Мои задачи'}>
                    <Table source={myTasksTableMock} columns={columns} onClick={(row: any) => console.log(row)} />
                  </TabBarItem>
                  <TabBarItem label={'Порученные мной'} />
                </TabBar>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
