import * as React from 'react';
import style from './datepickerInput.sass';
import block from 'bem-css-modules';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { IconCalendar } from '../../../assets/icons';

import MomentLocaleUtils from 'react-day-picker/moment';

import 'moment/locale/ru';

const b = block(style);

type DatepickerInputProps = {};
type DatepickerInputState = {};

export class DatepickerInput extends React.Component<DatepickerInputProps, DatepickerInputState> {
  render() {

    return <div className={b(null)}>
      <i className={b('icon')}>
        <IconCalendar/>
      </i>
      <DayPickerInput placeholder={'Укажите дату'}
                      format="D MMMM YYYY"
                      formatDate={MomentLocaleUtils.formatDate}
                      parseDate={MomentLocaleUtils.parseDate}
                      dayPickerProps={{
                        locale: 'ru',
                        localeUtils: MomentLocaleUtils,
                        modifiers: {
                          disabled: [
                            {
                              before: new Date()
                            }
                          ]
                        }
                      }}/>
    </div>;
  }
}
