import * as React from 'react';
import style from './timePicker.sass';
import block from 'bem-css-modules';
import Datetime from 'react-datetime';
import { IconClockSolid } from '../../../assets/icons';

const b = block(style);

type TimePickerProps = {};
type TimePickerState = {};

export class TimePicker extends React.Component<TimePickerProps, TimePickerState> {

  state = {
    value: ''
  };

  componentDidMount() {
    const datetime = new Date().setHours(10, 40);

    this.setState({
      value: datetime
    });
  }

  handleOnChange = (value) => {
    this.setState({
      value
    });
  };

  render() {
    const { value } = this.state;

    return <div className={b(null)}>
      <i className={b('icon')}>
        <IconClockSolid/>
      </i>
      <Datetime value={value}
                timeConstraints={{
                  minutes: {
                    min: 5,
                    max: 5,
                    step: 1
                  }
                }}
                dateFormat={false}
                timeFormat={'HH:mm'}
                initialViewMode={'time'}
                onChange={this.handleOnChange}/>
    </div>;
  }
}
