import * as React from 'react';
import style from './datepicker.sass';
import block from 'bem-css-modules';
import DayPicker, { DateUtils } from 'react-day-picker';
import MomentLocaleUtils from 'react-day-picker/moment';
import 'moment/locale/ru';
import moment from 'moment';
import { IconCalendarOutline } from '../../../assets/icons';

const b = block(style);

type DatePickerProps = {
  open: boolean,
  from: Date,
  to: Date,
  onChange: (date: { to: Date, from: Date }) => 0
};
type DatePickerState = {
  open: boolean,
  from: Date,
  to: Date
};

export class DatePicker extends React.Component<DatePickerProps, DatePickerState> {

  static defaultProps = {
    open: false,
    from: (new Date()),
    to: (new Date()),
    onChange: (date: DatePickerState) => ({})
  };

  state = {
    open: this.props.open,
    from: this.props.from,
    to: this.props.to
  };

  private wrapperRef = React.createRef<HTMLDivElement>();

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  handleToggle = () => this.setState((state) => ({ open: !state.open }));

  handleOnClose = () => this.setState(() => ({ open: false }));

  handleClickOutside = e => {
    if (this.wrapperRef && this.wrapperRef.current && !this.wrapperRef.current.contains(e.target)) {
      this.handleOnClose();
    }
  };

  handleOnChange = (day) => {
    const { onChange } = this.props;
    const range = DateUtils.addDayToRange(day, this.state);


    this.setState((state) => {
      return {
        to: (range.to === null) ? state.to : range.to,
        from: (range.from === null) ? state.from : range.from
      };
    }, () => {
      onChange({
        to: this.state.to,
        from: this.state.from
      });
    });
  };

  handleOnShowToggle = () => {
    this.setState(state => ({
      open: !state.open
    }));
  };

  render() {
    const { from, to, open } = this.state;
    const modifiers = { start: from, end: to };

    return <div className={b(null)}
                ref={this.wrapperRef}>
      <div className={b('date')}
           onClick={this.handleOnShowToggle}>
        {moment(from).format('D MMM')} - {moment(to).format('D MMM')},
        <span className={b('year')}>{moment(to).format('YYYY')}</span>
        <IconCalendarOutline/>
      </div>
      <div className={b('container', { open })}>
        <DayPicker {...this.props}
                   pagedNavigation
                   showOutsideDays
                   locale={'ru'}
                   localeUtils={MomentLocaleUtils}
                   modifiers={modifiers}
                   selectedDays={[from, { from, to }]}
                   onDayClick={this.handleOnChange}/>
      </div>

    </div>;
  }
}
