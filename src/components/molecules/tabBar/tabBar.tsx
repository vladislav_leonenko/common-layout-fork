import * as React from 'react';
import style from './tabBar.sass';
import block from 'bem-css-modules';
import { TabBarNav } from '../../atoms/tabBarNav/tabBarNav';

const b = block(style);

type TabBarProps = {
  children: React.ReactNode,
  className: string,
  vertical: boolean,
  onChange?: (name: any) => void
};
type TabBarState = {
  currentTab: number | string,
  activeTab: null | number | string
};

interface ChildrenInterface {
  label: string
}

export class TabBar extends React.Component<TabBarProps, TabBarState> {

  static defaultProps = {
    children: [],
    className: '',
    vertical: false
  };

  state = {
    currentTab: 0,
    activeTab: null
  };

  componentDidMount() {
    const { children } = this.props;
    const activeTab = this.getChildrenLabels(children)[0];

    this.setActiveTab(activeTab);
  }


  getChildrenLabels = (children: any) => {
    return children.map(({ props }: any) => props.label);
  };

  setActiveTab = (activeTab: number | string, name: number | string = '') => {
    const { onChange } = this.props;
    const { currentTab } = this.state;

    if (currentTab !== activeTab) {
      this.setState(() => ({ activeTab }));

      if (onChange) {
        onChange(name);
      }
    }
  };

  renderTabs = () => {
    const { children = [] } = this.props;
    const { activeTab } = this.state;

    return this.getChildrenLabels(children).map((label: string | number, name: string | number) => {
      return <TabBarNav key={name}
                        isActiveTab={(activeTab === label)}
                        label={label}
                        name={name}
                        onChangeTab={this.setActiveTab}/>;
    });
  };

  render() {
    const { activeTab } = this.state;
    const { children, className, vertical, ...attrs } = this.props;

    return <div className={b(null)}>
      <div className={b('navigation')}>
        {this.renderTabs()}
      </div>

      <div className={b('container')}>
        {React.Children.map(children, (child: any) => React.cloneElement(child, {
          isActiveTab: (activeTab === child.props.label)
        }))}
      </div>
    </div>;
  }
}
