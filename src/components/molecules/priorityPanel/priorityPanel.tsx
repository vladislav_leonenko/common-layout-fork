import * as React from 'react';
import style from './priorityPanel.sass';
import block from 'bem-css-modules';

const b = block(style);

export const PriorityPanel: React.FC<React.ReactNode> = ({ children }) => <div className={b(null)}>{children}</div>;
