import * as React from 'react'
import style from './widget-statistics.sass';
import block from 'bem-css-modules';

const b = block(style);

interface WidgetStatisticsProps {
  header: string,
  today: number,
  expired: number
}

export const WidgetStatistics: React.FC<WidgetStatisticsProps> = ({header, today, expired}) => {
  return (<div className={b(null)}>
    <div className={b('header')}>{header}</div>
    <div className={b('body')}>
      <div className={b('column')}>
        {today}
        <div className={b('text')}>На сегодня</div>
      </div>

      <div className={b('column', {expired: true})}>
        {expired}
        <div className={b('text')}>Просрочено</div>
      </div>
    </div>
  </div>);
}
