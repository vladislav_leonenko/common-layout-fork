import * as React from 'react';
import style from './sidebar.sass';
import block from 'bem-css-modules';
import { Logotype } from '../../atoms';
import { Menu } from '../menu/menu';
import { ItemInterface } from '../../../mocks/menu';

const b = block(style);

interface SidebarProps {
  list: ItemInterface[]
}

export const Sidebar: React.FC<SidebarProps> = ({ list }) => {
  return (<div className={b(null)}>
    <div className={b('logotype')}>
      <Logotype/>
    </div>
    <Menu list={list}/>
  </div>);
};
