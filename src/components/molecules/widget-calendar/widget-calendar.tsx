import * as React from 'react';
import style from './widget-calendar.sass';
import block from 'bem-css-modules';
import { Widget } from '../widget/widget';
import { IconCalendarOutline } from '../../../assets/icons';
import { CalendarList } from './list/list';
import { calendarWidgetMockInterface } from '../../../mocks/calendarWidget';

const b = block(style);

interface WidgetCalendarProps {
  list: calendarWidgetMockInterface[],
  link?: string,
  onClickHeader?: (link: string) => ({})

}

export const WidgetCalendar: React.FC<WidgetCalendarProps> = ({ list, link, onClickHeader }: WidgetCalendarProps) => {
  return (<div className={b(null)}>
    <Widget icon={<IconCalendarOutline/>} header={'Календарь'} link={link} onClickHeader={onClickHeader}>
      <CalendarList list={list}/>
    </Widget>
  </div>);
};
