import * as React from 'react';
import style from './list.sass';
import block from 'bem-css-modules';
import moment from 'moment';
import { CalendarItem } from './item';
import { calendarWidgetMockInterface } from '../../../../mocks/calendarWidget';
import { useEffect, useRef } from 'react';

const b = block(style);

interface CalendarListProps {
  list: calendarWidgetMockInterface[]
}

export const CalendarList: React.FC<CalendarListProps> = ({ list }) => {

  const containerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (containerRef.current) {
      // Количество текущих часов умножаем на высоту (80px)
      const scrollPixel = (new Date()).getHours() * 80;
      containerRef.current.scrollTo(0, scrollPixel);
    }
  }, []);

  const getDecimalIndents = (time: string) => {
    const arrTime = time.split(':');
    const hour = +arrTime[0];
    const minute = +arrTime[1];

    // Вычисляем минуты
    const percent = (minute * 100) / 60;
    return hour + (percent / 100);
  };

  const getHourToday = () => {

    let complete: any[] = [];

    return Array.from({ length: 24 }, (_, i) => i).reduce((r, hour) => {
      let dataHour: any = {
        list: []
      };
      const currentHour = parseInt(moment({ hour, minute: 0 }).format('H'));

      list.forEach((item) => {
        const timeStart = parseInt(moment(item.dateStart).format('H'));
        if (currentHour == timeStart && timeStart < (currentHour + 1)) {
          const durationTime = moment.utc(
            moment.duration(
              moment(item.dateEnd)
                .diff(moment(item.dateStart)))
              .asMilliseconds()
          ).format('H:mm');

          dataHour.list.push({
            ...item,
            length: getDecimalIndents(durationTime),
            timeStart: moment(item.dateStart).format('H:mm'),
            timeEnd: moment(item.dateEnd).format('H:mm')
          });
        }
      });

      r.push({
        hour: currentHour,
        ...dataHour
      });
      return r;
    }, complete);
  };


  return (<div className={b(null)} ref={containerRef}>
    {getHourToday().map((item) => {
      return <CalendarItem key={item.hour.toString()} {...item}/>;
    })}
  </div>);
};
