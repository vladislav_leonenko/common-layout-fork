import * as React from 'react';
import style from './list.sass';
import block from 'bem-css-modules';
import { ItemEvent } from './item-event';

const b = block(style);

interface CalendarItemProps {
  hour: number,
  list: any[],
}

export const CalendarItem: React.FC<CalendarItemProps> = ({ hour, list }) => {

  return (<div className={b('item')}>
    <div className={b('line')}>
      <div className={b('hour')}>{hour}</div>
      {list.map((item, index) => {
        return <ItemEvent key={item.id} indent={index} {...item}/>;
      })}
    </div>
  </div>);
};

