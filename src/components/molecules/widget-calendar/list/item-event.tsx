import * as React from 'react'
import style from './item-event.sass';
import block from 'bem-css-modules';
import {useRef} from "react";

const b = block(style);

interface ItemEventProps {
  id: string,
  indent: number,
  type: string,
  length: number,
  title: string,
  description: string,
  timeStart: string,
  timeEnd: string,
}

export const ItemEvent: React.FC<ItemEventProps> = ({id, indent, length, type, title, description, timeStart, timeEnd}) => {


  const elementRef = useRef<HTMLDivElement>(null);

  const getWidth = () => {
    const total = Math.abs((indent * 28) + 13);
    return `calc(100% - ${total}px`
  }

  const handleOnClick = (e: React.MouseEvent<HTMLDivElement>) => {
    e.preventDefault();
  }

  return (<>
    <div className={b('view')}>
      fdghdfs
    </div>
    <div onClick={handleOnClick}
         ref={elementRef}
         className={b(null, {[type]: true})} style={{
      left: (indent * 28) + 13,
      height: (length * 75),
      width: getWidth()
    }}>
      {title}
    </div>
  </>);
}
