import * as React from 'react'
import {useEffect, useState} from 'react'
import style from './menu.sass';
import block from 'bem-css-modules';
import {ItemInterface} from "../../../mocks/menu";
import {
  IconMain,
  IconCalendar,
  IconTasks,
  IconAnalytics,
  IconMarketplace,
  IconEducation,
  IconKPI,
  IconProcesses
} from '../../../assets/icons';
import {MenuItem} from "../../atoms";

const b = block(style);

interface MenuProps {
  list: ItemInterface[]
}

const mappingIcon = {
  'main': <IconMain/>,
  'calendar': <IconCalendar/>,
  'tasks': <IconTasks/>,
  'analytics': <IconAnalytics/>,
  'marketplace': <IconMarketplace/>,
  'education': <IconEducation/>,
  'kpi': <IconKPI/>,
  'processes': <IconProcesses/>
};

const getIcon = (icon: string): React.ReactNode => {
  // @ts-ignore
  return mappingIcon[icon];
}

export const Menu: React.FC<MenuProps> = ({list}) => {

  const [currentIcon, setIcon] = useState("main")

  const getCurrentSectionName = () => {
    let pathname = window.location.pathname
    let section =  pathname.substring(pathname.lastIndexOf("/") + 1)
    return section.length === 0 ? "main" : section
  }

  const handlerOnClick = (icon: string, link: string) => {
    let pathname = window.location.pathname
    if (!pathname.endsWith(link)) {
      window.location.pathname = pathname.substring(0, pathname.lastIndexOf("/")) + link
    }
    setIcon(icon)
  }

  useEffect(() => {
        setIcon(getCurrentSectionName())
      }, []
  )

  return (<div className={b(null)}>
    {list.map((item: ItemInterface, index): React.ReactNode => {
      return ( item.is_hidden ? null : <MenuItem key={item.icon}
                       {...item}
                       is_active={(currentIcon === item.icon)}
                       getIcon={getIcon}
                       onClick={handlerOnClick}/>)
    })}
  </div>);
}
