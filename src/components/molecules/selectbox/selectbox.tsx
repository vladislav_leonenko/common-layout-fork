import * as React from 'react';
import style from './selectbox.sass';
import block from 'bem-css-modules';
import { IconArrowDown } from '../../../assets/icons';
import { Checkbox, Option } from '../../atoms';

const b = block(style);

type SelectBoxProps = {
  label: string,
  list?: any[],
  multiple: boolean,
  placeholder: string | number,
  selected: [string | number] | string | number,
  onSelect: (value: string | number) => 0
};
type SelectBoxState = {
  open: boolean
};

export class SelectBox extends React.Component<SelectBoxProps, SelectBoxState> {

  static defaultProps = {
    label: '',
    multiple: false,
    placeholder: 'Не указан',
    selected: '',
    onSelect: (value) => ({})
  };

  state = {
    open: false
  };

  private wrapperRef = React.createRef<HTMLDivElement>();

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  handleToggle = () => this.setState((state) => ({ open: !state.open }));

  handleOnClose = () => this.setState(() => ({ open: false }));

  handleClickOutside = e => {
    if (this.wrapperRef && this.wrapperRef.current && !this.wrapperRef.current.contains(e.target)) {
      this.handleOnClose();
    }
  };

  handleOnSelect = (value) => {
    const { multiple, onSelect } = this.props;

    onSelect(value);

    if (!multiple) {
      this.handleOnClose();
      return;
    }
  };

  render() {
    const { placeholder, multiple, selected, children } = this.props;
    const { open } = this.state;

    return <div className={b(null, { open })}
                ref={this.wrapperRef}>
      <div className={b('select')}
           onClick={this.handleToggle}>
        {placeholder}
        <span className={b('arrow')}><IconArrowDown/></span>
      </div>

      <div className={b('list')}>
        {multiple && Array.isArray(selected)
          ? (React.Children.map(children, (child: any) => {

            return (
              <Option value={child.props.value}
                      onClick={this.handleOnSelect}>
                <Checkbox checked={selected.includes(child.props.value)}>
                  {child.props.children}
                </Checkbox>
              </Option>
            );

          }))
          : (React.Children.map(children, (child: any) => React.cloneElement(child, {
            onClick: this.handleOnSelect
          })))
        }
      </div>
    </div>;
  }
}
