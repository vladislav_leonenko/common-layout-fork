import * as React from 'react'
import style from './fullNameWithAvatar.sass';
import block from 'bem-css-modules';
import {Avatar} from "../../atoms/avatar/avatar";
import {FullName} from "../../atoms/fullName/fullName";

const b = block(style);

interface FullNameWithAvatarProps {
  image: string,
  fullName: string,
  post: string
}

export const FullNameWithAvatar: React.FC<FullNameWithAvatarProps> = ({image, fullName, post}) => {
  return (<div className={b(null)}>
    <div className={b('avatar')}>
      <Avatar image={image}/>
    </div>
    <FullName fullName={fullName} post={post}/>
  </div>);
}
