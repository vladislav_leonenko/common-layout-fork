import * as React from 'react';
import style from './widget.sass';
import block from 'bem-css-modules';
import { IconArrow } from '../../../assets/icons';

const b = block(style);

interface WidgetProps {
  header: string,
  link?: string,
  icon?: React.ReactNode,
  children?: React.ReactNode,
  onClickHeader?: (link: string) => ({})
}

export const Widget: React.FC<WidgetProps> = ({ header, link, icon, children, onClickHeader }: WidgetProps) => {

  const handleOnClick = () => {
    if (onClickHeader && link != null) {
        onClickHeader(link);
    }
  };

  return (<div className={b(null)}>
    <div className={b('header')} onClick={handleOnClick}>
      <div className={b('left')}>
        <div className={b('icon')}>{icon}</div>
        <span className={b('title')}>{header}</span>
      </div>

      <div className={b('right')}>
        <a href={link} className={b('link')}><IconArrow/></a>
      </div>
    </div>

    <div className={b('content')}>
      {children}
    </div>
  </div>);
};
