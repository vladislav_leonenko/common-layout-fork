import * as React from 'react'
import style from './header.sass';
import block from 'bem-css-modules';
import {FullNameWithAvatar} from "../fullNameWithAvatar/fullNameWithAvatar";
import {DateWithTime} from "../../atoms/dateWithTime/dateWithTime";
import {Faq} from "../../atoms/faq/faq";

const b = block(style);

interface HeaderProps {
  image: string,
  fullName: string,
  post: string
}

export const Header: React.FC<HeaderProps> = ({image, fullName, post}) => {
  return (<div className={b(null)}>
    <div className={b('left')}>
      <FullNameWithAvatar image={image} fullName={fullName} post={post}/>
    </div>
    <div className={b('right')}>
      <div className={b('date')}>
        <DateWithTime/>
      </div>
      <Faq/>
    </div>
  </div>);
}
