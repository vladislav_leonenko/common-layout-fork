import * as React from 'react';
import style from './widget-tasks.sass';
import block from 'bem-css-modules';
import { Widget } from '../widget/widget';
import { IconTasksOutline } from '../../../assets/icons';
import { WidgetStatistics } from '../widget-statistics/widget-statistics';
import { WidgetTaskListItem } from '../../atoms/widgetTaskListItem/widgetTaskListItem';
import { MyTasksWidgetInterface } from '../../../types';

const b = block(style);

interface WidgetTasksProps extends MyTasksWidgetInterface {
  link?: string,
  onClickHeader?: (link: string) => ({}),
  onClickTask: (id_task: string | number) => void
}

export const WidgetTasks: React.FC<WidgetTasksProps> = ({ my, received, link, onClickHeader, onClickTask }: WidgetTasksProps) => {
  return (<div className={b(null)}>
    <Widget header={'Задачи'} link={link} onClickHeader={onClickHeader} icon={<IconTasksOutline/>}>
      <div className="container-fluid no-padding">
        <div className="row">
          <div className="col-lg-6 col-md-6 col-sm-6">

            <WidgetStatistics header={'Мои задачи'} today={my.today} expired={my.expired}/>
            <div className={b('list')}>
              {my.list.map((task) => (
                <WidgetTaskListItem key={task.id}
                                    onClickTask={onClickTask}
                                    {...task}/>

              ))}
            </div>

          </div>
          <div className="col-lg-6 col-md-6 col-sm-6">
            <WidgetStatistics header={'Порученные мной'} today={received.today} expired={received.expired}/>
            {received.list.map((task) => (
              <WidgetTaskListItem key={task.id}
                                  onClickTask={onClickTask}
                                  {...task}/>
            ))}
          </div>
        </div>
      </div>
    </Widget>
  </div>);
};
