import * as React from 'react';
import style from './notificationPanel.sass';
import block from 'bem-css-modules';
import { Success, Warning } from '../../../assets/icons';
import { Link, LinkProps } from '../../atoms';

export interface NotificationPanelInterface {
  links?: LinkProps[];
  isSuccess?: boolean;
  content: {
    isNew: boolean;
    type: string;
    companyName: string;
  };
  onClick?: () => void;
}

const b = block(style);

export const NotificationPanel: React.FC<NotificationPanelInterface> = ({
  links,
  isSuccess = true,
  content: { isNew, type, companyName },
  onClick
}: NotificationPanelInterface) => {
  return (
    <div className={b(null, { warningBackground: !isSuccess })}>
      <div className={b('iconWrapper')}>{isSuccess ? <Success /> : <Warning />}</div>
      <div className={b('contentWrapper')}>
        <div className={b('contentTitle')}>
          <span className={b('contentTitleStatus', { warning: !isSuccess })}>({isNew ? 'Новый' : 'Просрочен'}) </span>
          {type}
        </div>
        <div className={b('contentCompany')}>{companyName}</div>
      </div>
      <div className={b('linksWrapper')}>
        {links && links.map(({ ...rest }) => <Link className={b('linkItem')} {...rest} />)}
      </div>
      <button onClick={onClick} className={b('button')} />
    </div>
  );
};
