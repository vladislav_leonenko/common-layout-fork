import * as React from 'react';
import style from './leadCard.sass';
import block from 'bem-css-modules';
import { Person, GetCash } from '../../../assets/icons';

const b = block(style);

export const LeadCard = ({
  id,
  title,
  created,
  timer = '4:29',
  companyName,
  fullName,
  leadChannel,
  timerType = 'success',
  titleType = 'default',
  links,
  branch,
  performer
}) => {
  return (
    <div className={b(null)} key={id}>
      <p className={b('title', { titleType })}>{title}</p>
      <span className={b('timer', { timerType })}>{timer}</span>
      <div className={b('detailsWrapper')}>
        {created && <span className={b('detailsItem')}>Дата подачи: {created}</span>}
        {branch && <span className={b('detailsItem')}>ТП: {branch}</span>}
        {performer && <span className={b('detailsItem')}>Исполнитель: {performer}</span>}
      </div>
      <p className={b('companyName')}>{companyName}</p>
      <div className={b('wrapper')}>
        <div className={b('wrapperIcon')}>
          <Person />
        </div>
        <p className={b('fullName')}>{fullName}</p>
      </div>
      <div className={b('wrapper')}>
        <div className={b('wrapperIcon')}>
          <GetCash />
        </div>
        <p className={b('leadChannel')}>{leadChannel}</p>
      </div>
      {links && (
        <div className={b('linksWrapper')}>
          {links.map(({ title, href, id }) => (
            <a className={b('link')} href={href} key={id}>
              {title}
            </a>
          ))}
        </div>
      )}
    </div>
  );
};
