import * as React from 'react';
import style from './table.sass';
import block from 'bem-css-modules';
import { ColumnsTable } from '../../../types';

const b = block(style);

type TableProps = {
  renderKey: string,
  source: any[],
  columns: ColumnsTable[],
  onClick: (row: any) => void
};
type TableState = {};

export class Table extends React.Component<TableProps, TableState> {

  static defaultProps = {
    renderKey: 'id',
    source: [],
    columns: []
  };

  render() {
    const { columns, source, renderKey, onClick } = this.props;
    let existsColumns: string[] = [];

    return (<table className={b(null)}>
      <thead>
      <tr className={b('row')}>
        {columns.map((column) => {
          existsColumns.push(column.index);
          return (
            <th key={column.key}
                style={{ width: column.width || 'auto' }}
                className={b('header')}>{column.title}</th>
          );
        })}
      </tr>
      </thead>

      <tbody>
      {source.map((item) => {
        return (<tr className={b('row', { cursor: true })}
                    key={item[renderKey]}
                    onClick={() => onClick(item)}>
          {columns.map((column, index) => {
            if (column.render) {
              return <td key={index} className={b('column')}>
                {column.render(item[column.index], item)}
              </td>;
            } else {
              return <td key={index} className={b('column')}>{item[column.index]}</td>;
            }
          })}
        </tr>);
      })}
      </tbody>
    </table>);
  }
}
