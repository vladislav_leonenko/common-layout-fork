import * as React from 'react';
import style from './layout.sass';
import block from 'bem-css-modules';
import { Sidebar, Header } from '../../molecules';
import { ItemInterface} from '../../../mocks/menu';

const b = block(style);

interface LayoutProps {
  image: string,
  fullName: string,
  post: string,
  menuItems: ItemInterface[]
  children?: React.ReactNode
}

export const Layout: React.FC<LayoutProps> = ({ image, fullName, post, menuItems, children }: LayoutProps) => {
  return (<div className={b(null)}>
    <div className={b('sidebar')}>
      <Sidebar list={menuItems}/>
    </div>
    <div className={b('content')}>
      <Header image={image} fullName={fullName} post={post}/>
      <main className={b('body')}>
        {children}
      </main>
    </div>
  </div>);
};
