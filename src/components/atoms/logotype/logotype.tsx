import * as React from 'react'
import style from './logotype.sass';
import block from 'bem-css-modules';
import {IconLogo} from '../../../assets/icons';

const b = block(style);

export const Logotype: React.FC = () => {
  return (<div className={b(null)}><IconLogo/></div>);
}
