import React from 'react';
import style from './avatar.sass';
import block from 'bem-css-modules';

const b = block(style);

interface AvatarProps {
  image?: string
}

export const Avatar: React.FC<AvatarProps> = ({ image = '' }: AvatarProps) => {
  return (<div className={b(null)}>
    {image.length > 0
      ? (<img src={image} className={b('image')}/>)
      : null}
  </div>);
};
