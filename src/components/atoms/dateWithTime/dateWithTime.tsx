import * as React from 'react';
import block from 'bem-css-modules';
import style from './dateWithTime.sass';
import moment from 'moment';

moment.locale('ru');
const b = block(style);

type DateWithTimeState = {
  datetime: string
};

export class DateWithTime extends React.Component<{}, DateWithTimeState> {

  state = {
    datetime: moment().format('D MMMM, H:mm')
  };

  componentDidMount() {
    setInterval(() => {
      this.updateTime();
    }, 1000);
  }

  updateTime = () => this.setState(() => ({ datetime: moment().format('D MMMM, H:mm') }));

  render() {
    const { datetime } = this.state;
    return <time dateTime={datetime} className={b(null)}>{datetime}</time>;
  }
}
