import * as React from 'react';
import style from './priorityColumn.sass';
import block from 'bem-css-modules';

const b = block(style);

type PriorityColumnProps = {
  color: string,
};
type PriorityColumnState = {};

export class PriorityColumn extends React.Component<PriorityColumnProps, PriorityColumnState> {
  render() {
    const { color } = this.props;

    return <span className={b(null, { [color]: true })}/>;
  }
}
