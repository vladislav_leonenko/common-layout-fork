import * as React from 'react';
import style from './switch.sass';
import block from 'bem-css-modules';

const b = block(style);

type SwitchProps = {
  children?: string | number | React.ReactNode,
  id?: string,
  checked: boolean,
  name: string,
  onChange: (name: string, checked: boolean) => void
};
type SwitchState = {};

export class Switch extends React.Component<SwitchProps, SwitchState> {

  static defaultProps = {
    id: `switch`,
    checked: false
  };

  handleOnChange = () => {
    const { name, onChange } = this.props;
    onChange(name, !this.props.checked);
  };

  render() {
    const { id, children, checked } = this.props;

    return <label className={b(null, { checked })}
                  htmlFor={id}>
      <span className={b('label')}>{children}</span>
      <input id={id}
             checked={checked}
             onChange={this.handleOnChange}
             className={b('input')}
             type="checkbox"/>
      <div className={b('container')}>
        <span className={b('slider')}/>
      </div>
    </label>;
  }
}
