import * as React from 'react';
import style from './tabBarNav.sass';
import block from 'bem-css-modules';

const b = block(style);

type TabBarNavProps = {
  label: string | number,
  isActiveTab: boolean,
  onChangeTab: (label: string | number, name: string | number) => void,
  name: string | number
};
type TabBarNavState = {};

export class TabBarNav extends React.Component<TabBarNavProps, TabBarNavState> {

  static defaultProps = {
    name: ''
  };

  handleOnChangeTab = (): void => {
    const { label, name, onChangeTab } = this.props;
    onChangeTab(label, name);
  };

  render() {
    const { label, isActiveTab } = this.props;
    return <button className={b(null, { isActiveTab })} onClick={this.handleOnChangeTab}>{label}</button>;
  }
}
