import * as React from 'react';
import style from './badge.sass';
import block from 'bem-css-modules';

const b = block(style);

type BadgeProps = {
  color: 'red' | 'yellow' | 'blue' | 'green' | 'gray',
  size: 'small' | 'medium' | 'large'
};
type BadgeState = {};

export class Badge extends React.Component<BadgeProps, BadgeState> {

  static defaultProps = {
    color: '',
    size: 'medium'
  };

  render() {
    const { color, size } = this.props;

    if (color.length === 0) {
      return false;
    }

    return <span className={b(null, { [size]: true, [color]: true })}/>;
  }
}
