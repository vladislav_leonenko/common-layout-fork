import * as React from 'react';
import style from './option.sass';
import block from 'bem-css-modules';

const b = block(style);

type OptionProps = {
  value: string | number,
  disabled?: boolean,
  onClick?: (value: string | number) => void
};
type OptionState = {};

export class Option extends React.Component<OptionProps, OptionState> {

  static defaultProps = {
    disabled: false
  };

  handleOnClick = (e) => {
    e.preventDefault();
    e.stopPropagation();

    const { value, disabled, onClick } = this.props;

    if (disabled) {
      return false;
    }

    if (onClick) {
      onClick(value);
    }
  };

  render() {
    const { children, disabled, ...props } = this.props;

    return <div {...props}
                onClick={this.handleOnClick}
                className={b(null, { disabled })}>
      {children}
    </div>;
  }
}
