import * as React from 'react';
import style from './checkbox.sass';
import block from 'bem-css-modules';

const b = block(style);

type CheckboxProps = {
  checked: boolean,
  disabled?: boolean,
  onChange: (checked: boolean) => void,
};
type CheckboxState = {};

export class Checkbox extends React.Component<CheckboxProps, CheckboxState> {

  static defaultProps = {
    checked: false,
    disabled: false,
    onChange: (checked: boolean) => 0
  };

  handleOnChange = e => {
    e.preventDefault();
    e.stopPropagation();
    const { onChange, disabled } = this.props;

    if (disabled) {
      return false;
    }

    onChange(!this.props.checked);
  };

  render() {
    const { checked, disabled, children } = this.props;

    return <label className={b('wrapper', { disabled })}>
      <span className={b(null, { checked })}>
        <input type="checkbox"
               checked={checked}
               onChange={this.handleOnChange}
               className={b('native')}/>
        <span className={b('inner')}/>
      </span>
      <span className={b('label')}>{children}</span>
    </label>;
  }
}
