import * as React from 'react';
import style from './fullName.sass';
import block from 'bem-css-modules';

const b = block(style);

interface FullNameProps {
  fullName: string,
  post: string
}

export const FullName: React.FC<FullNameProps> = ({ fullName, post }: FullNameProps) => {
  return (<div className={b(null)}>
    <div className={b('name')}>{fullName}</div>
    <div className={b('post')}>{post}</div>
  </div>);
};
