import * as React from 'react';
import style from './link.sass';
import block from 'bem-css-modules';

const b = block(style);

export interface LinkProps {
  href?: string;
  content?: string;
  type?: 'primary' | 'secondary';
  size?: '13' | '15';
  className?: string;
  beforeAdornment?: React.ReactNode;
  afterAdornment?: React.ReactNode;
}

export const Link: React.FC<LinkProps> = ({
  href,
  type = 'primary',
  size = '13',
  className,
  content,
  beforeAdornment,
  afterAdornment,
  ...rest
}: LinkProps) => {
  return (
    <>
      <a className={`${b({ type, size: size })} ${className && className}`} href={href} {...rest}>
        {beforeAdornment}
        {content}
        {afterAdornment}
      </a>
    </>
  );
};
