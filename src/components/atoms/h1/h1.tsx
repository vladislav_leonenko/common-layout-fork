import * as React from 'react';
import style from './h1.sass';
import block from 'bem-css-modules';

const b = block(style);

interface H1Props {
  children: React.ReactNode | string | number | any
}

export const H1: React.FC<H1Props> = ({ children }: H1Props) => {
  return (<h1 className={b(null)}>{children}</h1>);
};
