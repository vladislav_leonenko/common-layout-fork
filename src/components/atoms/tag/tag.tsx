import * as React from 'react';
import style from './tag.sass';
import block from 'bem-css-modules';

const b = block(style);

type TagProps = {
  name: string | number,
  onClick: (name: string | number) => 0
};
type TagState = {};

export class Tag extends React.Component<TagProps, TagState> {

  static defaultProps = {
    name: '',
    onClick: () => ({})
  };

  handleOnClick = (e: React.MouseEvent<HTMLButtonElement>)=> {
    const { name, onClick } = this.props;
    e.preventDefault();
    onClick(name);
  };

  render() {
    const { children } = this.props;

    return <span className={b(null)}>
      <span className={b('body')}>
        {children}
      </span>
      <button onClick={this.handleOnClick}
              className={b('remove')}/>
    </span>;
  }
}
