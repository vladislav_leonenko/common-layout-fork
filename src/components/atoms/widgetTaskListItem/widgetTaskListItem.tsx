import * as React from 'react';
import style from './widgetTaskListItem.sass';
import block from 'bem-css-modules';
import moment from 'moment';
import { TaskItemInterface } from '../../../types';
import { IconClockOutline } from '../../../assets/icons';

const b = block(style);

interface WidgetTaskListItemProps extends TaskItemInterface {
  onClickTask: (id_task: string | number) => void
}

export const WidgetTaskListItem: React.FC<WidgetTaskListItemProps> = ({ id, name, limitDate, badge, last, is_expired, onClickTask }: WidgetTaskListItemProps) => {

  const handleOnClick = () => {
    onClickTask(id);
  };

  return (<div onClick={handleOnClick}
               className={b(null, { [badge]: true, last })}>
    {(is_expired) ? <i className={b('expired')}><IconClockOutline/></i> : null}
    <div className={b('title')}>{name}</div>
    <time className={b('date')}>{moment(limitDate).format('DD MMMM, HH:mm')}</time>
  </div>);
};
