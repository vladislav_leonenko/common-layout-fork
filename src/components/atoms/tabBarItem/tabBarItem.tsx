import * as React from 'react';
import style from './tabBarItem.sass';
import block from 'bem-css-modules';

const b = block(style);

type TabBarItemProps = {
  children?: React.ReactNode,
  label: string | number,
  isActiveTab: boolean,
  name: string
};

export class TabBarItem extends React.Component<TabBarItemProps> {

  static defaultProps = {
    isActiveTab: false,
    label: '',
    name: ''
  };

  render() {
    const { children, label, isActiveTab, ...attrs } = this.props;

    return <div className={b(null, { isActiveTab })} {...attrs}>
      {children}
    </div>;
  }
}
