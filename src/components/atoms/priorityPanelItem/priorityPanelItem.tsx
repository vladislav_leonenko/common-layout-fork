import * as React from 'react';
import style from './priorityPanelItem.sass';
import block from 'bem-css-modules';
import { PriorityPanelItemInterface } from '../../../mocks/priorityPanel';
import { IconArrow } from '../../../assets/icons';

const b = block(style);

export const PriorityPanelItem: React.FC<PriorityPanelItemInterface> = ({ data, link, children, ...rest }) => (
  <div className={b(null)} {...rest}>
    <div className={b('wrapper')}>
      {data.map(({ id, title, value, isWarning, ...last }) => (
        <div key={id} className={b('innerItem', { attention: isWarning })} {...last}>
          {isWarning && '('}
          {title}:<span className={b('value', { attention: isWarning })}>{value}</span>
          {isWarning && ')'}
        </div>
      ))}
      <a href={link} className={b('link')}>
        <IconArrow />
      </a>
    </div>
    {children}
  </div>
);
