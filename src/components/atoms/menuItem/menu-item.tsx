import * as React from 'react';
import style from './menu-item.sass';
import block from 'bem-css-modules';
import { ItemInterface } from '../../../mocks/menu';

const b = block(style);

interface MenuItemProps extends ItemInterface {
  link: string,
  icon: string,
  is_active: boolean,
  name: string,
  getIcon: (icon: string) => React.ReactNode,
  onClick: (icon: string, link: string) => void
}

export const MenuItem: React.FC<MenuItemProps> = ({ link, icon, is_active, name, getIcon, onClick }: MenuItemProps) => {

  const handleOnClick = (e: React.MouseEvent<HTMLAnchorElement>) => {
    e.preventDefault();
    onClick(icon, link);
  };

  return (<div className={b(null)}>
    <a href={link} onClick={handleOnClick}
       className={b('link', { is_active })}>
      <div className={b('icon')}>
        {getIcon(icon)}
      </div>
      {name}
    </a>
  </div>);
};
