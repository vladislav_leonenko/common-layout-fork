import * as React from 'react'
import style from './faq.sass';
import block from 'bem-css-modules';
import {IconFaq} from '../../../assets/icons';

const b = block(style);

export const Faq: React.FC = () => {
  return (<a className={b(null)}>
    <IconFaq/>
  </a>);
}
