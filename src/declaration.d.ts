// Декларации для того, чтобы TS не ругался на импорты .scss .css и .svg файлов

declare module '*.sass' {
  const content: { [className: string]: string };
  export default content;
}
declare module '*.css' {
  const content: { [className: string]: string };
  export default content;
}

interface SvgrComponent
  extends React.FunctionComponent<React.SVGAttributes<SVGElement>> {}

declare module '*.svg' {
  const component: SvgrComponent;
  export default component;
}
