export interface TaskItemInterface {
  id: number,
  name: string,
  limitDate: Date,
  badge: string,
  last?: boolean,
  is_expired?: boolean
}

export interface MyTasksWidgetInterface {
  my: {
    today: number,
    expired: number,
    list: TaskItemInterface[]
  },
  received: {
    today: number,
    expired: number,
    list: TaskItemInterface[]
  }
}

export interface TasksTableItem {
  priority: string,
  title: string,
  executor: string,
  type: string,
  status: string,
  completion_date: string
}
