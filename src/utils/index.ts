export const PRIORITY_NAME = {
  IMPORTANT_URGENT: 'Важно/Срочно',
  IMPORTANT_NOT_URGENT: 'Важно/Не срочно',
  NOT_IMPORTANT_URGENT: 'Не важно/Срочно',
  NOT_IMPORTANT_NOT_URGENT: 'Не важно/Не срочно',
  NO_PRIORITY: 'Нет приоритета'
};

export const PRIORITY_COLOR = {
  IMPORTANT_URGENT: 'error',
  IMPORTANT_NOT_URGENT: 'attention',
  NOT_IMPORTANT_URGENT: 'primary',
  NOT_IMPORTANT_NOT_URGENT: 'success',
  NO_PRIORITY: 'neutral'
}
