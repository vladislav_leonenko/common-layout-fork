import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import typescript from 'rollup-plugin-typescript2';
import sourceMaps from 'rollup-plugin-sourcemaps';
import excludeDependenciesFromBundle from 'rollup-plugin-exclude-dependencies-from-bundle';
import smartAsset from 'rollup-plugin-smart-asset';
import postcss from 'rollup-plugin-postcss';
import json from 'rollup-plugin-json';
import svgr from '@svgr/rollup';
import pkg from './package.json';
import autoprefixer from 'autoprefixer';
import cssnano from 'cssnano';

export default [
  {
    input: 'src/index.ts',
    output: [
      {
        file: pkg.main,
        format: 'cjs',
        sourcemap: true
      },
      {
        file: pkg.module,
        format: 'es',
        sourcemap: true
      }
    ],
    external: [
      'react',
      'react-dom',
      'prop-types'
    ],
    plugins: [
      smartAsset({
        url: 'copy',
        useHash: false,
        keepName: true,
        assetsPath: 'img',
        include: ['**/*.png', '**/*.jpg', '**/*.jpeg']
      }),
      sourceMaps(),
      resolve({}),
      svgr({
        svgo: false,
        icon: false
      }),
      json(),
      postcss({
        modules: true,
        extract: 'ui-kit-stream.css',
        minimize: true,
        plugins: [
          autoprefixer({
            overrideBrowserslist: ['last 3 versions'],
            cascade: false
          }),
          cssnano()
        ]
      }),
      typescript({
        rollupCommonJSResolveHack: true,
        clean: !!process.env.CLEAN_BUILD,
        exclude: ['src/**/*.stories.tsx', 'src/**/*.test.(tsx|ts)']
      }),
      commonjs({}),
      excludeDependenciesFromBundle()
    ]
  }
];
