export declare const PRIORITY_NAME: {
    IMPORTANT_URGENT: string;
    IMPORTANT_NOT_URGENT: string;
    NOT_IMPORTANT_URGENT: string;
    NOT_IMPORTANT_NOT_URGENT: string;
    NO_PRIORITY: string;
};
export declare const PRIORITY_COLOR: {
    IMPORTANT_URGENT: string;
    IMPORTANT_NOT_URGENT: string;
    NOT_IMPORTANT_URGENT: string;
    NOT_IMPORTANT_NOT_URGENT: string;
    NO_PRIORITY: string;
};
