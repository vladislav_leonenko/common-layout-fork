import React from 'react';
interface AvatarProps {
    image?: string;
}
export declare const Avatar: React.FC<AvatarProps>;
export {};
