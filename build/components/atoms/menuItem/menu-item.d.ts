import * as React from 'react';
import { ItemInterface } from '../../../mocks/menu';
interface MenuItemProps extends ItemInterface {
    link: string;
    icon: string;
    is_active: boolean;
    name: string;
    getIcon: (icon: string) => React.ReactNode;
    onClick: (icon: string, link: string) => void;
}
export declare const MenuItem: React.FC<MenuItemProps>;
export {};
