import * as React from 'react';
declare type DateWithTimeState = {
    datetime: string;
};
export declare class DateWithTime extends React.Component<{}, DateWithTimeState> {
    state: {
        datetime: string;
    };
    componentDidMount(): void;
    updateTime: () => void;
    render(): JSX.Element;
}
export {};
