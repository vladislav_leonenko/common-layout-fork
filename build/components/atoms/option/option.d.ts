import * as React from 'react';
declare type OptionProps = {
    value: string | number;
    disabled?: boolean;
    onClick?: (value: string | number) => void;
};
declare type OptionState = {};
export declare class Option extends React.Component<OptionProps, OptionState> {
    static defaultProps: {
        disabled: boolean;
    };
    handleOnClick: (e: any) => false | undefined;
    render(): JSX.Element;
}
export {};
