import * as React from 'react';
declare type BadgeProps = {
    color: 'red' | 'yellow' | 'blue' | 'green' | 'gray';
    size: 'small' | 'medium' | 'large';
};
declare type BadgeState = {};
export declare class Badge extends React.Component<BadgeProps, BadgeState> {
    static defaultProps: {
        color: string;
        size: string;
    };
    render(): false | JSX.Element;
}
export {};
