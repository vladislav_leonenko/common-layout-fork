import * as React from 'react';
declare type TabBarItemProps = {
    children?: React.ReactNode;
    label: string | number;
    isActiveTab: boolean;
    name: string;
};
export declare class TabBarItem extends React.Component<TabBarItemProps> {
    static defaultProps: {
        isActiveTab: boolean;
        label: string;
        name: string;
    };
    render(): JSX.Element;
}
export {};
