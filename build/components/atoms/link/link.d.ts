import * as React from 'react';
export interface LinkProps {
    href?: string;
    content?: string;
    type?: 'primary' | 'secondary';
    size?: '13' | '15';
    className?: string;
    beforeAdornment?: React.ReactNode;
    afterAdornment?: React.ReactNode;
}
export declare const Link: React.FC<LinkProps>;
