import * as React from 'react';
declare type TagProps = {
    name: string | number;
    onClick: (name: string | number) => 0;
};
declare type TagState = {};
export declare class Tag extends React.Component<TagProps, TagState> {
    static defaultProps: {
        name: string;
        onClick: () => {};
    };
    handleOnClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
    render(): JSX.Element;
}
export {};
