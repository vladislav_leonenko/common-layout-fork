import * as React from 'react';
import { TaskItemInterface } from '../../../types';
interface WidgetTaskListItemProps extends TaskItemInterface {
    onClickTask: (id_task: string | number) => void;
}
export declare const WidgetTaskListItem: React.FC<WidgetTaskListItemProps>;
export {};
