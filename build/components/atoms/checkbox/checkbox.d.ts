import * as React from 'react';
declare type CheckboxProps = {
    checked: boolean;
    disabled?: boolean;
    onChange: (checked: boolean) => void;
};
declare type CheckboxState = {};
export declare class Checkbox extends React.Component<CheckboxProps, CheckboxState> {
    static defaultProps: {
        checked: boolean;
        disabled: boolean;
        onChange: (checked: boolean) => number;
    };
    handleOnChange: (e: any) => false | undefined;
    render(): JSX.Element;
}
export {};
