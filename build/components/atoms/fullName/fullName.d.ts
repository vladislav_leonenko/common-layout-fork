import * as React from 'react';
interface FullNameProps {
    fullName: string;
    post: string;
}
export declare const FullName: React.FC<FullNameProps>;
export {};
