import * as React from 'react';
declare type TabBarNavProps = {
    label: string | number;
    isActiveTab: boolean;
    onChangeTab: (label: string | number, name: string | number) => void;
    name: string | number;
};
declare type TabBarNavState = {};
export declare class TabBarNav extends React.Component<TabBarNavProps, TabBarNavState> {
    static defaultProps: {
        name: string;
    };
    handleOnChangeTab: () => void;
    render(): JSX.Element;
}
export {};
