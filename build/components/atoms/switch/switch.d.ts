import * as React from 'react';
declare type SwitchProps = {
    children?: string | number | React.ReactNode;
    id?: string;
    checked: boolean;
    name: string;
    onChange: (name: string, checked: boolean) => void;
};
declare type SwitchState = {};
export declare class Switch extends React.Component<SwitchProps, SwitchState> {
    static defaultProps: {
        id: string;
        checked: boolean;
    };
    handleOnChange: () => void;
    render(): JSX.Element;
}
export {};
