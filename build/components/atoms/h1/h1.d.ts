import * as React from 'react';
interface H1Props {
    children: React.ReactNode | string | number | any;
}
export declare const H1: React.FC<H1Props>;
export {};
