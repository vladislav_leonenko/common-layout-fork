import * as React from 'react';
declare type PriorityColumnProps = {
    color: string;
};
declare type PriorityColumnState = {};
export declare class PriorityColumn extends React.Component<PriorityColumnProps, PriorityColumnState> {
    render(): JSX.Element;
}
export {};
