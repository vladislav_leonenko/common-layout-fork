import * as React from 'react';
import 'moment/locale/ru';
declare type DatepickerInputProps = {};
declare type DatepickerInputState = {};
export declare class DatepickerInput extends React.Component<DatepickerInputProps, DatepickerInputState> {
    render(): JSX.Element;
}
export {};
