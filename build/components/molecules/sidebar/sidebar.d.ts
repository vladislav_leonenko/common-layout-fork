import * as React from 'react';
import { ItemInterface } from '../../../mocks/menu';
interface SidebarProps {
    list: ItemInterface[];
}
export declare const Sidebar: React.FC<SidebarProps>;
export {};
