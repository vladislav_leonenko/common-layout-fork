import * as React from 'react';
declare type TabBarProps = {
    children: React.ReactNode;
    className: string;
    vertical: boolean;
    onChange?: (name: any) => void;
};
declare type TabBarState = {
    currentTab: number | string;
    activeTab: null | number | string;
};
export declare class TabBar extends React.Component<TabBarProps, TabBarState> {
    static defaultProps: {
        children: never[];
        className: string;
        vertical: boolean;
    };
    state: {
        currentTab: number;
        activeTab: null;
    };
    componentDidMount(): void;
    getChildrenLabels: (children: any) => any;
    setActiveTab: (activeTab: number | string, name?: number | string) => void;
    renderTabs: () => any;
    render(): JSX.Element;
}
export {};
