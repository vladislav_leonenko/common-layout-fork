import * as React from 'react';
interface WidgetStatisticsProps {
    header: string;
    today: number;
    expired: number;
}
export declare const WidgetStatistics: React.FC<WidgetStatisticsProps>;
export {};
