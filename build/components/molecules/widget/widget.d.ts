import * as React from 'react';
interface WidgetProps {
    header: string;
    link?: string;
    icon?: React.ReactNode;
    children?: React.ReactNode;
    onClickHeader?: (link: string) => ({});
}
export declare const Widget: React.FC<WidgetProps>;
export {};
