import * as React from 'react';
import { calendarWidgetMockInterface } from '../../../../mocks/calendarWidget';
interface CalendarListProps {
    list: calendarWidgetMockInterface[];
}
export declare const CalendarList: React.FC<CalendarListProps>;
export {};
