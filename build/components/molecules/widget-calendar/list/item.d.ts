import * as React from 'react';
interface CalendarItemProps {
    hour: number;
    list: any[];
}
export declare const CalendarItem: React.FC<CalendarItemProps>;
export {};
