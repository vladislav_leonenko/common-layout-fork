import * as React from 'react';
interface ItemEventProps {
    id: string;
    indent: number;
    type: string;
    length: number;
    title: string;
    description: string;
    timeStart: string;
    timeEnd: string;
}
export declare const ItemEvent: React.FC<ItemEventProps>;
export {};
