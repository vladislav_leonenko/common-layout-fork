import * as React from 'react';
import { calendarWidgetMockInterface } from '../../../mocks/calendarWidget';
interface WidgetCalendarProps {
    list: calendarWidgetMockInterface[];
    link?: string;
    onClickHeader?: (link: string) => ({});
}
export declare const WidgetCalendar: React.FC<WidgetCalendarProps>;
export {};
