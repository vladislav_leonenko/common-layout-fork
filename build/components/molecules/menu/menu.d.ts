import * as React from 'react';
import { ItemInterface } from "../../../mocks/menu";
interface MenuProps {
    list: ItemInterface[];
}
export declare const Menu: React.FC<MenuProps>;
export {};
