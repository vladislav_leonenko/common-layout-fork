import * as React from 'react';
interface FullNameWithAvatarProps {
    image: string;
    fullName: string;
    post: string;
}
export declare const FullNameWithAvatar: React.FC<FullNameWithAvatarProps>;
export {};
