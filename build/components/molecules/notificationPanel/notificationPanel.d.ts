import * as React from 'react';
import { LinkProps } from '../../atoms';
export interface NotificationPanelInterface {
    links?: LinkProps[];
    isSuccess?: boolean;
    content: {
        isNew: boolean;
        type: string;
        companyName: string;
    };
    onClick?: () => void;
}
export declare const NotificationPanel: React.FC<NotificationPanelInterface>;
