import * as React from 'react';
import 'moment/locale/ru';
declare type DatePickerProps = {
    open: boolean;
    from: Date;
    to: Date;
    onChange: (date: {
        to: Date;
        from: Date;
    }) => 0;
};
declare type DatePickerState = {
    open: boolean;
    from: Date;
    to: Date;
};
export declare class DatePicker extends React.Component<DatePickerProps, DatePickerState> {
    static defaultProps: {
        open: boolean;
        from: Date;
        to: Date;
        onChange: (date: DatePickerState) => {};
    };
    state: {
        open: boolean;
        from: Date;
        to: Date;
    };
    private wrapperRef;
    componentDidMount(): void;
    componentWillUnmount(): void;
    handleToggle: () => void;
    handleOnClose: () => void;
    handleClickOutside: (e: any) => void;
    handleOnChange: (day: any) => void;
    handleOnShowToggle: () => void;
    render(): JSX.Element;
}
export {};
