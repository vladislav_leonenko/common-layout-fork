import * as React from 'react';
declare type TimePickerProps = {};
declare type TimePickerState = {};
export declare class TimePicker extends React.Component<TimePickerProps, TimePickerState> {
    state: {
        value: string;
    };
    componentDidMount(): void;
    handleOnChange: (value: any) => void;
    render(): JSX.Element;
}
export {};
