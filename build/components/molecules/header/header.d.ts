import * as React from 'react';
interface HeaderProps {
    image: string;
    fullName: string;
    post: string;
}
export declare const Header: React.FC<HeaderProps>;
export {};
