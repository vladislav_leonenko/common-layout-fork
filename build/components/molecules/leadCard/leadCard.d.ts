/// <reference types="react" />
export declare const LeadCard: ({ id, title, created, timer, companyName, fullName, leadChannel, timerType, titleType, links, branch, performer }: {
    id: any;
    title: any;
    created: any;
    timer?: string | undefined;
    companyName: any;
    fullName: any;
    leadChannel: any;
    timerType?: string | undefined;
    titleType?: string | undefined;
    links: any;
    branch: any;
    performer: any;
}) => JSX.Element;
