import * as React from 'react';
import { MyTasksWidgetInterface } from '../../../types';
interface WidgetTasksProps extends MyTasksWidgetInterface {
    link?: string;
    onClickHeader?: (link: string) => ({});
    onClickTask: (id_task: string | number) => void;
}
export declare const WidgetTasks: React.FC<WidgetTasksProps>;
export {};
