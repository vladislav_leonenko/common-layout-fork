import * as React from 'react';
declare type SelectBoxProps = {
    label: string;
    list?: any[];
    multiple: boolean;
    placeholder: string | number;
    selected: [string | number] | string | number;
    onSelect: (value: string | number) => 0;
};
declare type SelectBoxState = {
    open: boolean;
};
export declare class SelectBox extends React.Component<SelectBoxProps, SelectBoxState> {
    static defaultProps: {
        label: string;
        multiple: boolean;
        placeholder: string;
        selected: string;
        onSelect: (value: any) => {};
    };
    state: {
        open: boolean;
    };
    private wrapperRef;
    componentDidMount(): void;
    componentWillUnmount(): void;
    handleToggle: () => void;
    handleOnClose: () => void;
    handleClickOutside: (e: any) => void;
    handleOnSelect: (value: any) => void;
    render(): JSX.Element;
}
export {};
