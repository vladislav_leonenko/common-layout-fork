import * as React from 'react';
import { ColumnsTable } from '../../../types';
declare type TableProps = {
    renderKey: string;
    source: any[];
    columns: ColumnsTable[];
    onClick: (row: any) => void;
};
declare type TableState = {};
export declare class Table extends React.Component<TableProps, TableState> {
    static defaultProps: {
        renderKey: string;
        source: never[];
        columns: never[];
    };
    render(): JSX.Element;
}
export {};
