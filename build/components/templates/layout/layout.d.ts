import * as React from 'react';
import { ItemInterface } from '../../../mocks/menu';
interface LayoutProps {
    image: string;
    fullName: string;
    post: string;
    menuItems: ItemInterface[];
    children?: React.ReactNode;
}
export declare const Layout: React.FC<LayoutProps>;
export {};
