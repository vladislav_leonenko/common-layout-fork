import * as React from 'react';
declare type TasksPageProps = {
    columns: any[];
};
declare type TasksPageState = {};
export declare class TasksPage extends React.Component<TasksPageProps, TasksPageState> {
    state: {
        only_is_active: boolean;
    };
    handleOnOnlyIsActive: (name: string, checked: boolean) => void;
    render(): JSX.Element;
}
export {};
