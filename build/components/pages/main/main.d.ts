import * as React from 'react';
import { calendarWidgetMockInterface } from '../../../mocks/calendarWidget';
import { MyTasksWidgetInterface } from '../../../types';
interface MainPageProps {
    tasksList: MyTasksWidgetInterface;
    calendarList: calendarWidgetMockInterface[];
}
export declare const MainPage: React.FC<MainPageProps>;
export {};
