import { MyTasksWidgetInterface } from '../types';
export declare const myTasksMock: MyTasksWidgetInterface;
export declare const myTasksTableMock: {
    priority: string;
    name: string;
    executor: string;
    type: string;
    status: string;
    completion_date: string;
}[];
