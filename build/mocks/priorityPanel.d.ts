export interface PriorityPanelItemInterface {
    data: {
        id: number;
        title: string;
        value: string | number;
        isWarning?: boolean;
    }[];
    link: string;
}
export declare const priorityPanelMock: PriorityPanelItemInterface[];
