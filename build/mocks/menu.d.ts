export interface ItemInterface {
    icon: string;
    name: string;
    link: string;
    is_active?: boolean;
    is_hidden?: boolean;
}
export declare const menuMock: ItemInterface[];
