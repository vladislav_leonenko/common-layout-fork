export interface calendarWidgetMockInterface {
    type: string;
    dateStart: string;
    dateEnd: string;
    title: string;
    description: string;
}
export declare const calendarWidgetMock: calendarWidgetMockInterface[];
