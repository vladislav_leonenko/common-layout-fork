export declare const mockKPI: {
    data: {
        id: number;
        title: string;
        value: string;
    }[];
    link: string;
};
export declare const leadCardsMockUnallocated: {
    count_actual: number;
    count_overdue: number;
    data: {
        lead_id: number;
        lead_task_name: string;
        lead_company_name: string;
        lead_channel: string;
        lead_created_date: string;
        contact_person_name: string;
        contact_person_phone: string;
        salepoint_assignment_date: any;
        assignment_deadline: any;
        work_starting_deadline: string;
        feedback_fixation_deadline: string;
    }[];
};
export declare const leadCardsMockAssigned: {
    count_actual: number;
    count_overdue: number;
    data: {
        lead_id: number;
        lead_task_name: string;
        lead_company_name: string;
        lead_channel: string;
        lead_created_date: string;
        contact_person_name: string;
        contact_person_phone: string;
        salepoint_assignment_date: any;
        assignment_deadline: any;
        work_starting_deadline: string;
        feedback_fixation_deadline: string;
    }[];
};
export declare const leadCardsMock: {
    id: number;
    title: string;
    created: string;
    timer: string;
    companyName: string;
    fullName: string;
    leadChannel: string;
    timerType: string;
    titleType: string;
    links: {
        title: string;
        href: string;
        id: number;
    }[];
    branch: string;
    performer: string;
};
