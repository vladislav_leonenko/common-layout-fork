import React, { createElement, Component, Fragment, useState, useEffect, Children, cloneElement, createRef, useRef } from 'react';
import moment from 'moment';
import DayPicker, { DateUtils } from 'react-day-picker';
import MomentLocaleUtils from 'react-day-picker/moment';
import 'moment/locale/ru';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import Datetime from 'react-datetime';

var style = {"avatar":"avatar_avatar__v3zX3","avatar__image":"avatar_avatar__image__3Rpsk"};

function unwrapExports (x) {
	return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
}

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var bemCssModules = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
var isDev = process.env.NODE_ENV !== 'production';
var settings = {
    throwOnError: false,
    elementDelimiter: '__',
    modifierDelimiter: '_'
};
/**
 * Base function for bem naming with css modules
 * @param {Object} cssModule. Imported css module
 * @param {String} name. BEM name
 * @param {String} [element]
 * @param {Object} [mods]
 * @param {Object} [states]
 * @return {String}
 */
function block(cssModule, name, elementParam, modsParam, statesParam) {
    var isElementAsModes = elementParam && typeof elementParam === 'object';
    var mods = isElementAsModes ? elementParam : modsParam;
    var states = isElementAsModes ? modsParam : statesParam;
    var element = isElementAsModes ? '' : elementParam;
    var modifierDelimiter = settings.modifierDelimiter, elementDelimiter = settings.elementDelimiter, throwOnError = settings.throwOnError;
    var baseBlock = element ? "" + name + elementDelimiter + element : name;
    var result = cssModule[baseBlock] || '';
    if (isDev) {
        if (!result && !mods) {
            var message = "There is no " + name + elementDelimiter + element + " in cssModule";
            if (throwOnError) {
                throw Error(message);
            }
            else {
                console.warn(message);
                return '';
            }
        }
    }
    if (mods) {
        result = Object.keys(mods)
            .reduce(function (acc, next) {
            var modValue = mods[next];
            var mod;
            if (modValue === undefined) {
                return acc;
            }
            if (typeof modValue === 'boolean') {
                if (isDev) {
                    if (!("" + baseBlock + modifierDelimiter + next in cssModule)) {
                        var message = "There is no " + baseBlock + modifierDelimiter + next + " in cssModule";
                        if (throwOnError) {
                            throw Error(message);
                        }
                        else {
                            console.warn(message);
                            return acc;
                        }
                    }
                }
                if (modValue) {
                    mod = cssModule["" + baseBlock + modifierDelimiter + next];
                }
                else {
                    return acc;
                }
            }
            else {
                var currentMode = "" + baseBlock + modifierDelimiter + next + modifierDelimiter + mods[next];
                if (isDev) {
                    if (!(currentMode in cssModule)) {
                        var message = "There is no " + currentMode + " in cssModule";
                        if (throwOnError) {
                            throw Error(message);
                        }
                        else {
                            console.warn(message);
                            return acc;
                        }
                    }
                }
                mod = cssModule[currentMode];
            }
            return acc + " " + mod;
        }, result);
    }
    if (states) {
        result = Object.keys(states)
            .reduce(function (acc, next) {
            if (!states[next]) {
                return acc;
            }
            var state = cssModule["is-" + next];
            if (!state) {
                var message = "There is no is-" + next + " in cssModule";
                if (throwOnError) {
                    throw Error(message);
                }
                else {
                    console.warn(message);
                    return acc;
                }
            }
            return acc + " " + state;
        }, result);
    }
    return result.trim();
}
var extractModuleName = function (cssModule) {
    if (isDev) {
        if (!cssModule || typeof cssModule !== 'object' || Array.isArray(cssModule)) {
            var message = 'cssModule object should be an Object with keys';
            if (settings.throwOnError) {
                throw Error(message);
            }
            else {
                console.warn(message);
                return '';
            }
        }
    }
    var name = Object.keys(cssModule)[0];
    if (isDev) {
        if (!name) {
            var message = 'cssModule has no keys';
            if (settings.throwOnError) {
                throw Error(message);
            }
            else {
                console.warn(message);
                return '';
            }
        }
    }
    var indexElement = name.indexOf(settings.elementDelimiter);
    if (indexElement !== -1) {
        name = name.slice(0, indexElement);
    }
    var indexModifier = name.indexOf(settings.modifierDelimiter);
    if (indexModifier !== -1) {
        name = name.slice(0, indexModifier);
    }
    return name;
};
var bem = function (cssModule, name) {
    return block.bind(null, cssModule, name || extractModuleName(cssModule));
};
bem.setSettings = function (newSettings) {
    return Object.assign(settings, newSettings);
};
exports.default = bem;
});

var block = unwrapExports(bemCssModules);

var b = block(style);
var Avatar = function (_a) {
    var _b = _a.image, image = _b === void 0 ? '' : _b;
    return (React.createElement("div", { className: b(null) }, image.length > 0
        ? (React.createElement("img", { src: image, className: b('image') }))
        : null));
};

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

var style$1 = {"dateWithTime":"dateWithTime_dateWithTime__PO7rP"};

moment.locale('ru');
var b$1 = block(style$1);
var DateWithTime = /** @class */ (function (_super) {
    __extends(DateWithTime, _super);
    function DateWithTime() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            datetime: moment().format('D MMMM, H:mm')
        };
        _this.updateTime = function () { return _this.setState(function () { return ({ datetime: moment().format('D MMMM, H:mm') }); }); };
        return _this;
    }
    DateWithTime.prototype.componentDidMount = function () {
        var _this = this;
        setInterval(function () {
            _this.updateTime();
        }, 1000);
    };
    DateWithTime.prototype.render = function () {
        var datetime = this.state.datetime;
        return createElement("time", { dateTime: datetime, className: b$1(null) }, datetime);
    };
    return DateWithTime;
}(Component));

var style$2 = {"faq":"faq_faq__G8Ud3"};

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var _ref = /*#__PURE__*/createElement("path", {
  d: "M16.078 2.68188C16.078 3.78645 15.1826 4.68188 14.078 4.68188C13.7418 4.68188 13.425 4.59895 13.147 4.45241L10.8605 6.57157C10.9505 6.79918 11 7.04726 11 7.30688C11 8.41145 10.1046 9.30688 8.99999 9.30688C7.89542 9.30688 6.99999 8.41145 6.99999 7.30688C6.99999 7.24186 7.0031 7.17756 7.00916 7.11412L5.25496 6.17284C4.90115 6.4894 4.43399 6.68188 3.92187 6.68188C2.8173 6.68188 1.92187 5.78645 1.92187 4.68188C1.92187 3.57732 2.8173 2.68188 3.92187 2.68188C5.02644 2.68188 5.92187 3.57732 5.92187 4.68188C5.92187 4.80356 5.911 4.92269 5.89019 5.03837L7.55023 5.92913C7.91461 5.54583 8.42939 5.30688 8.99999 5.30688C9.37035 5.30688 9.7172 5.40755 10.0147 5.58302L12.2553 3.50634C12.1414 3.25495 12.078 2.97582 12.078 2.68188C12.078 1.57732 12.9734 0.681885 14.078 0.681885C15.1826 0.681885 16.078 1.57732 16.078 2.68188Z",
  fill: "#6FA3FF"
});

var _ref2 = /*#__PURE__*/createElement("path", {
  d: "M15.7305 7.68191V15.6881H17.0664C17.4254 15.6881 17.7164 15.9791 17.7164 16.3381C17.7164 16.6971 17.4254 16.9881 17.0664 16.9881H14.7903L14.7805 16.9882H13.3805L13.3707 16.9881H9.7104L9.70059 16.9882H8.29943L8.28962 16.9881H4.62788L4.61807 16.9882H3.21807L3.20827 16.9881H0.9336C0.574615 16.9881 0.2836 16.6971 0.2836 16.3381C0.2836 15.9791 0.574615 15.6881 0.9336 15.6881H2.26807L2.26807 9.68191C2.26807 8.77064 3.0068 8.03191 3.91807 8.03191C4.82934 8.03191 5.56807 8.77064 5.56807 9.68191L5.56807 15.6881H7.34943V12.245C7.34943 11.3334 8.08842 10.5944 9.00001 10.5944C9.9116 10.5944 10.6506 11.3334 10.6506 12.245V15.6881H12.4305V7.68191C12.4305 6.77064 13.1693 6.03191 14.0805 6.03191C14.9918 6.03191 15.7305 6.77064 15.7305 7.68191Z",
  fill: "#6FA3FF"
});

function SvgAnalytics(props) {
  return /*#__PURE__*/createElement("svg", _extends({
    width: 18,
    height: 17,
    viewBox: "0 0 18 17",
    fill: "none"
  }, props), _ref, _ref2);
}

function _extends$1() { _extends$1 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$1.apply(this, arguments); }

var _ref$1 = /*#__PURE__*/createElement("path", {
  d: "M4.89008 1.59995C4.89008 1.24097 4.59907 0.949951 4.24008 0.949951C3.8811 0.949951 3.59008 1.24097 3.59008 1.59995V2.98996H3.1001C1.4401 2.98996 0.100098 4.32996 0.100098 5.98996V7.64996C0.100098 8.03996 0.410098 8.34996 0.800098 8.34996H15.4001C15.7901 8.34996 16.1001 8.03996 16.1001 7.64996V5.98996C16.1001 4.32996 14.7501 2.98996 13.1001 2.98996H12.6001V1.59995C12.6001 1.24097 12.3091 0.949951 11.9501 0.949951C11.5911 0.949951 11.3001 1.24097 11.3001 1.59995V2.98996H8.75008V1.59995C8.75008 1.24097 8.45907 0.949951 8.10008 0.949951C7.7411 0.949951 7.45008 1.24097 7.45008 1.59995V2.98996H4.89008V1.59995Z",
  fill: "#307FFF"
});

var _ref2$1 = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M0.800098 9.64996H15.4001C15.7801 9.64996 16.1001 9.95996 16.1001 10.35V16.12C16.1001 17.78 14.7601 19.12 13.1001 19.12H3.1001C1.4401 19.12 0.100098 17.78 0.100098 16.12V10.35C0.100098 9.95996 0.410098 9.64996 0.800098 9.64996ZM6.52009 15.52C6.91009 15.52 7.22009 15.21 7.22009 14.82V13.5C7.22009 13.11 6.91009 12.8 6.52009 12.8H4.20009C3.81009 12.8 3.50009 13.11 3.50009 13.5V14.82C3.50009 15.21 3.81009 15.52 4.20009 15.52H6.52009Z",
  fill: "#307FFF"
});

function SvgCalendar(props) {
  return /*#__PURE__*/createElement("svg", _extends$1({
    width: 17,
    height: 20,
    viewBox: "0 0 17 20",
    fill: "none"
  }, props), _ref$1, _ref2$1);
}

function _extends$2() { _extends$2 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$2.apply(this, arguments); }

var _ref$2 = /*#__PURE__*/createElement("path", {
  d: "M0.717367 7.24084C-0.217656 6.76145 -0.227126 5.42837 0.700989 4.93574L8.59349 0.74656C9.47305 0.279711 10.5269 0.279711 11.4065 0.74656L19.299 4.93574C19.679 5.13747 19.9019 5.48014 19.9683 5.84741C19.9889 5.91071 20 5.97828 20 6.04844L20.0281 12.3469C20.0281 12.7059 19.7371 12.9969 19.3781 12.9969C19.0191 12.9969 18.7281 12.7059 18.7281 12.3469L18.7067 7.53613L11.3687 11.2983C10.5094 11.7388 9.49058 11.7388 8.6313 11.2983L0.717367 7.24084Z",
  fill: "#6FA3FF"
});

var _ref2$2 = /*#__PURE__*/createElement("path", {
  d: "M3.32999 10.5321C3.32999 10.3078 3.56722 10.1628 3.76686 10.2651L8.03821 12.4551C9.26985 13.0865 10.7302 13.0865 11.9618 12.4551L16.1931 10.2857C16.3927 10.1833 16.63 10.3283 16.63 10.5526V12.8656C16.63 13.7695 16.2294 14.627 15.5361 15.207C12.3204 17.8971 7.63953 17.8971 4.42392 15.207C3.7306 14.627 3.32999 13.7695 3.32999 12.8656V10.5321Z",
  fill: "#6FA3FF"
});

function SvgEducation(props) {
  return /*#__PURE__*/createElement("svg", _extends$2({
    width: 21,
    height: 18,
    viewBox: "0 0 21 18",
    fill: "none"
  }, props), _ref$2, _ref2$2);
}

function _extends$3() { _extends$3 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$3.apply(this, arguments); }

var _ref$3 = /*#__PURE__*/createElement("path", {
  d: "M5 9.2932C5.3866 9.2932 5.7 8.9798 5.7 8.5932C5.7 8.2066 5.3866 7.8932 5 7.8932C4.6134 7.8932 4.3 8.2066 4.3 8.5932C4.3 8.9798 4.6134 9.2932 5 9.2932Z",
  fill: "#6FA3FF"
});

var _ref2$3 = /*#__PURE__*/createElement("path", {
  d: "M11.0024 14.1068C11.389 14.1068 11.7024 13.7934 11.7024 13.4068C11.7024 13.0202 11.389 12.7068 11.0024 12.7068C10.6158 12.7068 10.3024 13.0202 10.3024 13.4068C10.3024 13.7934 10.6158 14.1068 11.0024 14.1068Z",
  fill: "#6FA3FF"
});

var _ref3 = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M4.13998 0.950012C4.49896 0.950012 4.78998 1.24103 4.78998 1.60001V2.99002H7.34999V1.60001C7.34999 1.24103 7.64101 0.950012 7.99999 0.950012C8.35898 0.950012 8.64999 1.24103 8.64999 1.60001V2.99002H11.2V1.60001C11.2 1.24103 11.491 0.950012 11.85 0.950012C12.209 0.950012 12.5 1.24103 12.5 1.60001V2.99002H13C14.65 2.99002 16 4.33002 16 5.99002V16C16 17.66 14.66 19 13 19H3C1.34 19 0 17.66 0 16V5.99002C0 4.33002 1.34 2.99002 3 2.99002H3.48998V1.60001C3.48998 1.24103 3.78099 0.950012 4.13998 0.950012ZM5 10.5932C6.10457 10.5932 7 9.69777 7 8.5932C7 7.48863 6.10457 6.5932 5 6.5932C3.89543 6.5932 3 7.48863 3 8.5932C3 9.69777 3.89543 10.5932 5 10.5932ZM11.5058 8.40383C11.7588 8.14919 11.7575 7.73763 11.5028 7.4846C11.2482 7.23157 10.8366 7.23288 10.5836 7.48752L4.57995 13.5295C4.32692 13.7842 4.32823 14.1957 4.58288 14.4487C4.83752 14.7018 5.24908 14.7005 5.50211 14.4458L11.5058 8.40383ZM11.0024 15.4068C12.107 15.4068 13.0024 14.5114 13.0024 13.4068C13.0024 12.3022 12.107 11.4068 11.0024 11.4068C9.89784 11.4068 9.00241 12.3022 9.00241 13.4068C9.00241 14.5114 9.89784 15.4068 11.0024 15.4068Z",
  fill: "#6FA3FF"
});

function SvgKpi(props) {
  return /*#__PURE__*/createElement("svg", _extends$3({
    width: 16,
    height: 19,
    viewBox: "0 0 16 19",
    fill: "none"
  }, props), _ref$3, _ref2$3, _ref3);
}

function _extends$4() { _extends$4 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$4.apply(this, arguments); }

var _ref$4 = /*#__PURE__*/createElement("path", {
  d: "M9.08055 0.919335C9.58823 0.411653 10.4113 0.411654 10.919 0.919336L18.9998 9.00009C19.2713 9.23489 19.3011 9.64536 19.0663 9.91692C18.8316 10.1885 18.4211 10.2183 18.1495 9.98348L16.9998 8.83374V16.7001C16.9998 17.4181 16.4178 18.0001 15.6998 18.0001H12.6998C12.3132 18.0001 11.9998 17.6867 11.9998 17.3001V12.3001C11.9998 11.5821 11.4178 11.0001 10.6998 11.0001H9.29979C8.58182 11.0001 7.99979 11.5821 7.99979 12.3001V17.3001C7.99979 17.6867 7.68639 18.0001 7.29979 18.0001H4.29979C3.58182 18.0001 2.99979 17.4181 2.99979 16.7001V8.83374L1.85004 9.98348C1.57849 10.2183 1.16801 10.1885 0.933219 9.91692C0.698425 9.64536 0.728226 9.23489 0.999781 9.00009L3.99979 6.00009L9.08055 0.919335Z",
  fill: "#307FFF"
});

function SvgMain(props) {
  return /*#__PURE__*/createElement("svg", _extends$4({
    width: 20,
    height: 18,
    viewBox: "0 0 20 18",
    fill: "none"
  }, props), _ref$4);
}

function _extends$5() { _extends$5 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$5.apply(this, arguments); }

var _ref$5 = /*#__PURE__*/createElement("path", {
  d: "M17.3333 11.3333C16.8447 11.3333 16.3907 11.1947 15.9967 10.964C15.6033 11.1927 15.1533 11.3333 14.6667 11.3333C14.1787 11.3333 13.7273 11.192 13.3333 10.9627C12.9393 11.192 12.488 11.3333 12 11.3333C11.512 11.3333 11.0607 11.192 10.6667 10.9627C10.2727 11.192 9.82133 11.3333 9.33333 11.3333C8.84667 11.3333 8.39667 11.1927 8.00333 10.964C7.60933 11.1947 7.15533 11.3333 6.66667 11.3333C6.182 11.3333 5.73333 11.1933 5.34133 10.9667C4.94667 11.198 4.48933 11.3333 4 11.3333V18.6667C4 19.4033 4.59667 20 5.33333 20H13.3333V13.3333H17.3333V20H18.6667C19.4033 20 20 19.4033 20 18.6667V11.3333C19.5107 11.3333 19.0533 11.198 18.6587 10.9667C18.2667 11.1933 17.818 11.3333 17.3333 11.3333ZM10.6667 17.3333H6.66667V13.3333H10.6667V17.3333Z",
  fill: "#307FFF"
});

var _ref2$4 = /*#__PURE__*/createElement("path", {
  d: "M19.3334 6H4.66669V5.33333C4.66669 4.59667 5.26335 4 6.00002 4H18C18.7367 4 19.3334 4.59667 19.3334 5.33333V6Z",
  fill: "#307FFF"
});

var _ref3$1 = /*#__PURE__*/createElement("path", {
  d: "M15.3333 6H13.3333V8.66667C13.3333 9.40333 13.93 10 14.6666 10C15.4033 10 16 9.40333 16 8.66667L15.3333 6Z",
  fill: "#307FFF"
});

var _ref4 = /*#__PURE__*/createElement("path", {
  d: "M19.3333 6H17.3333L18.6666 8.66667C18.6666 9.40333 19.2633 10 20 10C20.7366 10 21.3333 9.40333 21.3333 8.66667L19.3333 6Z",
  fill: "#307FFF"
});

var _ref5 = /*#__PURE__*/createElement("path", {
  d: "M15.3333 6H13.3333V8.66667C13.3333 9.40333 13.93 10 14.6666 10C15.4033 10 16 9.40333 16 8.66667L15.3333 6Z",
  fill: "#307FFF"
});

var _ref6 = /*#__PURE__*/createElement("path", {
  d: "M8.66667 6H10.6667V8.66667C10.6667 9.40333 10.07 10 9.33333 10C8.59667 10 8 9.40333 8 8.66667L8.66667 6Z",
  fill: "#307FFF"
});

var _ref7 = /*#__PURE__*/createElement("path", {
  d: "M4.66669 6H6.66669L5.33335 8.66667C5.33335 9.40333 4.73669 10 4.00002 10C3.26335 10 2.66669 9.40333 2.66669 8.66667L4.66669 6Z",
  fill: "#307FFF"
});

function SvgMarketplace(props) {
  return /*#__PURE__*/createElement("svg", _extends$5({
    width: 24,
    height: 24,
    viewBox: "0 0 24 24",
    fill: "none"
  }, props), _ref$5, _ref2$4, _ref3$1, _ref4, _ref5, _ref6, _ref7);
}

function _extends$6() { _extends$6 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$6.apply(this, arguments); }

var _ref$6 = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M20.39 7.52495C21.2283 7.52495 21.2283 8.82495 20.39 8.82495H19V15.2H20.39C20.749 15.2 21.04 15.491 21.04 15.85C21.04 16.209 20.749 16.5 20.39 16.5H19V18C19 19.65 17.66 21 16 21H7C5.34 21 4 19.66 4 18V6.03497C4 4.37497 5.34 3.03497 7 3.03497H16C17.66 3.03497 19 4.37497 19 6.03497V7.52495H20.39ZM8.5 9.22681C9.05228 9.22681 9.5 8.77909 9.5 8.22681C9.5 7.67452 9.05228 7.22681 8.5 7.22681C7.94772 7.22681 7.5 7.67452 7.5 8.22681C7.5 8.77909 7.94772 9.22681 8.5 9.22681ZM14.72 8.87689C15.079 8.87689 15.37 8.58587 15.37 8.22689C15.37 7.8679 15.079 7.57689 14.72 7.57689H11.5C11.141 7.57689 10.85 7.8679 10.85 8.22689C10.85 8.58587 11.141 8.87689 11.5 8.87689H14.72ZM15.3809 12C15.3809 12.359 15.0898 12.65 14.7309 12.65H11.5109C11.1519 12.65 10.8609 12.359 10.8609 12C10.8609 11.641 11.1519 11.35 11.5109 11.35H14.7309C15.0898 11.35 15.3809 11.641 15.3809 12ZM14.72 16.4794C15.079 16.4794 15.37 16.1884 15.37 15.8294C15.37 15.4704 15.079 15.1794 14.72 15.1794L11.5 15.1794C11.141 15.1794 10.85 15.4704 10.85 15.8294C10.85 16.1884 11.141 16.4794 11.5 16.4794H14.72ZM9.52585 12C9.52585 12.5523 9.07814 13 8.52585 13C7.97357 13 7.52585 12.5523 7.52585 12C7.52585 11.4477 7.97357 11 8.52585 11C9.07814 11 9.52585 11.4477 9.52585 12ZM9.52585 15.8025C9.52585 16.3548 9.07814 16.8025 8.52585 16.8025C7.97357 16.8025 7.52585 16.3548 7.52585 15.8025C7.52585 15.2502 7.97357 14.8025 8.52585 14.8025C9.07814 14.8025 9.52585 15.2502 9.52585 15.8025Z",
  fill: "#307FFF"
});

function SvgTasks(props) {
  return /*#__PURE__*/createElement("svg", _extends$6({
    width: 24,
    height: 24,
    viewBox: "0 0 24 24",
    fill: "none"
  }, props), _ref$6);
}

function _extends$7() { _extends$7 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$7.apply(this, arguments); }

var _ref$7 = /*#__PURE__*/createElement("path", {
  d: "M5.5 5.22675C5.5 5.77903 5.05228 6.22675 4.5 6.22675C3.94772 6.22675 3.5 5.77903 3.5 5.22675C3.5 4.67446 3.94772 4.22675 4.5 4.22675C5.05228 4.22675 5.5 4.67446 5.5 5.22675Z",
  fill: "#7E7E7E"
});

var _ref2$5 = /*#__PURE__*/createElement("path", {
  d: "M11.37 5.22682C11.37 5.58581 11.079 5.87682 10.72 5.87682H7.50002C7.14103 5.87682 6.85001 5.58581 6.85001 5.22682C6.85001 4.86784 7.14103 4.57682 7.50002 4.57682H10.72C11.079 4.57682 11.37 4.86784 11.37 5.22682Z",
  fill: "#7E7E7E"
});

var _ref3$2 = /*#__PURE__*/createElement("path", {
  d: "M11.3809 8.99994C11.3809 9.35892 11.0898 9.64994 10.7309 9.64994H7.51085C7.15187 9.64994 6.86085 9.35892 6.86085 8.99994C6.86085 8.64095 7.15187 8.34994 7.51085 8.34994H10.7309C11.0898 8.34994 11.3809 8.64095 11.3809 8.99994Z",
  fill: "#7E7E7E"
});

var _ref4$1 = /*#__PURE__*/createElement("path", {
  d: "M11.37 12.8293C11.37 13.1883 11.079 13.4793 10.72 13.4793H7.50002C7.14103 13.4793 6.85001 13.1883 6.85001 12.8293C6.85001 12.4703 7.14103 12.1793 7.50002 12.1793L10.72 12.1793C11.079 12.1793 11.37 12.4703 11.37 12.8293Z",
  fill: "#7E7E7E"
});

var _ref5$1 = /*#__PURE__*/createElement("path", {
  d: "M5.52585 8.99994C5.52585 9.55222 5.07814 9.99994 4.52585 9.99994C3.97357 9.99994 3.52585 9.55222 3.52585 8.99994C3.52585 8.44765 3.97357 7.99994 4.52585 7.99994C5.07814 7.99994 5.52585 8.44765 5.52585 8.99994Z",
  fill: "#7E7E7E"
});

var _ref6$1 = /*#__PURE__*/createElement("path", {
  d: "M4.52585 13.8025C5.07814 13.8025 5.52585 13.3547 5.52585 12.8025C5.52585 12.2502 5.07814 11.8025 4.52585 11.8025C3.97357 11.8025 3.52585 12.2502 3.52585 12.8025C3.52585 13.3547 3.97357 13.8025 4.52585 13.8025Z",
  fill: "#7E7E7E"
});

var _ref7$1 = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M16.39 5.82489C17.2283 5.82489 17.2283 4.52489 16.39 4.52489H15V3.03491C15 1.37491 13.66 0.0349121 12 0.0349121H3C1.34 0.0349121 0 1.37491 0 3.03491V14.9999C0 16.6599 1.34 17.9999 3 17.9999H12C13.66 17.9999 15 16.6499 15 14.9999V13.4999H16.39C16.749 13.4999 17.04 13.2089 17.04 12.8499C17.04 12.491 16.749 12.1999 16.39 12.1999H15V5.82489H16.39ZM12 1.33491C12.94 1.33491 13.7 2.09491 13.7 3.03491L13.7 14.9999C13.7 15.9399 12.94 16.6999 12 16.6999H3C2.06 16.6999 1.3 15.9399 1.3 14.9999L1.3 3.03491C1.3 2.09491 2.06 1.33491 3 1.33491L12 1.33491Z",
  fill: "#7E7E7E"
});

function SvgTasksOutline(props) {
  return /*#__PURE__*/createElement("svg", _extends$7({
    width: 18,
    height: 18,
    viewBox: "0 0 18 18",
    fill: "none"
  }, props), _ref$7, _ref2$5, _ref3$2, _ref4$1, _ref5$1, _ref6$1, _ref7$1);
}

function _extends$8() { _extends$8 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$8.apply(this, arguments); }

var _ref$8 = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M8.98012 12.65C8.23114 12.65 7.63012 13.251 7.63012 14V15.32C7.63012 16.0689 8.23114 16.67 8.98012 16.67H11.3001C12.0491 16.67 12.6501 16.0689 12.6501 15.32V14C12.6501 13.251 12.0491 12.65 11.3001 12.65H8.98012ZM8.93012 14C8.93012 13.9812 8.93642 13.9704 8.94349 13.9633C8.95056 13.9563 8.96131 13.95 8.98012 13.95H11.3001C11.3189 13.95 11.3297 13.9563 11.3368 13.9633C11.3438 13.9704 11.3501 13.9812 11.3501 14V15.32C11.3501 15.3388 11.3438 15.3495 11.3368 15.3566C11.3297 15.3637 11.3189 15.37 11.3001 15.37H8.98012C8.96131 15.37 8.95056 15.3637 8.94349 15.3566C8.93642 15.3495 8.93012 15.3388 8.93012 15.32V14Z",
  fill: "#0062FF"
});

var _ref2$6 = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M8.89007 2.59995C8.89007 2.24097 8.59906 1.94995 8.24007 1.94995C7.88109 1.94995 7.59007 2.24097 7.59007 2.59995V3.98996H7.1001C5.4401 3.98996 4.1001 5.32996 4.1001 6.98996V17.12C4.1001 18.78 5.4401 20.12 7.1001 20.12H17.1001C18.7601 20.12 20.1001 18.78 20.1001 17.12V6.98996C20.1001 5.32996 18.7501 3.98996 17.1001 3.98996H16.6001V2.59995C16.6001 2.24097 16.3091 1.94995 15.9501 1.94995C15.5911 1.94995 15.3001 2.24097 15.3001 2.59995V3.98996H12.7501V2.59995C12.7501 2.24097 12.4591 1.94995 12.1001 1.94995C11.7411 1.94995 11.4501 2.24097 11.4501 2.59995V3.98996H8.89007V2.59995ZM7.59007 5.28996V6.85995C7.59007 7.21894 7.88109 7.50995 8.24007 7.50995C8.59906 7.50995 8.89007 7.21894 8.89007 6.85995V5.28996H11.4501V6.85995C11.4501 7.21894 11.7411 7.50995 12.1001 7.50995C12.4591 7.50995 12.7501 7.21894 12.7501 6.85995V5.28996H15.3001V6.85995C15.3001 7.21894 15.5911 7.50995 15.9501 7.50995C16.3091 7.50995 16.6001 7.21894 16.6001 6.85995V5.28996H17.1001C18.0401 5.28996 18.8001 6.04996 18.8001 6.98996V9.34997H5.4001V6.98996C5.4001 6.04996 6.1601 5.28996 7.1001 5.28996H7.59007ZM5.4001 10.65H18.8001V17.12C18.8001 18.06 18.0401 18.82 17.1001 18.82H7.1001C6.1601 18.82 5.4001 18.06 5.4001 17.12V10.65Z",
  fill: "#0062FF"
});

function SvgCalendarOutline(props) {
  return /*#__PURE__*/createElement("svg", _extends$8({
    width: 24,
    height: 24,
    viewBox: "0 0 24 24",
    fill: "none"
  }, props), _ref$8, _ref2$6);
}

function _extends$9() { _extends$9 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$9.apply(this, arguments); }

var _ref$9 = /*#__PURE__*/createElement("path", {
  d: "M4.91683 4.85583C4.91683 5.31606 4.54373 5.68916 4.0835 5.68916C3.62326 5.68916 3.25016 5.31606 3.25016 4.85583C3.25016 4.39559 3.62326 4.02249 4.0835 4.02249C4.54373 4.02249 4.91683 4.39559 4.91683 4.85583Z",
  fill: "#7E7E7E"
});

var _ref2$7 = /*#__PURE__*/createElement("path", {
  d: "M9.80851 4.85589C9.80851 5.15504 9.56599 5.39756 9.26684 5.39756H6.58351C6.28435 5.39756 6.04184 5.15504 6.04184 4.85589C6.04184 4.55674 6.28435 4.31422 6.58351 4.31422H9.26684C9.56599 4.31422 9.80851 4.55674 9.80851 4.85589Z",
  fill: "#7E7E7E"
});

var _ref3$3 = /*#__PURE__*/createElement("path", {
  d: "M9.81754 8.00015C9.81754 8.29931 9.57503 8.54182 9.27587 8.54182H6.59254C6.29338 8.54182 6.05087 8.29931 6.05087 8.00015C6.05087 7.701 6.29338 7.45849 6.59254 7.45849H9.27587C9.57503 7.45849 9.81754 7.701 9.81754 8.00015Z",
  fill: "#7E7E7E"
});

var _ref4$2 = /*#__PURE__*/createElement("path", {
  d: "M9.80851 11.1913C9.80851 11.4905 9.56599 11.733 9.26684 11.733H6.58351C6.28435 11.733 6.04184 11.4905 6.04184 11.1913C6.04184 10.8921 6.28435 10.6496 6.58351 10.6496L9.26684 10.6496C9.56599 10.6496 9.80851 10.8921 9.80851 11.1913Z",
  fill: "#7E7E7E"
});

var _ref5$2 = /*#__PURE__*/createElement("path", {
  d: "M4.93837 8.00015C4.93837 8.46039 4.56528 8.83349 4.10504 8.83349C3.6448 8.83349 3.27171 8.46039 3.27171 8.00015C3.27171 7.53992 3.6448 7.16682 4.10504 7.16682C4.56528 7.16682 4.93837 7.53992 4.93837 8.00015Z",
  fill: "#7E7E7E"
});

var _ref6$2 = /*#__PURE__*/createElement("path", {
  d: "M4.10504 12.0022C4.56528 12.0022 4.93837 11.6292 4.93837 11.1689C4.93837 10.7087 4.56528 10.3356 4.10504 10.3356C3.6448 10.3356 3.27171 10.7087 3.27171 11.1689C3.27171 11.6292 3.6448 12.0022 4.10504 12.0022Z",
  fill: "#7E7E7E"
});

var _ref7$2 = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M13.9918 5.35428C14.6904 5.35428 14.6904 4.27094 13.9918 4.27094H12.8335V3.0293C12.8335 1.64596 11.7168 0.529297 10.3335 0.529297H2.8335C1.45016 0.529297 0.333496 1.64596 0.333496 3.0293V13.0002C0.333496 14.3835 1.45016 15.5002 2.8335 15.5002H10.3335C11.7168 15.5002 12.8335 14.3752 12.8335 13.0002V11.7502H13.9918C14.291 11.7502 14.5335 11.5076 14.5335 11.2085C14.5335 10.9093 14.291 10.6668 13.9918 10.6668H12.8335V5.35428H13.9918ZM10.3335 1.61263C11.1168 1.61263 11.7502 2.24596 11.7502 3.0293L11.7502 13.0002C11.7502 13.7835 11.1168 14.4168 10.3335 14.4168H2.8335C2.05016 14.4168 1.41683 13.7835 1.41683 13.0002L1.41683 3.0293C1.41683 2.24596 2.05016 1.61263 2.8335 1.61263L10.3335 1.61263Z",
  fill: "#7E7E7E"
});

function SvgUnionOutline(props) {
  return /*#__PURE__*/createElement("svg", _extends$9({
    width: 15,
    height: 16,
    viewBox: "0 0 15 16",
    fill: "none"
  }, props), _ref$9, _ref2$7, _ref3$3, _ref4$2, _ref5$2, _ref6$2, _ref7$2);
}

function _extends$a() { _extends$a = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$a.apply(this, arguments); }

var _ref$a = /*#__PURE__*/createElement("path", {
  d: "M8.54167 4.79453C8.54167 4.49538 8.29915 4.25286 8 4.25286C7.70085 4.25286 7.45833 4.49538 7.45833 4.79453V8.54453C7.45833 8.84368 7.70085 9.0862 8 9.0862H11.3333C11.6325 9.0862 11.875 8.84368 11.875 8.54453C11.875 8.24538 11.6325 8.00286 11.3333 8.00286H8.54167V4.79453Z",
  fill: "#D92020"
});

var _ref2$8 = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M0.5 8C0.5 3.85786 3.85786 0.5 8 0.5C12.1421 0.5 15.5 3.85786 15.5 8C15.5 12.1421 12.1421 15.5 8 15.5C3.85786 15.5 0.5 12.1421 0.5 8ZM8 1.58333C4.45617 1.58333 1.58333 4.45617 1.58333 8C1.58333 11.5438 4.45617 14.4167 8 14.4167C11.5438 14.4167 14.4167 11.5438 14.4167 8C14.4167 4.45617 11.5438 1.58333 8 1.58333Z",
  fill: "#D92020"
});

function SvgClockUnion(props) {
  return /*#__PURE__*/createElement("svg", _extends$a({
    width: 16,
    height: 16,
    viewBox: "0 0 16 16",
    fill: "none"
  }, props), _ref$a, _ref2$8);
}

function _extends$b() { _extends$b = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$b.apply(this, arguments); }

var _ref$b = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M7.18955 0.5C4.96868 0.5 3.24258 2.43335 3.49334 4.64002L3.71171 6.56165C3.91271 8.33047 5.40935 9.66667 7.18955 9.66667C8.96975 9.66667 10.4664 8.33047 10.6674 6.56165L10.8858 4.64002C11.1365 2.43335 9.41042 0.5 7.18955 0.5ZM9.59099 6.43933L9.80935 4.5177C9.98709 2.95366 8.76366 1.58333 7.18955 1.58333C5.61544 1.58333 4.39201 2.95366 4.56974 4.5177L4.78811 6.43933C4.9269 7.66069 5.96032 8.58333 7.18955 8.58333C8.41877 8.58333 9.45219 7.6607 9.59099 6.43933Z",
  fill: "#0062FF"
});

var _ref2$9 = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M5.64485 10.5C3.30136 10.5 1.27229 12.1277 0.763915 14.4153L0.680632 14.7901C0.599676 15.1544 0.876888 15.5 1.25007 15.5H13.129C13.5022 15.5 13.7794 15.1544 13.6985 14.7901L13.6152 14.4153C13.1068 12.1277 11.0777 10.5 8.73425 10.5H5.64485ZM12.4982 14.4167C12.0189 12.7485 10.4891 11.5833 8.73425 11.5833H5.64485C3.88996 11.5833 2.36015 12.7485 1.88092 14.4167H12.4982Z",
  fill: "#0062FF"
});

var _ref3$4 = /*#__PURE__*/createElement("path", {
  d: "M10.8101 9.66667C10.4933 9.66667 10.3804 9.28186 10.5788 9.03488C10.6179 8.98624 10.656 8.93677 10.6931 8.8865C10.8298 8.70126 11.0393 8.58358 11.2654 8.54037C12.2863 8.34532 13.0903 7.50632 13.2116 6.43933L13.4299 4.5177C13.5807 3.19109 12.7234 2.00386 11.4991 1.67396C11.2054 1.59482 10.9318 1.43896 10.7405 1.20237C10.6992 1.1512 10.6568 1.10088 10.6134 1.05145C10.4292 0.841598 10.5309 0.5 10.8101 0.5C13.031 0.5 14.7571 2.43335 14.5063 4.64002L14.288 6.56165C14.087 8.33047 12.5903 9.66667 10.8101 9.66667Z",
  fill: "#0062FF"
});

var _ref4$3 = /*#__PURE__*/createElement("path", {
  d: "M15.0951 15.5C14.8095 15.5 14.606 15.207 14.6029 14.9215C14.6 14.6599 14.7872 14.4167 15.0488 14.4167H16.1188C15.7546 13.149 14.7838 12.1718 13.5685 11.7757C13.3946 11.719 13.2479 11.637 13.1354 11.4929C12.8324 11.105 13.0966 10.5305 13.5739 10.6503C15.3748 11.1027 16.8182 12.5364 17.2358 14.4153L17.319 14.7901C17.4 15.1544 17.1228 15.5 16.7496 15.5H15.0951Z",
  fill: "#0062FF"
});

function SvgGroupPersons(props) {
  return /*#__PURE__*/createElement("svg", _extends$b({
    width: 18,
    height: 16,
    viewBox: "0 0 18 16",
    fill: "none"
  }, props), _ref$b, _ref2$9, _ref3$4, _ref4$3);
}

function _extends$c() { _extends$c = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$c.apply(this, arguments); }

var _ref$c = /*#__PURE__*/createElement("path", {
  d: "M17.0518 0.347589C16.5883 -0.115863 15.8369 -0.115863 15.3734 0.347589L13.1787 2.5423C12.7153 3.00575 12.7153 3.75715 13.1787 4.2206L15.3735 6.41531C15.8369 6.87877 16.5883 6.87877 17.0518 6.41531L19.2465 4.2206C19.7099 3.75715 19.7099 3.00575 19.2465 2.5423L17.0518 0.347589Z",
  fill: "#307FFF"
});

var _ref2$a = /*#__PURE__*/createElement("path", {
  d: "M0 1.68144C0 0.963469 0.58203 0.381439 1.3 0.381439H4.7C5.41797 0.381439 6 0.963469 6 1.68144V5.08144C6 5.79941 5.41797 6.38144 4.7 6.38144H1.3C0.58203 6.38144 0 5.79941 0 5.08144V1.68144Z",
  fill: "#307FFF"
});

var _ref3$5 = /*#__PURE__*/createElement("path", {
  d: "M16.2125 8.0586C16.5715 8.0586 16.8625 8.34962 16.8625 8.7086V11.6722C16.8625 12.0311 16.5715 12.3222 16.2125 12.3222C15.8535 12.3222 15.5625 12.0311 15.5625 11.6722V8.7086C15.5625 8.34962 15.8535 8.0586 16.2125 8.0586Z",
  fill: "#307FFF"
});

var _ref4$4 = /*#__PURE__*/createElement("path", {
  d: "M13.2126 16.6266C13.2126 14.9698 14.5557 13.6266 16.2126 13.6266C17.8694 13.6266 19.2126 14.9698 19.2126 16.6266C19.2126 18.2835 17.8694 19.6266 16.2126 19.6266C14.5557 19.6266 13.2126 18.2835 13.2126 16.6266Z",
  fill: "#307FFF"
});

var _ref5$3 = /*#__PURE__*/createElement("path", {
  d: "M7.95106 2.73141C7.59207 2.73141 7.30106 3.02243 7.30106 3.38141C7.30106 3.7404 7.59207 4.03141 7.95106 4.03141H10.8815C11.2405 4.03141 11.5315 3.7404 11.5315 3.38141C11.5315 3.02243 11.2405 2.73141 10.8815 2.73141H7.95106Z",
  fill: "#307FFF"
});

var _ref6$3 = /*#__PURE__*/createElement("path", {
  d: "M9.33398 15.9585L7.82099 14.4965C7.56715 14.2426 7.56715 13.8311 7.82099 13.5772C8.07483 13.3234 8.48639 13.3234 8.74023 13.5772L11.7162 16.1489C11.97 16.4027 11.97 16.8142 11.7162 17.0681L8.74804 19.6319C8.4942 19.8857 8.08264 19.8857 7.8288 19.6319C7.57496 19.3781 7.57496 18.9665 7.8288 18.7127L9.33304 17.2594H6.00561C3.98978 17.2594 2.35562 15.6252 2.35562 13.6094V8.33203C2.35562 7.97305 2.64663 7.68203 3.00562 7.68203C3.3646 7.68203 3.65562 7.97305 3.65562 8.33203V13.6094C3.65562 14.9072 4.70775 15.9594 6.00561 15.9594L9.33398 15.9585Z",
  fill: "#307FFF"
});

function SvgProcesses(props) {
  return /*#__PURE__*/createElement("svg", _extends$c({
    width: 20,
    height: 20,
    viewBox: "0 0 20 20",
    fill: "none"
  }, props), _ref$c, _ref2$a, _ref3$5, _ref4$4, _ref5$3, _ref6$3);
}

function _extends$d() { _extends$d = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$d.apply(this, arguments); }

var _ref$d = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M10.214 2.34344C9.44921 1.57866 8.20927 1.57866 7.44449 2.34344L2.31797 7.46996C1.13012 8.65781 1.13012 10.5837 2.31797 11.7715L3.43755 12.8911C4.6254 14.079 6.55127 14.079 7.73912 12.8911L12.571 8.05922C12.7825 7.84768 13.1255 7.84768 13.337 8.05922C13.5486 8.27075 13.5486 8.61372 13.337 8.82525L8.50515 13.6571C6.89424 15.2681 4.28243 15.2681 2.67152 13.6571L1.55193 12.5376C-0.058978 10.9266 -0.0589782 8.31484 1.55193 6.70393L6.67846 1.57741C7.8663 0.389562 9.79218 0.389562 10.98 1.57741L11.5693 2.16666C12.7571 3.35451 12.7571 5.28038 11.5693 6.46823L6.73738 11.3001C6.10278 11.9347 5.07389 11.9347 4.43929 11.3001L3.90896 10.7698C3.27436 10.1352 3.27435 9.1063 3.90896 8.4717L8.44623 3.93443C8.65776 3.7229 9.00072 3.7229 9.21226 3.93443C9.42379 4.14596 9.42379 4.48893 9.21226 4.70046L4.67499 9.23773C4.46345 9.44926 4.46346 9.79223 4.67499 10.0038L5.20532 10.5341C5.41685 10.7456 5.75982 10.7456 5.97135 10.5341L10.8032 5.7022C11.568 4.93742 11.568 3.69747 10.8032 2.93269L10.214 2.34344Z",
  fill: "#0062FF"
});

function SvgUpload(props) {
  return /*#__PURE__*/createElement("svg", _extends$d({
    width: 14,
    height: 15,
    viewBox: "0 0 14 15",
    fill: "none"
  }, props), _ref$d);
}

function _extends$e() { _extends$e = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$e.apply(this, arguments); }

var _ref$e = /*#__PURE__*/createElement("path", {
  d: "M5.18229 9.37698C5.18229 9.22698 5.24062 9.08531 5.35729 8.97698C5.58229 8.77698 5.92396 8.79365 6.12396 9.01031L9.45908 12.199L9.45908 3.87516C9.45908 3.57601 9.7016 3.3335 10.0007 3.3335C10.2999 3.3335 10.5424 3.57601 10.5424 3.87516V12.1462L13.5823 8.94364C13.7823 8.71864 14.1239 8.7103 14.3489 8.9103C14.5656 9.1103 14.5823 9.45197 14.3823 9.67697L10.4341 13.8585C10.2007 14.1168 9.80075 14.1168 9.56741 13.8585L5.32396 9.74365C5.22396 9.63531 5.18229 9.51031 5.18229 9.37698Z",
  fill: "#0062FF"
});

var _ref2$b = /*#__PURE__*/createElement("path", {
  d: "M4.16667 15.7085C3.86751 15.7085 3.625 15.951 3.625 16.2502C3.625 16.5493 3.86751 16.7918 4.16667 16.7918H15.8333C16.1325 16.7918 16.375 16.5493 16.375 16.2502C16.375 15.951 16.1325 15.7085 15.8333 15.7085H4.16667Z",
  fill: "#0062FF"
});

function SvgDownload(props) {
  return /*#__PURE__*/createElement("svg", _extends$e({
    width: 20,
    height: 20,
    viewBox: "0 0 20 20",
    fill: "none"
  }, props), _ref$e, _ref2$b);
}

function _extends$f() { _extends$f = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$f.apply(this, arguments); }

var _ref$f = /*#__PURE__*/createElement("path", {
  d: "M8.33366 3.87516C8.33366 3.57601 8.09115 3.3335 7.79199 3.3335H6.66699C5.28628 3.3335 4.16699 4.45278 4.16699 5.8335V14.1668C4.16699 15.5475 5.28628 16.6668 6.66699 16.6668H13.3337C14.7144 16.6668 15.8337 15.5475 15.8337 14.1668V6.99068C15.8337 6.69153 15.5911 6.44902 15.292 6.44902C14.9928 6.44902 14.7503 6.69153 14.7503 6.99068V14.1668C14.7503 14.9492 14.1161 15.5835 13.3337 15.5835H6.66699C5.88459 15.5835 5.25033 14.9492 5.25033 14.1668V5.8335C5.25033 5.05109 5.88459 4.41683 6.66699 4.41683H7.79199C8.09115 4.41683 8.33366 4.17432 8.33366 3.87516Z",
  fill: "#0062FF"
});

var _ref2$c = /*#__PURE__*/createElement("path", {
  d: "M13.5904 8.13085C13.7071 8.23918 13.7654 8.38085 13.7654 8.53085C13.7654 8.66418 13.7238 8.78918 13.6238 8.89752L10.4359 12.2809C10.2025 12.5392 9.80253 12.5392 9.5692 12.2809L6.64878 8.93087C6.44879 8.70587 6.46545 8.3642 6.68212 8.1642C6.90712 7.9642 7.24879 7.97253 7.44879 8.19753L9.46086 10.5813L9.47378 10.5814V6.44753C9.47378 4.76767 10.8356 3.40587 12.5155 3.40587H15.304C15.6031 3.40587 15.8457 3.64838 15.8457 3.94753C15.8457 4.24669 15.6031 4.4892 15.304 4.4892H12.5155C11.4339 4.4892 10.5571 5.36598 10.5571 6.44753V10.5735L12.8238 8.16418C13.0238 7.94752 13.3654 7.93085 13.5904 8.13085Z",
  fill: "#0062FF"
});

function SvgImport(props) {
  return /*#__PURE__*/createElement("svg", _extends$f({
    width: 20,
    height: 20,
    viewBox: "0 0 20 20",
    fill: "none"
  }, props), _ref$f, _ref2$c);
}

function _extends$g() { _extends$g = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$g.apply(this, arguments); }

var _ref$g = /*#__PURE__*/createElement("path", {
  d: "M8.99168 12.7417C9.22501 12.975 9.60834 12.9667 9.83334 12.725L13.5167 8.725C13.7167 8.5 13.7 8.15834 13.4833 7.95834C13.2667 7.75834 12.9167 7.76667 12.7167 7.99167L9.39168 11.6083L7.39168 9.60834C7.18334 9.4 6.83334 9.4 6.62501 9.60834C6.41668 9.81667 6.41668 10.1667 6.62501 10.375L8.99168 12.7417Z",
  fill: "#0062FF"
});

var _ref2$d = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M10 2.5C5.85786 2.5 2.5 5.85786 2.5 10C2.5 14.1421 5.85786 17.5 10 17.5C14.1421 17.5 17.5 14.1421 17.5 10C17.5 5.85786 14.1421 2.5 10 2.5ZM3.58333 10C3.58333 6.45617 6.45617 3.58333 10 3.58333C13.5438 3.58333 16.4167 6.45617 16.4167 10C16.4167 13.5438 13.5438 16.4167 10 16.4167C6.45617 16.4167 3.58333 13.5438 3.58333 10Z",
  fill: "#0062FF"
});

function SvgOk(props) {
  return /*#__PURE__*/createElement("svg", _extends$g({
    width: 20,
    height: 20,
    viewBox: "0 0 20 20",
    fill: "none"
  }, props), _ref$g, _ref2$d);
}

function _extends$h() { _extends$h = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$h.apply(this, arguments); }

var _ref$h = /*#__PURE__*/createElement("path", {
  d: "M8.67736 0.927184C8.36006 0.609883 7.84561 0.609884 7.52831 0.927185L7.00354 1.45196C6.73274 1.72275 6.72982 2.16089 6.99699 2.43527L7.89217 3.35461C8.16442 3.6342 8.61272 3.63719 8.88867 3.36124L9.40026 2.84965C9.71756 2.53235 9.71756 2.0179 9.40026 1.7006L8.67736 0.927184Z",
  fill: "#0062FF"
});

var _ref2$e = /*#__PURE__*/createElement("path", {
  d: "M7.32411 4.92581C7.59491 4.65501 7.59782 4.21687 7.33065 3.94249L6.43547 3.02315C6.16323 2.74356 5.71492 2.74057 5.43898 3.01652L0.983088 7.47241C0.734603 7.7209 0.56095 8.03421 0.481931 8.37663L0.232252 9.45857C0.0972392 10.0436 0.283829 10.2807 0.868887 10.1457L1.95083 9.89604C2.29324 9.81702 2.60656 9.64337 2.85504 9.39488L7.32411 4.92581Z",
  fill: "#0062FF"
});

function SvgPencil(props) {
  return /*#__PURE__*/createElement("svg", _extends$h({
    width: 10,
    height: 11,
    viewBox: "0 0 10 11",
    fill: "none"
  }, props), _ref$h, _ref2$e);
}

function _extends$i() { _extends$i = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$i.apply(this, arguments); }

var _ref$i = /*#__PURE__*/createElement("path", {
  d: "M9 13.65C9.41421 13.65 9.75 13.3142 9.75 12.9C9.75 12.4858 9.41421 12.15 9 12.15C8.58579 12.15 8.25 12.4858 8.25 12.9C8.25 13.3142 8.58579 13.65 9 13.65Z",
  fill: "#7E7E7E"
});

var _ref2$f = /*#__PURE__*/createElement("path", {
  d: "M9 10.7962C9.35898 10.7962 9.65 10.5052 9.65 10.1462V4.99997C9.65 4.64099 9.35898 4.34997 9 4.34997C8.64102 4.34997 8.35 4.64099 8.35 4.99997V10.1462C8.35 10.5052 8.64102 10.7962 9 10.7962Z",
  fill: "#7E7E7E"
});

var _ref3$6 = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M9 0C4.02944 0 0 4.02944 0 9C0 13.9706 4.02944 18 9 18C13.9706 18 18 13.9706 18 9C18 4.02944 13.9706 0 9 0ZM1.3 9C1.3 4.74741 4.74741 1.3 9 1.3C13.2526 1.3 16.7 4.74741 16.7 9C16.7 13.2526 13.2526 16.7 9 16.7C4.74741 16.7 1.3 13.2526 1.3 9Z",
  fill: "#7E7E7E"
});

function SvgExclamationPoint(props) {
  return /*#__PURE__*/createElement("svg", _extends$i({
    width: 18,
    height: 18,
    viewBox: "0 0 18 18",
    fill: "none"
  }, props), _ref$i, _ref2$f, _ref3$6);
}

function _extends$j() { _extends$j = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$j.apply(this, arguments); }

var _ref$j = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M4.58317 0.444069C4.83003 0.161875 5.1867 0 5.56163 0H14.4189C14.7929 0 15.1488 0.161083 15.3956 0.442083L19.2438 4.82321C19.6758 5.31505 19.6747 6.05141 19.2412 6.54196L10.9738 15.8976C10.4563 16.4832 9.54294 16.4832 9.0255 15.8976L0.756517 6.54021C0.323693 6.05041 0.321848 5.3154 0.752206 4.82344L4.58317 0.444069ZM10.0123 1.3H10.0246L12.8534 4.98642H7.18351L10.0123 1.3ZM8.37366 1.3H5.56163L2.33684 4.98642H5.54487L8.37366 1.3ZM5.61977 6.28642H2.2671L7.83552 12.5878L5.61977 6.28642ZM10.0185 14.8769L13.0391 6.28642H6.9978L10.0185 14.8769ZM14.4921 4.98642L11.6633 1.3H14.4189L17.6569 4.98642H14.4921ZM14.4172 6.28642L12.2263 12.5171L17.7322 6.28642H14.4172Z",
  fill: "#7E7E7E"
});

function SvgDiamond(props) {
  return /*#__PURE__*/createElement("svg", _extends$j({
    width: 20,
    height: 17,
    viewBox: "0 0 20 17",
    fill: "none"
  }, props), _ref$j);
}

function _extends$k() { _extends$k = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$k.apply(this, arguments); }

var _ref$k = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M0.5 8C0.5 3.85786 3.85786 0.5 8 0.5C12.1421 0.5 15.5 3.85786 15.5 8C15.5 12.1421 12.1421 15.5 8 15.5C3.85786 15.5 0.5 12.1421 0.5 8ZM11.3333 4.73398C11.5449 4.94552 11.5449 5.28848 11.3333 5.50001L8.76605 8.00003L11.266 10.5673C11.4776 10.7788 11.4776 11.1218 11.266 11.3333C11.0545 11.5449 10.7115 11.5449 10.5 11.3333L8.00002 8.76606L5.50001 11.3333C5.28848 11.5449 4.94552 11.5449 4.73398 11.3333C4.52245 11.1218 4.52245 10.7788 4.73398 10.5673L7.23398 8.00003L4.73396 5.50002C4.52242 5.28848 4.52242 4.94552 4.73396 4.73398C4.94549 4.52245 5.28846 4.52245 5.49999 4.73398L8.00002 7.234L10.5673 4.73398C10.7788 4.52245 11.1218 4.52245 11.3333 4.73398Z",
  fill: "#0062FF"
});

function SvgCancel(props) {
  return /*#__PURE__*/createElement("svg", _extends$k({
    width: 16,
    height: 16,
    viewBox: "0 0 16 16",
    fill: "none"
  }, props), _ref$k);
}

function _extends$l() { _extends$l = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$l.apply(this, arguments); }

var _ref$l = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M0.5 8C0.5 3.85786 3.85786 0.5 8 0.5C12.1421 0.5 15.5 3.85786 15.5 8C15.5 12.1421 12.1421 15.5 8 15.5C3.85786 15.5 0.5 12.1421 0.5 8ZM6.99162 10.7416C7.22496 10.9749 7.60829 10.9666 7.83329 10.7249L11.5166 6.7249C11.7166 6.4999 11.7 6.15824 11.4833 5.95824C11.2666 5.75824 10.9166 5.76657 10.7166 5.99157L7.39162 9.60824L5.39162 7.60824C5.18329 7.3999 4.83329 7.3999 4.62496 7.60824C4.41662 7.81657 4.41662 8.16657 4.62496 8.3749L6.99162 10.7416Z",
  fill: "#0062FF"
});

function SvgApply(props) {
  return /*#__PURE__*/createElement("svg", _extends$l({
    width: 16,
    height: 16,
    viewBox: "0 0 16 16",
    fill: "none"
  }, props), _ref$l);
}

function _extends$m() { _extends$m = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$m.apply(this, arguments); }

var _ref$m = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M0.333008 6.16658C0.333008 2.94159 2.94134 0.333252 6.16634 0.333252C9.39134 0.333252 11.9997 2.94159 11.9997 6.16658C11.9997 7.63501 11.4589 8.9756 10.5654 10.0003L13.4832 12.9257C13.6944 13.1375 13.694 13.4805 13.4822 13.6918C13.2704 13.903 12.9274 13.9026 12.7162 13.6908L9.78073 10.7477C8.78741 11.5321 7.53218 11.9999 6.16634 11.9999C2.94134 11.9999 0.333008 9.39158 0.333008 6.16658ZM10.9163 6.16658C10.9163 3.54992 8.78301 1.41659 6.16634 1.41659C3.54967 1.41659 1.41634 3.54992 1.41634 6.16658C1.41634 8.78325 3.54967 10.9166 6.16634 10.9166C8.78301 10.9166 10.9163 8.78325 10.9163 6.16658Z",
  fill: "#7E7E7E"
});

function SvgLoupe(props) {
  return /*#__PURE__*/createElement("svg", _extends$m({
    width: 14,
    height: 14,
    viewBox: "0 0 14 14",
    fill: "none"
  }, props), _ref$m);
}

function _extends$n() { _extends$n = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$n.apply(this, arguments); }

var _ref$n = /*#__PURE__*/createElement("path", {
  d: "M9 4.34998C9.41421 4.34998 9.75 4.68576 9.75 5.09998C9.75 5.51419 9.41421 5.84998 9 5.84998C8.58579 5.84998 8.25 5.51419 8.25 5.09998C8.25 4.68576 8.58579 4.34998 9 4.34998Z",
  fill: "#7E7E7E"
});

var _ref2$g = /*#__PURE__*/createElement("path", {
  d: "M9 7.20376C9.35898 7.20376 9.65 7.49477 9.65 7.85376V11.7C9.65 12.059 9.941 12.35 10.3 12.35L10.35 12.35C10.709 12.35 11 12.641 11 13C11 13.359 10.709 13.65 10.35 13.65L10.2999 13.65C9.22301 13.65 8.35 12.7769 8.35 11.7V7.85376C8.35 7.49477 8.64102 7.20376 9 7.20376Z",
  fill: "#7E7E7E"
});

var _ref3$7 = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M9 0C4.02944 0 0 4.02944 0 9C0 13.9706 4.02944 18 9 18C13.9706 18 18 13.9706 18 9C18 4.02944 13.9706 0 9 0ZM1.3 9C1.3 4.74741 4.74741 1.3 9 1.3C13.2526 1.3 16.7 4.74741 16.7 9C16.7 13.2526 13.2526 16.7 9 16.7C4.74741 16.7 1.3 13.2526 1.3 9Z",
  fill: "#7E7E7E"
});

function SvgInfo(props) {
  return /*#__PURE__*/createElement("svg", _extends$n({
    width: 18,
    height: 18,
    viewBox: "0 0 18 18",
    fill: "none"
  }, props), _ref$n, _ref2$g, _ref3$7);
}

function _extends$o() { _extends$o = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$o.apply(this, arguments); }

var _ref$o = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M10 2.5C5.85786 2.5 2.5 5.85786 2.5 10C2.5 14.1421 5.85786 17.5 10 17.5C14.1421 17.5 17.5 14.1421 17.5 10C17.5 5.85786 14.1421 2.5 10 2.5ZM10 6.25285C10.2992 6.25285 10.5417 6.49536 10.5417 6.79451V10.0028H13.3333C13.6325 10.0028 13.875 10.2454 13.875 10.5445C13.875 10.8437 13.6325 11.0862 13.3333 11.0862H10C9.70084 11.0862 9.45833 10.8437 9.45833 10.5445V6.79451C9.45833 6.49536 9.70084 6.25285 10 6.25285Z",
  fill: "#7E7E7E"
});

function SvgClockSolid(props) {
  return /*#__PURE__*/createElement("svg", _extends$o({
    width: 20,
    height: 20,
    viewBox: "0 0 20 20",
    fill: "none"
  }, props), _ref$o);
}

function _extends$p() { _extends$p = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$p.apply(this, arguments); }

var _ref$p = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M0 9C0 4.02944 4.02944 0 9 0C13.9706 0 18 4.02944 18 9C18 13.9706 13.9706 18 9 18C4.02944 18 0 13.9706 0 9ZM8.3245 10.6324C8.3245 10.9924 8.6145 11.2824 8.97451 11.2824C9.33451 11.2824 9.62451 10.9924 9.62451 10.6224C9.62451 10.3224 9.66451 10.1224 9.77451 9.84239C9.91451 9.51239 10.1945 9.21239 10.4945 8.90239L10.4991 8.89758C10.988 8.3888 11.5349 7.8195 11.6745 6.90239C11.8045 6.03239 11.4445 5.13239 10.7245 4.56239C9.98451 3.97239 9.00451 3.84239 8.1045 4.19239C6.92451 4.66239 6.25451 5.68239 6.31451 6.94239C6.3245 7.30239 6.6445 7.57239 6.9945 7.56239C7.3545 7.54239 7.6345 7.24239 7.6145 6.88239C7.5845 6.39239 7.7445 5.73239 8.58451 5.40239C9.0545 5.22239 9.5345 5.28239 9.91451 5.58239C10.2745 5.86239 10.4545 6.29239 10.3945 6.71239C10.3159 7.21369 9.97636 7.57007 9.58492 7.98096L9.56451 8.00239C9.2045 8.37239 8.8045 8.79239 8.5745 9.36239C8.3945 9.79239 8.3245 10.1624 8.3245 10.6324ZM9.85 13.35C9.85 13.8194 9.46944 14.2 9 14.2C8.53056 14.2 8.15 13.8194 8.15 13.35C8.15 12.8806 8.53056 12.5 9 12.5C9.46944 12.5 9.85 12.8806 9.85 13.35Z",
  fill: "#307FFF"
});

function SvgSubtract(props) {
  return /*#__PURE__*/createElement("svg", _extends$p({
    width: 18,
    height: 18,
    viewBox: "0 0 18 18",
    fill: "none"
  }, props), _ref$p);
}

function _extends$q() { _extends$q = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$q.apply(this, arguments); }

var _ref$q = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M7.16428 0L5.37209 4.66537H30.2078L32 0H7.16428ZM4.47544 6.997L2.68325 11.6613H27.519L29.3112 6.997H4.47544ZM1.79219 13.994L0 18.6583H24.8357L26.6268 13.994H1.79219Z",
  fill: "white"
});

function SvgLogo(props) {
  return /*#__PURE__*/createElement("svg", _extends$q({
    width: 32,
    height: 19,
    viewBox: "0 0 32 19",
    fill: "none"
  }, props), _ref$q);
}

function _extends$r() { _extends$r = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$r.apply(this, arguments); }

var _ref$r = /*#__PURE__*/createElement("path", {
  d: "M7.0201 12.1482C7.1501 12.2882 7.3201 12.3582 7.5001 12.3582C7.6601 12.3582 7.8101 12.3082 7.9401 12.1882L13.9801 6.69817C14.2901 6.41817 14.2901 5.93817 13.9801 5.65817L7.9401 0.168174C7.6701 -0.0718255 7.2601 -0.0518256 7.0201 0.208174C6.7801 0.478174 6.7901 0.888175 7.0601 1.12817L11.8957 5.52817H1.0001C0.641113 5.52817 0.350098 5.81919 0.350098 6.17817C0.350098 6.53716 0.641113 6.82817 1.0001 6.82817H11.8957L7.0601 11.2282C6.8001 11.4682 6.7801 11.8782 7.0201 12.1482Z",
  fill: "#B9B9B9"
});

function SvgArrow(props) {
  return /*#__PURE__*/createElement("svg", _extends$r({
    width: 15,
    height: 13,
    viewBox: "0 0 15 13",
    fill: "none"
  }, props), _ref$r);
}

function _extends$s() { _extends$s = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$s.apply(this, arguments); }

var _ref$s = /*#__PURE__*/createElement("path", {
  d: "M0.266602 1.13333C0.266602 0.983332 0.324934 0.841665 0.441601 0.733332C0.666601 0.533332 1.00827 0.549999 1.20827 0.766665L5.99185 6.07428L10.6169 0.733309C10.8169 0.508309 11.1585 0.499976 11.3835 0.699976C11.6002 0.899976 11.6169 1.24164 11.4169 1.46664L6.42518 7.21594C6.19185 7.47428 5.79185 7.47428 5.55852 7.21594L0.408267 1.5C0.308267 1.39166 0.266602 1.26666 0.266602 1.13333Z",
  fill: "#0062FF"
});

function SvgArrowDown(props) {
  return /*#__PURE__*/createElement("svg", _extends$s({
    width: 12,
    height: 8,
    viewBox: "0 0 12 8",
    fill: "none"
  }, props), _ref$s);
}

function _extends$t() { _extends$t = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$t.apply(this, arguments); }

var _ref$t = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M0 9C0 4.02944 4.02944 0 9 0C13.9706 0 18 4.02944 18 9C18 13.9706 13.9706 18 9 18C4.02944 18 0 13.9706 0 9ZM7.78995 12.2899C8.06995 12.5699 8.52995 12.5599 8.79995 12.2699L13.2199 7.46988C13.4599 7.19988 13.4399 6.78988 13.1799 6.54988C12.9199 6.30988 12.4999 6.31988 12.2599 6.58988L8.26995 10.9299L5.86995 8.52988C5.61995 8.27988 5.19995 8.27988 4.94995 8.52988C4.69995 8.77988 4.69995 9.19988 4.94995 9.44988L7.78995 12.2899Z",
  fill: "#1BA049"
});

function SvgSuccess(props) {
  return /*#__PURE__*/createElement("svg", _extends$t({
    width: 18,
    height: 18,
    viewBox: "0 0 18 18",
    fill: "none"
  }, props), _ref$t);
}

function _extends$u() { _extends$u = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$u.apply(this, arguments); }

var _ref$u = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M0 9C0 4.02944 4.02944 0 9 0C13.9706 0 18 4.02944 18 9C18 13.9706 13.9706 18 9 18C4.02944 18 0 13.9706 0 9ZM8.99999 4.25C9.41421 4.25 9.74999 4.58579 9.74999 5V10C9.74999 10.4142 9.41421 10.75 8.99999 10.75C8.58578 10.75 8.24999 10.4142 8.24999 10V5C8.24999 4.58579 8.58578 4.25 8.99999 4.25ZM8.99999 13.7C9.46944 13.7 9.84999 13.3195 9.84999 12.85C9.84999 12.3806 9.46944 12 8.99999 12C8.53055 12 8.14999 12.3806 8.14999 12.85C8.14999 13.3195 8.53055 13.7 8.99999 13.7Z",
  fill: "#FF5C22"
});

function SvgWarning(props) {
  return /*#__PURE__*/createElement("svg", _extends$u({
    width: 18,
    height: 18,
    viewBox: "0 0 18 18",
    fill: "none"
  }, props), _ref$u);
}

function _extends$v() { _extends$v = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$v.apply(this, arguments); }

var _ref$v = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M3.30376 4.64002C3.053 2.43335 4.7791 0.5 6.99997 0.5C9.22085 0.5 10.9469 2.43335 10.6962 4.64002L10.4778 6.56165C10.2768 8.33047 8.78018 9.66667 6.99997 9.66667C5.21977 9.66667 3.72313 8.33047 3.52213 6.56165L3.30376 4.64002ZM9.61978 4.5177L9.40141 6.43933C9.26262 7.6607 8.2292 8.58333 6.99997 8.58333C5.77075 8.58333 4.73733 7.66069 4.59854 6.43933L4.38017 4.5177C4.20244 2.95366 5.42586 1.58333 6.99997 1.58333C8.57409 1.58333 9.79751 2.95366 9.61978 4.5177Z",
  fill: "#9E9E9E"
});

var _ref2$h = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M0.57434 14.4153C1.08272 12.1277 3.11178 10.5 5.45528 10.5H8.54467C10.8882 10.5 12.9172 12.1277 13.4256 14.4153L13.5089 14.7901C13.5898 15.1544 13.3126 15.5 12.9394 15.5H1.0605C0.687312 15.5 0.410101 15.1544 0.491056 14.7901L0.57434 14.4153ZM8.54467 11.5833C10.2996 11.5833 11.8294 12.7485 12.3086 14.4167H1.69134C2.17058 12.7485 3.70039 11.5833 5.45528 11.5833H8.54467Z",
  fill: "#9E9E9E"
});

function SvgPerson(props) {
  return /*#__PURE__*/createElement("svg", _extends$v({
    width: 14,
    height: 16,
    viewBox: "0 0 14 16",
    fill: "none"
  }, props), _ref$v, _ref2$h);
}

function _extends$w() { _extends$w = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$w.apply(this, arguments); }

var _ref$w = /*#__PURE__*/createElement("path", {
  fillRule: "evenodd",
  clipRule: "evenodd",
  d: "M13 0.333334C14.3807 0.333334 15.5 1.45262 15.5 2.83333V4.52899C15.9778 4.64185 16.3333 5.07107 16.3333 5.58333V8.41667C16.3333 8.92893 15.9778 9.35815 15.5 9.47101V11.1667C15.5 12.5474 14.3807 13.6667 13 13.6667H3C1.61929 13.6667 0.5 12.5474 0.5 11.1667V2.83333C0.5 1.45262 1.61929 0.333334 3 0.333334H13ZM3 1.41667C2.2176 1.41667 1.58333 2.05093 1.58333 2.83333V11.1667C1.58333 11.9491 2.2176 12.5833 3 12.5833H13C13.7824 12.5833 14.4167 11.9491 14.4167 11.1667V9.5H13.8333C12.4526 9.5 11.3333 8.38071 11.3333 7C11.3333 5.61929 12.4526 4.5 13.8333 4.5H14.4167V2.83333C14.4167 2.05093 13.7824 1.41667 13 1.41667H3ZM13.8333 8.41667H15.25V5.58333H13.8333C13.0509 5.58333 12.4167 6.2176 12.4167 7C12.4167 7.7824 13.0509 8.41667 13.8333 8.41667Z",
  fill: "#9E9E9E"
});

function SvgGetCash(props) {
  return /*#__PURE__*/createElement("svg", _extends$w({
    width: 17,
    height: 14,
    viewBox: "0 0 17 14",
    fill: "none"
  }, props), _ref$w);
}

var b$2 = block(style$2);
var Faq = function () {
    return (createElement("a", { className: b$2(null) },
        createElement(SvgSubtract, null)));
};

var style$3 = {"fullName":"fullName_fullName__QBk6r","fullName__name":"fullName_fullName__name__eEGGF","fullName__post":"fullName_fullName__post__3gMEV"};

var b$3 = block(style$3);
var FullName = function (_a) {
    var fullName = _a.fullName, post = _a.post;
    return (createElement("div", { className: b$3(null) },
        createElement("div", { className: b$3('name') }, fullName),
        createElement("div", { className: b$3('post') }, post)));
};

var style$4 = {"h1":"h1_h1__3C1Gs"};

var b$4 = block(style$4);
var H1 = function (_a) {
    var children = _a.children;
    return (createElement("h1", { className: b$4(null) }, children));
};

var style$5 = {"logotype":"logotype_logotype__1oCKt"};

var b$5 = block(style$5);
var Logotype = function () {
    return (createElement("div", { className: b$5(null) },
        createElement(SvgLogo, null)));
};

var style$6 = {"menu-item":"menu-item_menu-item__3co6e","menu-item__link":"menu-item_menu-item__link__3FrKT","menu-item__link_is_active":"menu-item_menu-item__link_is_active__30JKG","menu-item__icon":"menu-item_menu-item__icon__2k6D7"};

var b$6 = block(style$6);
var MenuItem = function (_a) {
    var link = _a.link, icon = _a.icon, is_active = _a.is_active, name = _a.name, getIcon = _a.getIcon, onClick = _a.onClick;
    var handleOnClick = function (e) {
        e.preventDefault();
        onClick(icon, link);
    };
    return (createElement("div", { className: b$6(null) },
        createElement("a", { href: link, onClick: handleOnClick, className: b$6('link', { is_active: is_active }) },
            createElement("div", { className: b$6('icon') }, getIcon(icon)),
            name)));
};

var style$7 = {"priorityPanelItem":"priorityPanelItem_priorityPanelItem__NOiJi","priorityPanelItem__wrapper":"priorityPanelItem_priorityPanelItem__wrapper__D3BcZ","priorityPanelItem__innerItem":"priorityPanelItem_priorityPanelItem__innerItem__vb93A","priorityPanelItem__innerItem_attention":"priorityPanelItem_priorityPanelItem__innerItem_attention__2Prrw","priorityPanelItem__value":"priorityPanelItem_priorityPanelItem__value__2Bt2K","priorityPanelItem__value_attention":"priorityPanelItem_priorityPanelItem__value_attention__v6rEl","priorityPanelItem__link":"priorityPanelItem_priorityPanelItem__link__2q-UW"};

var b$7 = block(style$7);
var PriorityPanelItem = function (_a) {
    var data = _a.data, link = _a.link, children = _a.children, rest = __rest(_a, ["data", "link", "children"]);
    return (createElement("div", __assign({ className: b$7(null) }, rest),
        createElement("div", { className: b$7('wrapper') },
            data.map(function (_a) {
                var id = _a.id, title = _a.title, value = _a.value, isWarning = _a.isWarning, last = __rest(_a, ["id", "title", "value", "isWarning"]);
                return (createElement("div", __assign({ key: id, className: b$7('innerItem', { attention: isWarning }) }, last),
                    isWarning && '(',
                    title,
                    ":",
                    createElement("span", { className: b$7('value', { attention: isWarning }) }, value),
                    isWarning && ')'));
            }),
            createElement("a", { href: link, className: b$7('link') },
                createElement(SvgArrow, null))),
        children));
};

var style$8 = {"switch":"switch_switch__FNkWq","switch_checked":"switch_switch_checked__18Cbn","switch__slider":"switch_switch__slider__2kALq","switch__input":"switch_switch__input__2t_9S","switch__label":"switch_switch__label__1-1-t","switch__container":"switch_switch__container__3SnYD"};

var b$8 = block(style$8);
var Switch = /** @class */ (function (_super) {
    __extends(Switch, _super);
    function Switch() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.handleOnChange = function () {
            var _a = _this.props, name = _a.name, onChange = _a.onChange;
            onChange(name, !_this.props.checked);
        };
        return _this;
    }
    Switch.prototype.render = function () {
        var _a = this.props, id = _a.id, children = _a.children, checked = _a.checked;
        return createElement("label", { className: b$8(null, { checked: checked }), htmlFor: id },
            createElement("span", { className: b$8('label') }, children),
            createElement("input", { id: id, checked: checked, onChange: this.handleOnChange, className: b$8('input'), type: "checkbox" }),
            createElement("div", { className: b$8('container') },
                createElement("span", { className: b$8('slider') })));
    };
    Switch.defaultProps = {
        id: "switch",
        checked: false
    };
    return Switch;
}(Component));

var style$9 = {"tabBarItem":"tabBarItem_tabBarItem__13OGQ","tabBarItem_isActiveTab":"tabBarItem_tabBarItem_isActiveTab__3zJwb"};

var b$9 = block(style$9);
var TabBarItem = /** @class */ (function (_super) {
    __extends(TabBarItem, _super);
    function TabBarItem() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TabBarItem.prototype.render = function () {
        var _a = this.props, children = _a.children, label = _a.label, isActiveTab = _a.isActiveTab, attrs = __rest(_a, ["children", "label", "isActiveTab"]);
        return createElement("div", __assign({ className: b$9(null, { isActiveTab: isActiveTab }) }, attrs), children);
    };
    TabBarItem.defaultProps = {
        isActiveTab: false,
        label: '',
        name: ''
    };
    return TabBarItem;
}(Component));

var style$a = {"tabBarNav":"tabBarNav_tabBarNav__1Mm3q","tabBarNav_isActiveTab":"tabBarNav_tabBarNav_isActiveTab__2wkMN"};

var b$a = block(style$a);
var TabBarNav = /** @class */ (function (_super) {
    __extends(TabBarNav, _super);
    function TabBarNav() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.handleOnChangeTab = function () {
            var _a = _this.props, label = _a.label, name = _a.name, onChangeTab = _a.onChangeTab;
            onChangeTab(label, name);
        };
        return _this;
    }
    TabBarNav.prototype.render = function () {
        var _a = this.props, label = _a.label, isActiveTab = _a.isActiveTab;
        return createElement("button", { className: b$a(null, { isActiveTab: isActiveTab }), onClick: this.handleOnChangeTab }, label);
    };
    TabBarNav.defaultProps = {
        name: ''
    };
    return TabBarNav;
}(Component));

var style$b = {"widgetTaskListItem":"widgetTaskListItem_widgetTaskListItem__11_pB","widgetTaskListItem__title":"widgetTaskListItem_widgetTaskListItem__title__1aKt8","widgetTaskListItem_red":"widgetTaskListItem_widgetTaskListItem_red__2fb45","widgetTaskListItem_yellow":"widgetTaskListItem_widgetTaskListItem_yellow__nP672","widgetTaskListItem_blue":"widgetTaskListItem_widgetTaskListItem_blue__1Gf0R","widgetTaskListItem_green":"widgetTaskListItem_widgetTaskListItem_green__30Xc3","widgetTaskListItem_gray":"widgetTaskListItem_widgetTaskListItem_gray__1jpDo","widgetTaskListItem_last":"widgetTaskListItem_widgetTaskListItem_last__2taOh","widgetTaskListItem__date":"widgetTaskListItem_widgetTaskListItem__date__150rz","widgetTaskListItem__expired":"widgetTaskListItem_widgetTaskListItem__expired__3ifQa"};

var b$b = block(style$b);
var WidgetTaskListItem = function (_a) {
    var _b;
    var id = _a.id, name = _a.name, limitDate = _a.limitDate, badge = _a.badge, last = _a.last, is_expired = _a.is_expired, onClickTask = _a.onClickTask;
    var handleOnClick = function () {
        onClickTask(id);
    };
    return (createElement("div", { onClick: handleOnClick, className: b$b(null, (_b = {}, _b[badge] = true, _b.last = last, _b)) },
        (is_expired) ? createElement("i", { className: b$b('expired') },
            createElement(SvgClockUnion, null)) : null,
        createElement("div", { className: b$b('title') }, name),
        createElement("time", { className: b$b('date') }, moment(limitDate).format('DD MMMM, HH:mm'))));
};

var style$c = {"checkbox":"checkbox_checkbox__3nWmB","checkbox_checked":"checkbox_checkbox_checked__1obLl","checkbox__inner":"checkbox_checkbox__inner__1hmIn","checkbox__wrapper":"checkbox_checkbox__wrapper__oRUsL","checkbox__wrapper_disabled":"checkbox_checkbox__wrapper_disabled__8K9tl","checkbox__native":"checkbox_checkbox__native__3kkPV","checkbox__label":"checkbox_checkbox__label__Hjd_6"};

var b$c = block(style$c);
var Checkbox = /** @class */ (function (_super) {
    __extends(Checkbox, _super);
    function Checkbox() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.handleOnChange = function (e) {
            e.preventDefault();
            e.stopPropagation();
            var _a = _this.props, onChange = _a.onChange, disabled = _a.disabled;
            if (disabled) {
                return false;
            }
            onChange(!_this.props.checked);
        };
        return _this;
    }
    Checkbox.prototype.render = function () {
        var _a = this.props, checked = _a.checked, disabled = _a.disabled, children = _a.children;
        return createElement("label", { className: b$c('wrapper', { disabled: disabled }) },
            createElement("span", { className: b$c(null, { checked: checked }) },
                createElement("input", { type: "checkbox", checked: checked, onChange: this.handleOnChange, className: b$c('native') }),
                createElement("span", { className: b$c('inner') })),
            createElement("span", { className: b$c('label') }, children));
    };
    Checkbox.defaultProps = {
        checked: false,
        disabled: false,
        onChange: function (checked) { return 0; }
    };
    return Checkbox;
}(Component));

var style$d = {"option":"option_option__3Nn3l","option_disabled":"option_option_disabled__3AGO2"};

var b$d = block(style$d);
var Option = /** @class */ (function (_super) {
    __extends(Option, _super);
    function Option() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.handleOnClick = function (e) {
            e.preventDefault();
            e.stopPropagation();
            var _a = _this.props, value = _a.value, disabled = _a.disabled, onClick = _a.onClick;
            if (disabled) {
                return false;
            }
            if (onClick) {
                onClick(value);
            }
        };
        return _this;
    }
    Option.prototype.render = function () {
        var _a = this.props, children = _a.children, disabled = _a.disabled, props = __rest(_a, ["children", "disabled"]);
        return createElement("div", __assign({}, props, { onClick: this.handleOnClick, className: b$d(null, { disabled: disabled }) }), children);
    };
    Option.defaultProps = {
        disabled: false
    };
    return Option;
}(Component));

var style$e = {"badge":"badge_badge__3V7iM","badge_red":"badge_badge_red__37xA_","badge_yellow":"badge_badge_yellow__1P21N","badge_blue":"badge_badge_blue__3gyV8","badge_green":"badge_badge_green__1IY1L","badge_gray":"badge_badge_gray__3EmGG","badge_small":"badge_badge_small__1eCle","badge_medium":"badge_badge_medium__3x_OC"};

var b$e = block(style$e);
var Badge = /** @class */ (function (_super) {
    __extends(Badge, _super);
    function Badge() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Badge.prototype.render = function () {
        var _a;
        var _b = this.props, color = _b.color, size = _b.size;
        if (color.length === 0) {
            return false;
        }
        return createElement("span", { className: b$e(null, (_a = {}, _a[size] = true, _a[color] = true, _a)) });
    };
    Badge.defaultProps = {
        color: '',
        size: 'medium'
    };
    return Badge;
}(Component));

var style$f = {"tag":"tag_tag__1iUBZ","tag__remove":"tag_tag__remove__1qdWH","tag__body":"tag_tag__body__3mzpg"};

var b$f = block(style$f);
var Tag = /** @class */ (function (_super) {
    __extends(Tag, _super);
    function Tag() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.handleOnClick = function (e) {
            var _a = _this.props, name = _a.name, onClick = _a.onClick;
            e.preventDefault();
            onClick(name);
        };
        return _this;
    }
    Tag.prototype.render = function () {
        var children = this.props.children;
        return createElement("span", { className: b$f(null) },
            createElement("span", { className: b$f('body') }, children),
            createElement("button", { onClick: this.handleOnClick, className: b$f('remove') }));
    };
    Tag.defaultProps = {
        name: '',
        onClick: function () { return ({}); }
    };
    return Tag;
}(Component));

var style$g = {"priorityColumn":"priorityColumn_priorityColumn__3xo_4","priorityColumn_red":"priorityColumn_priorityColumn_red__2q7BS","priorityColumn_yellow":"priorityColumn_priorityColumn_yellow__1Qgbd","priorityColumn_blue":"priorityColumn_priorityColumn_blue__3o1X7","priorityColumn_green":"priorityColumn_priorityColumn_green__2xcWM","priorityColumn_gray":"priorityColumn_priorityColumn_gray__3Y0QW"};

var b$g = block(style$g);
var PriorityColumn = /** @class */ (function (_super) {
    __extends(PriorityColumn, _super);
    function PriorityColumn() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PriorityColumn.prototype.render = function () {
        var _a;
        var color = this.props.color;
        return createElement("span", { className: b$g(null, (_a = {}, _a[color] = true, _a)) });
    };
    return PriorityColumn;
}(Component));

var style$h = {"link":"link_link__1iz1J","link_type_primary":"link_link_type_primary__2KjQr","link_type_secondary":"link_link_type_secondary__7zRM_","link_size_15":"link_link_size_15__3zdEh","link_size_13":"link_link_size_13__3Uw-q"};

var b$h = block(style$h);
var Link = function (_a) {
    var href = _a.href, _b = _a.type, type = _b === void 0 ? 'primary' : _b, _c = _a.size, size = _c === void 0 ? '13' : _c, className = _a.className, content = _a.content, beforeAdornment = _a.beforeAdornment, afterAdornment = _a.afterAdornment, rest = __rest(_a, ["href", "type", "size", "className", "content", "beforeAdornment", "afterAdornment"]);
    return (createElement(Fragment, null,
        createElement("a", __assign({ className: b$h({ type: type, size: size }) + " " + (className && className), href: href }, rest),
            beforeAdornment,
            content,
            afterAdornment)));
};

var style$i = {"fullNameWithAvatar":"fullNameWithAvatar_fullNameWithAvatar__3ek29","fullNameWithAvatar__avatar":"fullNameWithAvatar_fullNameWithAvatar__avatar__1JPvP"};

var b$i = block(style$i);
var FullNameWithAvatar = function (_a) {
    var image = _a.image, fullName = _a.fullName, post = _a.post;
    return (createElement("div", { className: b$i(null) },
        createElement("div", { className: b$i('avatar') },
            createElement(Avatar, { image: image })),
        createElement(FullName, { fullName: fullName, post: post })));
};

var style$j = {"header":"header_header__332mG","header__left":"header_header__left__1ovbh","header__right":"header_header__right__2RHWf","header__date":"header_header__date__3W9Vc"};

var b$j = block(style$j);
var Header = function (_a) {
    var image = _a.image, fullName = _a.fullName, post = _a.post;
    return (createElement("div", { className: b$j(null) },
        createElement("div", { className: b$j('left') },
            createElement(FullNameWithAvatar, { image: image, fullName: fullName, post: post })),
        createElement("div", { className: b$j('right') },
            createElement("div", { className: b$j('date') },
                createElement(DateWithTime, null)),
            createElement(Faq, null))));
};

var style$k = {"menu":"menu_menu__2OiXz"};

var b$k = block(style$k);
var mappingIcon = {
    'main': createElement(SvgMain, null),
    'calendar': createElement(SvgCalendar, null),
    'tasks': createElement(SvgTasks, null),
    'analytics': createElement(SvgAnalytics, null),
    'marketplace': createElement(SvgMarketplace, null),
    'education': createElement(SvgEducation, null),
    'kpi': createElement(SvgKpi, null),
    'processes': createElement(SvgProcesses, null)
};
var getIcon = function (icon) {
    // @ts-ignore
    return mappingIcon[icon];
};
var Menu = function (_a) {
    var list = _a.list;
    var _b = useState("main"), currentIcon = _b[0], setIcon = _b[1];
    var getCurrentSectionName = function () {
        var pathname = window.location.pathname;
        var section = pathname.substring(pathname.lastIndexOf("/") + 1);
        return section.length === 0 ? "main" : section;
    };
    var handlerOnClick = function (icon, link) {
        var pathname = window.location.pathname;
        if (!pathname.endsWith(link)) {
            window.location.pathname = pathname.substring(0, pathname.lastIndexOf("/")) + link;
        }
        setIcon(icon);
    };
    useEffect(function () {
        setIcon(getCurrentSectionName());
    }, []);
    return (createElement("div", { className: b$k(null) }, list.map(function (item, index) {
        return (item.is_hidden ? null : createElement(MenuItem, __assign({ key: item.icon }, item, { is_active: (currentIcon === item.icon), getIcon: getIcon, onClick: handlerOnClick })));
    })));
};

var style$l = {"notificationPanel":"notificationPanel_notificationPanel__2tr2b","notificationPanel_warningBackground":"notificationPanel_notificationPanel_warningBackground__KvHXF","notificationPanel__iconWrapper":"notificationPanel_notificationPanel__iconWrapper__3JX3g","notificationPanel__contentWrapper":"notificationPanel_notificationPanel__contentWrapper__1HzPB","notificationPanel__linksWrapper":"notificationPanel_notificationPanel__linksWrapper__1SEVq","notificationPanel__linkItem":"notificationPanel_notificationPanel__linkItem__2e0G1","notificationPanel__button":"notificationPanel_notificationPanel__button__XRDFj","notificationPanel__contentTitle":"notificationPanel_notificationPanel__contentTitle__r_yC8","notificationPanel__contentTitleStatus":"notificationPanel_notificationPanel__contentTitleStatus__MlVZs","notificationPanel__contentTitleStatus_warning":"notificationPanel_notificationPanel__contentTitleStatus_warning__1czlU","notificationPanel__contentCompany":"notificationPanel_notificationPanel__contentCompany__32p4L"};

var b$l = block(style$l);
var NotificationPanel = function (_a) {
    var links = _a.links, _b = _a.isSuccess, isSuccess = _b === void 0 ? true : _b, _c = _a.content, isNew = _c.isNew, type = _c.type, companyName = _c.companyName, onClick = _a.onClick;
    return (createElement("div", { className: b$l(null, { warningBackground: !isSuccess }) },
        createElement("div", { className: b$l('iconWrapper') }, isSuccess ? createElement(SvgSuccess, null) : createElement(SvgWarning, null)),
        createElement("div", { className: b$l('contentWrapper') },
            createElement("div", { className: b$l('contentTitle') },
                createElement("span", { className: b$l('contentTitleStatus', { warning: !isSuccess }) },
                    "(",
                    isNew ? 'Новый' : 'Просрочен',
                    ") "),
                type),
            createElement("div", { className: b$l('contentCompany') }, companyName)),
        createElement("div", { className: b$l('linksWrapper') }, links && links.map(function (_a) {
            var rest = __rest(_a, []);
            return createElement(Link, __assign({ className: b$l('linkItem') }, rest));
        })),
        createElement("button", { onClick: onClick, className: b$l('button') })));
};

var style$m = {"priorityPanel":"priorityPanel_priorityPanel__3B9pQ"};

var b$m = block(style$m);
var PriorityPanel = function (_a) {
    var children = _a.children;
    return createElement("div", { className: b$m(null) }, children);
};

var style$n = {"selectbox":"selectbox_selectbox__1dDWw","selectbox_open":"selectbox_selectbox_open__1RhuJ","selectbox__arrow":"selectbox_selectbox__arrow__3zmiQ","selectbox__list":"selectbox_selectbox__list__1nJy7","selectbox__select":"selectbox_selectbox__select__1RN-2","selectbox__option":"selectbox_selectbox__option__2lTA6"};

var b$n = block(style$n);
var SelectBox = /** @class */ (function (_super) {
    __extends(SelectBox, _super);
    function SelectBox() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            open: false
        };
        _this.wrapperRef = createRef();
        _this.handleToggle = function () { return _this.setState(function (state) { return ({ open: !state.open }); }); };
        _this.handleOnClose = function () { return _this.setState(function () { return ({ open: false }); }); };
        _this.handleClickOutside = function (e) {
            if (_this.wrapperRef && _this.wrapperRef.current && !_this.wrapperRef.current.contains(e.target)) {
                _this.handleOnClose();
            }
        };
        _this.handleOnSelect = function (value) {
            var _a = _this.props, multiple = _a.multiple, onSelect = _a.onSelect;
            onSelect(value);
            if (!multiple) {
                _this.handleOnClose();
                return;
            }
        };
        return _this;
    }
    SelectBox.prototype.componentDidMount = function () {
        document.addEventListener('mousedown', this.handleClickOutside);
    };
    SelectBox.prototype.componentWillUnmount = function () {
        document.removeEventListener('mousedown', this.handleClickOutside);
    };
    SelectBox.prototype.render = function () {
        var _this = this;
        var _a = this.props, placeholder = _a.placeholder, multiple = _a.multiple, selected = _a.selected, children = _a.children;
        var open = this.state.open;
        return createElement("div", { className: b$n(null, { open: open }), ref: this.wrapperRef },
            createElement("div", { className: b$n('select'), onClick: this.handleToggle },
                placeholder,
                createElement("span", { className: b$n('arrow') },
                    createElement(SvgArrowDown, null))),
            createElement("div", { className: b$n('list') }, multiple && Array.isArray(selected)
                ? (Children.map(children, function (child) {
                    return (createElement(Option, { value: child.props.value, onClick: _this.handleOnSelect },
                        createElement(Checkbox, { checked: selected.includes(child.props.value) }, child.props.children)));
                }))
                : (Children.map(children, function (child) { return cloneElement(child, {
                    onClick: _this.handleOnSelect
                }); }))));
    };
    SelectBox.defaultProps = {
        label: '',
        multiple: false,
        placeholder: 'Не указан',
        selected: '',
        onSelect: function (value) { return ({}); }
    };
    return SelectBox;
}(Component));

var style$o = {"sidebar":"sidebar_sidebar__1R0iV","sidebar__logotype":"sidebar_sidebar__logotype__1jVA3"};

var b$o = block(style$o);
var Sidebar = function (_a) {
    var list = _a.list;
    return (createElement("div", { className: b$o(null) },
        createElement("div", { className: b$o('logotype') },
            createElement(Logotype, null)),
        createElement(Menu, { list: list })));
};

var style$p = {"tab-bar":"tabBar_tab-bar__3RVXW","tab-bar__navigation":"tabBar_tab-bar__navigation__1F1zB","tab-bar__container":"tabBar_tab-bar__container__Au3yn"};

var b$p = block(style$p);
var TabBar = /** @class */ (function (_super) {
    __extends(TabBar, _super);
    function TabBar() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            currentTab: 0,
            activeTab: null
        };
        _this.getChildrenLabels = function (children) {
            return children.map(function (_a) {
                var props = _a.props;
                return props.label;
            });
        };
        _this.setActiveTab = function (activeTab, name) {
            if (name === void 0) { name = ''; }
            var onChange = _this.props.onChange;
            var currentTab = _this.state.currentTab;
            if (currentTab !== activeTab) {
                _this.setState(function () { return ({ activeTab: activeTab }); });
                if (onChange) {
                    onChange(name);
                }
            }
        };
        _this.renderTabs = function () {
            var _a = _this.props.children, children = _a === void 0 ? [] : _a;
            var activeTab = _this.state.activeTab;
            return _this.getChildrenLabels(children).map(function (label, name) {
                return createElement(TabBarNav, { key: name, isActiveTab: (activeTab === label), label: label, name: name, onChangeTab: _this.setActiveTab });
            });
        };
        return _this;
    }
    TabBar.prototype.componentDidMount = function () {
        var children = this.props.children;
        var activeTab = this.getChildrenLabels(children)[0];
        this.setActiveTab(activeTab);
    };
    TabBar.prototype.render = function () {
        var activeTab = this.state.activeTab;
        var _a = this.props, children = _a.children, className = _a.className, vertical = _a.vertical, attrs = __rest(_a, ["children", "className", "vertical"]);
        return createElement("div", { className: b$p(null) },
            createElement("div", { className: b$p('navigation') }, this.renderTabs()),
            createElement("div", { className: b$p('container') }, Children.map(children, function (child) { return cloneElement(child, {
                isActiveTab: (activeTab === child.props.label)
            }); })));
    };
    TabBar.defaultProps = {
        children: [],
        className: '',
        vertical: false
    };
    return TabBar;
}(Component));

var style$q = {"widget":"widget_widget__2yIPh","widget__left":"widget_widget__left__19N1w","widget__right":"widget_widget__right__3e5bf","widget__header":"widget_widget__header__1gHSh","widget__icon":"widget_widget__icon__p-3_f","widget__title":"widget_widget__title__1lRAB","widget__link":"widget_widget__link__36k6O","widget__content":"widget_widget__content__1q_c6"};

var b$q = block(style$q);
var Widget = function (_a) {
    var header = _a.header, link = _a.link, icon = _a.icon, children = _a.children, onClickHeader = _a.onClickHeader;
    var handleOnClick = function () {
        if (onClickHeader && link != null) {
            onClickHeader(link);
        }
    };
    return (createElement("div", { className: b$q(null) },
        createElement("div", { className: b$q('header'), onClick: handleOnClick },
            createElement("div", { className: b$q('left') },
                createElement("div", { className: b$q('icon') }, icon),
                createElement("span", { className: b$q('title') }, header)),
            createElement("div", { className: b$q('right') },
                createElement("a", { href: link, className: b$q('link') },
                    createElement(SvgArrow, null)))),
        createElement("div", { className: b$q('content') }, children)));
};

var style$r = {"widget-calendar":"widget-calendar_widget-calendar__3dwOW"};

var style$s = {"calendar-list":"list_calendar-list__1p-Qv","calendar-list__item":"list_calendar-list__item__3kXGE","calendar-list__line":"list_calendar-list__line__NydTg","calendar-list__hour":"list_calendar-list__hour__1u1ED"};

var style$t = {"item-event":"item-event_item-event__3RiJm","item-event_red":"item-event_item-event_red__3Pg9H","item-event_yellow":"item-event_item-event_yellow__2K6A1","item-event_green":"item-event_item-event_green__XELnV","item-event_blue":"item-event_item-event_blue__1CmJu","item-event__view":"item-event_item-event__view__3MMgx"};

var b$r = block(style$t);
var ItemEvent = function (_a) {
    var _b;
    var id = _a.id, indent = _a.indent, length = _a.length, type = _a.type, title = _a.title, description = _a.description, timeStart = _a.timeStart, timeEnd = _a.timeEnd;
    var elementRef = useRef(null);
    var getWidth = function () {
        var total = Math.abs((indent * 28) + 13);
        return "calc(100% - " + total + "px";
    };
    var handleOnClick = function (e) {
        e.preventDefault();
    };
    return (createElement(Fragment, null,
        createElement("div", { className: b$r('view') }, "fdghdfs"),
        createElement("div", { onClick: handleOnClick, ref: elementRef, className: b$r(null, (_b = {}, _b[type] = true, _b)), style: {
                left: (indent * 28) + 13,
                height: (length * 75),
                width: getWidth()
            } }, title)));
};

var b$s = block(style$s);
var CalendarItem = function (_a) {
    var hour = _a.hour, list = _a.list;
    return (createElement("div", { className: b$s('item') },
        createElement("div", { className: b$s('line') },
            createElement("div", { className: b$s('hour') }, hour),
            list.map(function (item, index) {
                return createElement(ItemEvent, __assign({ key: item.id, indent: index }, item));
            }))));
};

var b$t = block(style$s);
var CalendarList = function (_a) {
    var list = _a.list;
    var containerRef = useRef(null);
    useEffect(function () {
        if (containerRef.current) {
            // Количество текущих часов умножаем на высоту (80px)
            var scrollPixel = (new Date()).getHours() * 80;
            containerRef.current.scrollTo(0, scrollPixel);
        }
    }, []);
    var getDecimalIndents = function (time) {
        var arrTime = time.split(':');
        var hour = +arrTime[0];
        var minute = +arrTime[1];
        // Вычисляем минуты
        var percent = (minute * 100) / 60;
        return hour + (percent / 100);
    };
    var getHourToday = function () {
        var complete = [];
        return Array.from({ length: 24 }, function (_, i) { return i; }).reduce(function (r, hour) {
            var dataHour = {
                list: []
            };
            var currentHour = parseInt(moment({ hour: hour, minute: 0 }).format('H'));
            list.forEach(function (item) {
                var timeStart = parseInt(moment(item.dateStart).format('H'));
                if (currentHour == timeStart && timeStart < (currentHour + 1)) {
                    var durationTime = moment.utc(moment.duration(moment(item.dateEnd)
                        .diff(moment(item.dateStart)))
                        .asMilliseconds()).format('H:mm');
                    dataHour.list.push(__assign(__assign({}, item), { length: getDecimalIndents(durationTime), timeStart: moment(item.dateStart).format('H:mm'), timeEnd: moment(item.dateEnd).format('H:mm') }));
                }
            });
            r.push(__assign({ hour: currentHour }, dataHour));
            return r;
        }, complete);
    };
    return (createElement("div", { className: b$t(null), ref: containerRef }, getHourToday().map(function (item) {
        return createElement(CalendarItem, __assign({ key: item.hour.toString() }, item));
    })));
};

var b$u = block(style$r);
var WidgetCalendar = function (_a) {
    var list = _a.list, link = _a.link, onClickHeader = _a.onClickHeader;
    return (createElement("div", { className: b$u(null) },
        createElement(Widget, { icon: createElement(SvgCalendarOutline, null), header: 'Календарь', link: link, onClickHeader: onClickHeader },
            createElement(CalendarList, { list: list }))));
};

var style$u = {"widget-statistics":"widget-statistics_widget-statistics__15emE","widget-statistics__header":"widget-statistics_widget-statistics__header__2DdFT","widget-statistics__body":"widget-statistics_widget-statistics__body__3Zgno","widget-statistics__column":"widget-statistics_widget-statistics__column__1VkRu","widget-statistics__column_expired":"widget-statistics_widget-statistics__column_expired__Gx1Cb","widget-statistics__text":"widget-statistics_widget-statistics__text__1WBOv"};

var b$v = block(style$u);
var WidgetStatistics = function (_a) {
    var header = _a.header, today = _a.today, expired = _a.expired;
    return (createElement("div", { className: b$v(null) },
        createElement("div", { className: b$v('header') }, header),
        createElement("div", { className: b$v('body') },
            createElement("div", { className: b$v('column') },
                today,
                createElement("div", { className: b$v('text') }, "\u041D\u0430 \u0441\u0435\u0433\u043E\u0434\u043D\u044F")),
            createElement("div", { className: b$v('column', { expired: true }) },
                expired,
                createElement("div", { className: b$v('text') }, "\u041F\u0440\u043E\u0441\u0440\u043E\u0447\u0435\u043D\u043E")))));
};

var style$v = {"widget-tasks":"widget-tasks_widget-tasks__8gbep","widget-tasks__list":"widget-tasks_widget-tasks__list__bdCrM"};

var b$w = block(style$v);
var WidgetTasks = function (_a) {
    var my = _a.my, received = _a.received, link = _a.link, onClickHeader = _a.onClickHeader, onClickTask = _a.onClickTask;
    return (createElement("div", { className: b$w(null) },
        createElement(Widget, { header: 'Задачи', link: link, onClickHeader: onClickHeader, icon: createElement(SvgTasksOutline, null) },
            createElement("div", { className: "container-fluid no-padding" },
                createElement("div", { className: "row" },
                    createElement("div", { className: "col-lg-6 col-md-6 col-sm-6" },
                        createElement(WidgetStatistics, { header: 'Мои задачи', today: my.today, expired: my.expired }),
                        createElement("div", { className: b$w('list') }, my.list.map(function (task) { return (createElement(WidgetTaskListItem, __assign({ key: task.id, onClickTask: onClickTask }, task))); }))),
                    createElement("div", { className: "col-lg-6 col-md-6 col-sm-6" },
                        createElement(WidgetStatistics, { header: 'Порученные мной', today: received.today, expired: received.expired }),
                        received.list.map(function (task) { return (createElement(WidgetTaskListItem, __assign({ key: task.id, onClickTask: onClickTask }, task))); })))))));
};

var style$w = {"datepicker":"datepicker_datepicker__2bVVz","datepicker__container":"datepicker_datepicker__container__34O0D","datepicker__container_open":"datepicker_datepicker__container_open__1qZ10","datepicker__date":"datepicker_datepicker__date__3E4KX","datepicker__year":"datepicker_datepicker__year__3gnxh"};

var b$x = block(style$w);
var DatePicker = /** @class */ (function (_super) {
    __extends(DatePicker, _super);
    function DatePicker() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            open: _this.props.open,
            from: _this.props.from,
            to: _this.props.to
        };
        _this.wrapperRef = createRef();
        _this.handleToggle = function () { return _this.setState(function (state) { return ({ open: !state.open }); }); };
        _this.handleOnClose = function () { return _this.setState(function () { return ({ open: false }); }); };
        _this.handleClickOutside = function (e) {
            if (_this.wrapperRef && _this.wrapperRef.current && !_this.wrapperRef.current.contains(e.target)) {
                _this.handleOnClose();
            }
        };
        _this.handleOnChange = function (day) {
            var onChange = _this.props.onChange;
            var range = DateUtils.addDayToRange(day, _this.state);
            _this.setState(function (state) {
                return {
                    to: (range.to === null) ? state.to : range.to,
                    from: (range.from === null) ? state.from : range.from
                };
            }, function () {
                onChange({
                    to: _this.state.to,
                    from: _this.state.from
                });
            });
        };
        _this.handleOnShowToggle = function () {
            _this.setState(function (state) { return ({
                open: !state.open
            }); });
        };
        return _this;
    }
    DatePicker.prototype.componentDidMount = function () {
        document.addEventListener('mousedown', this.handleClickOutside);
    };
    DatePicker.prototype.componentWillUnmount = function () {
        document.removeEventListener('mousedown', this.handleClickOutside);
    };
    DatePicker.prototype.render = function () {
        var _a = this.state, from = _a.from, to = _a.to, open = _a.open;
        var modifiers = { start: from, end: to };
        return createElement("div", { className: b$x(null), ref: this.wrapperRef },
            createElement("div", { className: b$x('date'), onClick: this.handleOnShowToggle },
                moment(from).format('D MMM'),
                " - ",
                moment(to).format('D MMM'),
                ",",
                createElement("span", { className: b$x('year') }, moment(to).format('YYYY')),
                createElement(SvgCalendarOutline, null)),
            createElement("div", { className: b$x('container', { open: open }) },
                createElement(DayPicker, __assign({}, this.props, { pagedNavigation: true, showOutsideDays: true, locale: 'ru', localeUtils: MomentLocaleUtils, modifiers: modifiers, selectedDays: [from, { from: from, to: to }], onDayClick: this.handleOnChange }))));
    };
    DatePicker.defaultProps = {
        open: false,
        from: (new Date()),
        to: (new Date()),
        onChange: function (date) { return ({}); }
    };
    return DatePicker;
}(Component));

var style$x = {"leadCard":"leadCard_leadCard__3_wtn","leadCard__wrapper":"leadCard_leadCard__wrapper__2Nw-L","leadCard__title":"leadCard_leadCard__title__207Y2","leadCard__title_titleType_default":"leadCard_leadCard__title_titleType_default__Ce4H_","leadCard__title_titleType_error":"leadCard_leadCard__title_titleType_error__32jqs","leadCard__timer":"leadCard_leadCard__timer__fhObH","leadCard__timer_timerType_success":"leadCard_leadCard__timer_timerType_success__3POz9","leadCard__timer_timerType_warning":"leadCard_leadCard__timer_timerType_warning__25vPm","leadCard__timer_timerType_error":"leadCard_leadCard__timer_timerType_error__1LguY","leadCard__detailsWrapper":"leadCard_leadCard__detailsWrapper__1LhSc","leadCard__detailsItem":"leadCard_leadCard__detailsItem__1v1_c","leadCard__companyName":"leadCard_leadCard__companyName__34KuL","leadCard__wrapperIcon":"leadCard_leadCard__wrapperIcon__1rRTr","leadCard__fullName":"leadCard_leadCard__fullName__3nauN","leadCard__leadChannel":"leadCard_leadCard__leadChannel__1-kDX","leadCard__linksWrapper":"leadCard_leadCard__linksWrapper__2CYrA","leadCard__link":"leadCard_leadCard__link__1gVxi"};

var b$y = block(style$x);
var LeadCard = function (_a) {
    var id = _a.id, title = _a.title, created = _a.created, _b = _a.timer, timer = _b === void 0 ? '4:29' : _b, companyName = _a.companyName, fullName = _a.fullName, leadChannel = _a.leadChannel, _c = _a.timerType, timerType = _c === void 0 ? 'success' : _c, _d = _a.titleType, titleType = _d === void 0 ? 'default' : _d, links = _a.links, branch = _a.branch, performer = _a.performer;
    return (createElement("div", { className: b$y(null), key: id },
        createElement("p", { className: b$y('title', { titleType: titleType }) }, title),
        createElement("span", { className: b$y('timer', { timerType: timerType }) }, timer),
        createElement("div", { className: b$y('detailsWrapper') },
            created && createElement("span", { className: b$y('detailsItem') },
                "\u0414\u0430\u0442\u0430 \u043F\u043E\u0434\u0430\u0447\u0438: ",
                created),
            branch && createElement("span", { className: b$y('detailsItem') },
                "\u0422\u041F: ",
                branch),
            performer && createElement("span", { className: b$y('detailsItem') },
                "\u0418\u0441\u043F\u043E\u043B\u043D\u0438\u0442\u0435\u043B\u044C: ",
                performer)),
        createElement("p", { className: b$y('companyName') }, companyName),
        createElement("div", { className: b$y('wrapper') },
            createElement("div", { className: b$y('wrapperIcon') },
                createElement(SvgPerson, null)),
            createElement("p", { className: b$y('fullName') }, fullName)),
        createElement("div", { className: b$y('wrapper') },
            createElement("div", { className: b$y('wrapperIcon') },
                createElement(SvgGetCash, null)),
            createElement("p", { className: b$y('leadChannel') }, leadChannel)),
        links && (createElement("div", { className: b$y('linksWrapper') }, links.map(function (_a) {
            var title = _a.title, href = _a.href, id = _a.id;
            return (createElement("a", { className: b$y('link'), href: href, key: id }, title));
        })))));
};

var style$y = {"datepickerInput":"datepickerInput_datepickerInput__PWMZR","datepickerInput__icon":"datepickerInput_datepickerInput__icon__1FUWd"};

var b$z = block(style$y);
var DatepickerInput = /** @class */ (function (_super) {
    __extends(DatepickerInput, _super);
    function DatepickerInput() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DatepickerInput.prototype.render = function () {
        return createElement("div", { className: b$z(null) },
            createElement("i", { className: b$z('icon') },
                createElement(SvgCalendar, null)),
            createElement(DayPickerInput, { placeholder: 'Укажите дату', format: "D MMMM YYYY", formatDate: MomentLocaleUtils.formatDate, parseDate: MomentLocaleUtils.parseDate, dayPickerProps: {
                    locale: 'ru',
                    localeUtils: MomentLocaleUtils,
                    modifiers: {
                        disabled: [
                            {
                                before: new Date()
                            }
                        ]
                    }
                } }));
    };
    return DatepickerInput;
}(Component));

var style$z = {"timePicker":"timePicker_timePicker__2b1XT","timePicker__icon":"timePicker_timePicker__icon__1hpZ3"};

var b$A = block(style$z);
var TimePicker = /** @class */ (function (_super) {
    __extends(TimePicker, _super);
    function TimePicker() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            value: ''
        };
        _this.handleOnChange = function (value) {
            _this.setState({
                value: value
            });
        };
        return _this;
    }
    TimePicker.prototype.componentDidMount = function () {
        var datetime = new Date().setHours(10, 40);
        this.setState({
            value: datetime
        });
    };
    TimePicker.prototype.render = function () {
        var value = this.state.value;
        return createElement("div", { className: b$A(null) },
            createElement("i", { className: b$A('icon') },
                createElement(SvgClockSolid, null)),
            createElement(Datetime, { value: value, timeConstraints: {
                    minutes: {
                        min: 5,
                        max: 5,
                        step: 1
                    }
                }, dateFormat: false, timeFormat: 'HH:mm', initialViewMode: 'time', onChange: this.handleOnChange }));
    };
    return TimePicker;
}(Component));

var style$A = {"table":"table_table__2OdZo","table__row":"table_table__row__248Hk","table__row_cursor":"table_table__row_cursor__a7QE6","table__header":"table_table__header__3Oy1U","table__column":"table_table__column__19Xoh","table__dots":"table_table__dots__n38XY","table__priority":"table_table__priority__3GY5A","table__priority_error":"table_table__priority_error__3XvhN","table__priority_attention":"table_table__priority_attention__fM83C","table__priority_primary":"table_table__priority_primary__nw38s","table__priority_success":"table_table__priority_success__1tJg8","table__priority_neutral":"table_table__priority_neutral__3dcdl"};

var b$B = block(style$A);
var Table = /** @class */ (function (_super) {
    __extends(Table, _super);
    function Table() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Table.prototype.render = function () {
        var _a = this.props, columns = _a.columns, source = _a.source, renderKey = _a.renderKey, onClick = _a.onClick;
        var existsColumns = [];
        return (createElement("table", { className: b$B(null) },
            createElement("thead", null,
                createElement("tr", { className: b$B('row') }, columns.map(function (column) {
                    existsColumns.push(column.index);
                    return (createElement("th", { key: column.key, style: { width: column.width || 'auto' }, className: b$B('header') }, column.title));
                }))),
            createElement("tbody", null, source.map(function (item) {
                return (createElement("tr", { className: b$B('row', { cursor: true }), key: item[renderKey], onClick: function () { return onClick(item); } }, columns.map(function (column, index) {
                    if (column.render) {
                        return createElement("td", { key: index, className: b$B('column') }, column.render(item[column.index], item));
                    }
                    else {
                        return createElement("td", { key: index, className: b$B('column') }, item[column.index]);
                    }
                })));
            }))));
    };
    Table.defaultProps = {
        renderKey: 'id',
        source: [],
        columns: []
    };
    return Table;
}(Component));

var style$B = {"layout":"layout_layout__2VgUT","layout__sidebar":"layout_layout__sidebar__2BAdF","layout__content":"layout_layout__content__3gkI6","layout__body":"layout_layout__body__3mJiE"};

var b$C = block(style$B);
var Layout = function (_a) {
    var image = _a.image, fullName = _a.fullName, post = _a.post, menuItems = _a.menuItems, children = _a.children;
    return (createElement("div", { className: b$C(null) },
        createElement("div", { className: b$C('sidebar') },
            createElement(Sidebar, { list: menuItems })),
        createElement("div", { className: b$C('content') },
            createElement(Header, { image: image, fullName: fullName, post: post }),
            createElement("main", { className: b$C('body') }, children))));
};

var menuMock = [{
        icon: 'main',
        name: 'Главная',
        link: '/',
    }, {
        icon: 'calendar',
        name: 'Календарь',
        link: '/calendar',
    }, {
        icon: 'tasks',
        name: 'Задачи',
        link: '/tasks',
    }, {
        icon: 'processes',
        name: 'Процессы',
        link: '/processes',
    }, {
        icon: 'analytics',
        name: 'Аналитика',
        link: '/analytics'
    }, {
        icon: 'marketplace',
        name: 'Витрина',
        link: '/marketplace',
    }, {
        icon: 'education',
        name: 'Обучение',
        link: '/education',
    }, {
        icon: 'kpi',
        name: 'KPI',
        link: '/kpi',
    }];

var myTasksMock = {
    my: {
        today: 30,
        expired: 2,
        list: [{
                id: 1,
                name: 'Отчет по найму сотрудников в апреле',
                limitDate: (new Date()),
                badge: 'red',
                is_expired: true
            }, {
                id: 2,
                name: 'Отчет о переводе сотрудников на диста…',
                limitDate: (new Date()),
                badge: 'yellow'
            }, {
                id: 3,
                name: 'Встретиться с клиентом ООО Рома…',
                limitDate: (new Date()),
                badge: 'red'
            }, {
                id: 4,
                name: 'Отчет по найму сотрудников в апреле',
                limitDate: (new Date()),
                badge: 'blue',
                last: true
            }]
    },
    received: {
        today: 34,
        expired: 1,
        list: [{
                id: 5,
                name: 'Собрать документы у ИП Петров',
                limitDate: (new Date()),
                badge: 'yellow'
            }, {
                id: 6,
                name: 'Позвонить ООО АБС',
                limitDate: (new Date()),
                badge: 'red'
            }, {
                id: 7,
                name: 'Подготовить отчет по ООО Комета',
                limitDate: (new Date()),
                badge: 'blue'
            }, {
                id: 8,
                name: 'Подготовить отчет по ООО Орбита',
                limitDate: (new Date()),
                badge: 'silver',
                last: true
            }]
    }
};

var calendarWidgetMock = [{
        type: 'red',
        dateStart: moment().set({
            hour: 9,
            minute: 30
        }).toISOString(),
        dateEnd: moment().set({
            hour: 10,
            minute: 0
        }).toISOString(),
        title: 'Стендап',
        description: 'Описание программы стендапа'
    }, {
        type: 'yellow',
        dateStart: moment().set({
            hour: 10,
            minute: 30
        }).toISOString(),
        dateEnd: moment().set({
            hour: 11,
            minute: 0
        }).toISOString(),
        title: 'Единая дизайн-система',
        description: 'Описание единой дизайн-системы'
    }, {
        type: 'green',
        dateStart: moment().set({
            hour: 11,
            minute: 0
        }).toISOString(),
        dateEnd: moment().set({
            hour: 13,
            minute: 30
        }).toISOString(),
        title: 'Знакомство с первой партией ИТ-команды',
        description: 'Описание знакомства с первой партией ИТ-команды'
    }, {
        type: 'blue',
        dateStart: moment().set({
            hour: 11,
            minute: 30
        }).toISOString(),
        dateEnd: moment().set({
            hour: 13,
            minute: 0
        }).toISOString(),
        title: 'Интервью дизайнера Иванова',
        description: 'Описание интервью дизайнера Иванова'
    }, {
        type: 'blue',
        dateStart: moment().set({
            hour: 11,
            minute: 30
        }).toISOString(),
        dateEnd: moment().set({
            hour: 12,
            minute: 30
        }).toISOString(),
        title: 'Интервью дизайнера Петрова',
        description: 'Описание интервью дизайнера Петрова'
    }, {
        type: 'blue',
        dateStart: moment().set({
            hour: 11,
            minute: 30
        }).toISOString(),
        dateEnd: moment().set({
            hour: 12,
            minute: 30
        }).toISOString(),
        title: 'Интервью дизайнера Петрова',
        description: 'Описание интервью дизайнера Петрова'
    }];

var priorityPanelMock = [
    {
        data: [
            {
                id: 1,
                title: 'KPI по Р/С',
                value: '57%'
            }
        ],
        link: ''
    },
    {
        data: [
            {
                id: 1,
                title: 'Нераспределенные лиды',
                value: '11'
            },
            {
                id: 2,
                title: 'Просрочено',
                value: '10',
                isWarning: true
            }
        ],
        link: ''
    },
    {
        data: [
            {
                id: 1,
                title: 'Назначенные',
                value: '2'
            },
            {
                id: 2,
                title: 'Просрочено',
                value: '1',
                isWarning: true
            }
        ],
        link: ''
    }
];

var linksData = [
    { content: 'Взять в работу', href: '#' },
    { content: 'Назначить', href: '#' }
];
var contentNew = {
    isNew: true,
    type: 'Лид на ТП',
    companyName: 'РКО — Рога и копыта'
};

var randomInteger = function (min, max) {
    var rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
};
var mockKPI = {
    data: [
        {
            id: 1,
            title: 'KPI',
            value: ''
        }
    ],
    link: ''
};
var plus10Minites = 1000 * 60 * 10;
var plus15Minites = 1000 * 60 * 15;
var unallocatedError = {
    //@ts-ignore
    lead_created: new Date(Date.parse(new Date()) - plus15Minites).toISOString(),
    salepoint_assignment_date: new Date().toISOString(),
    assignment_deadline: new Date(new Date().getTime()).toISOString(),
    work_starting_deadline: new Date(new Date().getTime() - 1000).toISOString(),
    feedback_fixation_deadline: new Date(new Date().getTime() + 10000).toISOString()
};
var unallocatedWarning = {
    //@ts-ignore
    lead_created: new Date(Date.parse(new Date()) - plus10Minites).toISOString(),
    work_starting_deadline: new Date(new Date().getTime() + 5000).toISOString(),
    feedback_fixation_deadline: new Date(new Date().getTime() + plus10Minites + 5000).toISOString()
};
var unallocatedSuccess = {
    lead_created: new Date().toISOString(),
    work_starting_deadline: new Date(new Date().getTime() + plus10Minites).toISOString(),
    feedback_fixation_deadline: new Date(new Date().getTime() + plus15Minites).toISOString()
};
var leadCardsMockUnallocated = {
    count_actual: randomInteger(5, 10),
    count_overdue: randomInteger(0, 4),
    data: [
        {
            lead_id: Math.random(),
            lead_task_name: 'Заявка на РКО 1',
            lead_company_name: 'ООО Льбина',
            lead_channel: 'REMOTE_CHANNEL',
            lead_created_date: unallocatedError.lead_created,
            contact_person_name: 'Иванов Иван',
            contact_person_phone: '+71234567890',
            salepoint_assignment_date: unallocatedError.salepoint_assignment_date,
            assignment_deadline: unallocatedError.assignment_deadline,
            work_starting_deadline: unallocatedError.work_starting_deadline,
            feedback_fixation_deadline: unallocatedError.feedback_fixation_deadline
        },
        {
            lead_id: Math.random(),
            lead_task_name: 'Заявка на РКО 2',
            lead_company_name: 'ООО Льбина',
            lead_channel: 'INCOMING_TRAFFIC',
            lead_created_date: unallocatedWarning.lead_created,
            contact_person_name: 'Иванов Иван',
            contact_person_phone: '+71234567890',
            //@ts-ignore
            salepoint_assignment_date: unallocatedWarning.salepoint_assignment_date,
            //@ts-ignore
            assignment_deadline: unallocatedWarning.assignment_deadline,
            work_starting_deadline: unallocatedWarning.work_starting_deadline,
            feedback_fixation_deadline: unallocatedWarning.feedback_fixation_deadline
        },
        {
            lead_id: Math.random(),
            lead_task_name: 'Заявка на РКО 3',
            lead_company_name: 'ООО Льбина',
            lead_channel: 'RECOMMENDATION',
            lead_created_date: unallocatedSuccess.lead_created,
            contact_person_name: 'Иванов Иван',
            contact_person_phone: '+71234567890',
            //@ts-ignore
            salepoint_assignment_date: unallocatedSuccess.salepoint_assignment_date,
            //@ts-ignore
            assignment_deadline: unallocatedSuccess.assignment_deadline,
            work_starting_deadline: unallocatedSuccess.work_starting_deadline,
            feedback_fixation_deadline: unallocatedSuccess.feedback_fixation_deadline
        }
    ]
};
var leadCardsMockAssigned = {
    count_actual: randomInteger(5, 3),
    count_overdue: randomInteger(1, 2),
    data: [
        {
            lead_id: Math.random(),
            lead_task_name: 'Заявка на РКО 4',
            lead_company_name: 'ООО Льбина',
            lead_channel: 'CRM_CAMPAIGN',
            lead_created_date: unallocatedWarning.lead_created,
            contact_person_name: 'Иванов Иван',
            contact_person_phone: '+71234567890',
            //@ts-ignore
            salepoint_assignment_date: unallocatedWarning.salepoint_assignment_date,
            //@ts-ignore
            assignment_deadline: unallocatedWarning.assignment_deadline,
            work_starting_deadline: unallocatedWarning.work_starting_deadline,
            feedback_fixation_deadline: unallocatedWarning.feedback_fixation_deadline
        }
    ]
};
var leadCardsMock = {
    id: 1,
    title: 'Заявка на Банковская гарантия',
    created: '15:40',
    timer: '4:29',
    companyName: 'ООО Льбина',
    fullName: 'Иванов Иван',
    leadChannel: 'CRM',
    timerType: 'success',
    titleType: 'default',
    links: [{ title: 'Назначить', href: '#', id: 12 }],
    branch: 'Московский',
    performer: 'Иванов В.В'
};

export { Avatar, Badge, Checkbox, DatePicker, DateWithTime, DatepickerInput, Faq, FullName, FullNameWithAvatar, SvgGetCash as GetCash, H1, Header, SvgAnalytics as IconAnalytics, SvgApply as IconApply, SvgArrow as IconArrow, SvgArrowDown as IconArrowDown, SvgCalendar as IconCalendar, SvgCalendarOutline as IconCalendarOutline, SvgCancel as IconCancel, SvgClockUnion as IconClockOutline, SvgClockSolid as IconClockSolid, SvgDiamond as IconDiamond, SvgDownload as IconDownload, SvgEducation as IconEducation, SvgExclamationPoint as IconExclamationPoint, SvgSubtract as IconFaq, SvgGroupPersons as IconGroupPerson, SvgImport as IconImport, SvgInfo as IconInfo, SvgKpi as IconKPI, SvgLogo as IconLogo, SvgLoupe as IconLoupe, SvgMain as IconMain, SvgMarketplace as IconMarketplace, SvgOk as IconOk, SvgPencil as IconPencil, SvgProcesses as IconProcesses, SvgTasks as IconTasks, SvgTasksOutline as IconTasksOutline, SvgUnionOutline as IconUnionOutline, SvgUpload as IconUpload, Layout, LeadCard, Link, Logotype, Menu, MenuItem, NotificationPanel, Option, SvgPerson as Person, PriorityColumn, PriorityPanel, PriorityPanelItem, SelectBox, Sidebar, SvgSuccess as Success, Switch, TabBar, TabBarItem, TabBarNav, Table, Tag, TimePicker, SvgWarning as Warning, Widget, WidgetCalendar, WidgetStatistics, WidgetTaskListItem, WidgetTasks, calendarWidgetMock, contentNew, leadCardsMock, leadCardsMockAssigned, leadCardsMockUnallocated, linksData, menuMock, mockKPI, myTasksMock, priorityPanelMock };
//# sourceMappingURL=index.esm.js.map
