/// <reference types="react" />
export interface ColumnsTable {
    title: string;
    index: string;
    key: string;
    width?: string;
    render?: (text: string, row: any) => React.ReactNode;
}
