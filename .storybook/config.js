import './global.sass';

import {addParameters, configure} from '@storybook/react';

const req = require.context('../stories', true, /.stories.tsx$/);

function loadStories() {
  req.keys().forEach(req);
}

configure(loadStories, module);

addParameters({
  backgrounds: [
    {name: 'blue', value: '#001D6D', default: true},
  ],
});
