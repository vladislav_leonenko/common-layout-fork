const path = require('path');
const SRC_PATH = path.join(__dirname, '../src');
const STORIES_PATH = path.join(__dirname, '../stories');

module.exports = ({ config, mode }) => {

  const fileLoaderRule = config.module.rules.find((rule) => rule.test.test('.svg'));
  fileLoaderRule.exclude = /\.svg$/;
  if (mode === 'PRODUCTION') {
    fileLoaderRule.query.name = '/' + fileLoaderRule.query.name;
  }

  config.module.rules.push({
    test: /\.svg$/,
    use: [
      {
        loader: '@svgr/webpack',
        options: {
          icon: false
        }
      }
    ]
  });

  config.module.rules.push({
    test: /\.(ts|tsx)$/,
    include: [SRC_PATH, STORIES_PATH],
    use: [
      {
        loader: require.resolve('awesome-typescript-loader'),
        options: {
          configFileName: './tsconfig.json'
        }
      },
      {
        loader: require.resolve('react-docgen-typescript-loader')
      }
    ]
  });

  config.module.rules.push({
    test: /\.css$/,
    use: [
      {
        loader: require.resolve('style-loader')
      },
      {
        loader: require.resolve('css-loader')
      }
    ]
  });

  config.module.rules.push({
    test: /\.sass$/,
    use: [
      {
        loader: require.resolve('style-loader')
      },
      {
        loader: require.resolve('css-loader'),
        options: {
          modules: {
            localIdentName: 'ui-kit-stream__[local]___[hash:base64:5]'
          }
        }
      },
      {
        loader: require.resolve('sass-loader')
      }
    ]
  });

  config.resolve.extensions.push('.ts', '.tsx');
  return config;
};
