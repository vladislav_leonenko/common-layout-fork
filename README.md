# Ui-Kit-STREAME

# Установка
Устанавливаем пакет путем 

```
$ yarn add ssh://git@bitbucket.region.vtb.ru:7999/frnt/headarea-uikit.git
```

# Подключение к проекту

К основному файлу index.[js|tsx] подклюаем стили 
```
$ ui-kit-stream/build/ui-kit-stream.css
```

# Использование 

```
import { Layout } from 'ui-kit-stream';

render() {
    return (
      <Layout image={'https://n7.nextpng.com/sticker-png/863/636/sticker-png-businessperson-management-computer-icons-patterson-glass-business-case-avatar-man-plan-silhouette-industry-business-model-thumbnail.png'}
              fullName={'Иванов Иван Иванович'}
              post={'Руководитель ДО Пресненский'}>
        <div>контент</div>
      </Layout>
    );
  }
```
